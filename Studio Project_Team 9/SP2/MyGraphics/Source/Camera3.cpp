#include "Camera3.h"
#include "Application.h"
#include "Mtx44.h"

Camera3::Camera3()
{
}

Camera3::~Camera3()
{
}

void Camera3::Init(const Vector3& pos, const Vector3& target, const Vector3& up)
{
	currentstate = ON_GROUND;
	jumpLimit = 3;
	jumpHeight = 0;

	this->position = defaultPosition = pos;
	this->target = defaultTarget = target;
	view = (target - position).Normalized();
	Vector3 right = view.Cross(up);
	right.y = 0;
	right.Normalize();
	this->up = defaultUp = right.Cross(view).Normalized();

	MOVEMENT_SPEED = 0.3f;

	move = true;
	xMouse = 0;
	yMouse = 0;
}

void Camera3::Update(double dt, Object o[])
{
	static const float CAMERA_SPEED = 5.f;
	//MOVEMENT_SPEED = 0.3f;//0.1f;

	//if (Application::IsKeyPressed(VK_SHIFT))
	//{
	//	MOVEMENT_SPEED = 0.6f;
	//}

	view = (target - position).Normalized();
	Vector3 right = view.Cross(up);


	Application::GetMousePos(xMouse, yMouse);

	Jump(dt);

	if (Application::IsKeyPressed('A'))
	{
		
		for (int i = 0; i < 67; i++)
		{
			if (((position.x - right.x) >= o[i].minX) && ((position.x - right.x) <= o[i].maxX))
			{
				if (((position.z - right.z) >= o[i].minZ) && ((position.z - right.z) <= o[i].maxZ))
				{
					move = false;
				}
			}
		}
		if (move)
		{
			if (((position.x - right.x) >= -188.5) && ((position.x - right.x) <= 190))
			{
				if (((position.z - right.z) >= -188.5) && ((position.z - right.z) <= 190))
				{
					position.x = position.x - right.x * MOVEMENT_SPEED;
					position.z = position.z - right.z * MOVEMENT_SPEED;
					target = position + view;

				}
			}
		}
		else
		{
			move = true;
		}
	}
	if (Application::IsKeyPressed('D'))
	{
		for (int i = 0; i < 67; i++)
		{
			if (((position.x + right.x) >= o[i].minX) && ((position.x + right.x) <= o[i].maxX))
			{
				if (((position.z + right.z) >= o[i].minZ) && ((position.z + right.z) <= o[i].maxZ))
				{
					move = false;
				}
			}
		}
		if (move)
		{
			if (((position.x + right.x) >= -188.5) && ((position.x + right.x) <= 190))
			{
				if (((position.z + right.z) >= -188.5) && ((position.z + right.z) <= 190))
				{
					position.x = position.x + right.x * MOVEMENT_SPEED;
					position.z = position.z + right.z * MOVEMENT_SPEED;
					target = position + view;

				}
			}
		}
		else
		{
			move = true;
		}
	}
	if (Application::IsKeyPressed('W'))
	{
		for (int i = 0; i < 67; i++)
		{
 			if (((position.x + view.x) >= o[i].minX) && ((position.x + view.x) <= o[i].maxX))
			{
				//std::cout << "COLLISION" << std::endl;

				if (((position.z + view.z) >= o[i].minZ) && ((position.z + view.z) <= o[i].maxZ))
				{
					move = false;
					//std::cout << "COLLISION" << std::endl;
				}
			}
		}
		if (move)
		{
			if (((position.x + view.x) >= -188.5) && ((position.x + view.x) <= 190))
			{
				if (((position.z + view.z) >= -188.5) && ((position.z + view.z) <= 190))
				{
					position.x = position.x + view.x * MOVEMENT_SPEED;
					position.z = position.z + view.z * MOVEMENT_SPEED;
					target = position + view;

				}
			}
		}
		else
		{
			move = true;
		}
		
	}
	if (Application::IsKeyPressed('S'))
	{
		for (int i = 0; i < 67; i++)
		{
			if (((position.x - view.x) >= o[i].minX) && ((position.x - view.x) <= o[i].maxX))
			{
				if (((position.z - view.z) >= o[i].minZ) && ((position.z - view.z) <= o[i].maxZ))
				{
					move = false;
				}
			}
		}
		if (move)
		{
			if (((position.x - view.x) >= -188.5) && ((position.x - view.x) <= 190))
			{
				if (((position.z - view.z) >= -188.5) && ((position.z - view.z) <= 190))
				{
					position.x = position.x - view.x * MOVEMENT_SPEED;
					position.z = position.z - view.z * MOVEMENT_SPEED;
					target = position + view;
				}
			}
			
		}
		else
		{
			move = true;
		}
	}

	if (Application::IsKeyPressed(' '))
	{
		if (currentstate == ON_GROUND)
		{
			currentstate = JUMPING_UP;
		}
	}

	if (Application::IsKeyPressed('R'))
	{
		Reset();
	}

	if (yMouse < 450)
	{
		float pitch = (float)(CAMERA_SPEED * dt * (450 - yMouse));
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();

		Mtx44 rotation;
		if (view.y < 0.8)
		{
			rotation.SetToRotation(pitch, right.x, right.y, right.z);

			view = rotation * view;
			target = position + view;
		}
		
	}


	if (yMouse > 450)
	{

		float pitch = (float)(CAMERA_SPEED * dt * (450 - yMouse));
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();

		Mtx44 rotation;
		if (view.y > -0.7)
		{
			rotation.SetToRotation(pitch, right.x, right.y, right.z);

			view = rotation * view;
			target = position + view;

		}
	}

	if (xMouse < 800)
	{
		float yaw = (float)(CAMERA_SPEED * dt * (800 - xMouse));
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();

		Mtx44 rotation;
		rotation.SetToRotation(yaw, up.x, up.y, up.z);

		view = rotation * view;
		target = position + view;
	}

	if (xMouse > 800)
	{
		float yaw = (float)(CAMERA_SPEED * dt * (800 - xMouse));
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();

		Mtx44 rotation;
		rotation.SetToRotation(yaw, up.x, up.y, up.z);

		view = rotation * view;
		target = position + view;
	}
	Application::SetMousePos();

}

void Camera3::Reset()
{
	position = defaultPosition;
	target = defaultTarget;
	up = defaultUp;
}

void Camera3::setMovement(float x)
{
	MOVEMENT_SPEED = x;
}

void Camera3::Jump(double dt)
{

	if (currentstate == JUMPING_UP)
	{
		position.y += dt * 15;
		jumpHeight += dt * 15;

		if (jumpHeight > jumpLimit)
		{
			currentstate = JUMP_MAX;
		}
		target = position + view;
	}
	else if (currentstate == JUMP_MAX)
	{
		airtime += dt;
		if (airtime >= 0.04)
		{
			currentstate = JUMPING_DOWN;
			airtime = 0;
		}
		target = position + view;
	}
	else if (currentstate == JUMPING_DOWN)
	{
		position.y -= dt * 15;
		jumpHeight -= dt * 15;


		if (jumpHeight <= 0)
		{
			position.y = 4;
			currentstate = ON_GROUND;
		}
		target = position + view;
	}
}