#include "House.h"
#include "GL\glew.h"

#include "shader.hpp"
#include "MatrixStack.h"
#include "Mtx44.h"
#include "Application.h"
#include "Utility.h"
#include "LoadTGA.h"

#include "Box.h"

House::House()
{

}

House::~House()
{

}

void House::Init()
{

	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Generate a default VAO for now
	glGenVertexArrays(1, &m_vertexArrayID);
	glBindVertexArray(m_vertexArrayID);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	camera.Init(Vector3(1, 4, 1), Vector3(0, 4, 0), Vector3(0, 1, 0));

	collidedBook = false;
	bookContent = false;



	//Load vertex and fragment shaders
	//m_programID = LoadShaders("Shader//SimpleVertexShader.vertexshader", "Shader//SimpleFragmentShader.fragmentshader");
	//m_programID = LoadShaders("Shader//Shading.vertexshader", "Shader//Shading.fragmentshader");
	m_programID = LoadShaders("Shader//Shading.vertexshader", "Shader//LightSource.fragmentshader");
	m_programID = LoadShaders("Shader//Texture.vertexshader", "Shader//Texture.fragmentshader");																				// Get a handle for our "colorTexture" uniform
	m_programID = LoadShaders("Shader//Texture.vertexshader", "Shader//Blending.fragmentshader");
	m_programID = LoadShaders("Shader//Texture.vertexshader", "Shader//Text.fragmentshader");
	m_parameters[U_COLOR_TEXTURE_ENABLED] = glGetUniformLocation(m_programID, "colorTextureEnabled");
	m_parameters[U_COLOR_TEXTURE] = glGetUniformLocation(m_programID, "colorTexture");
	m_parameters[U_NUMLIGHTS] = glGetUniformLocation(m_programID, "numLights");
	m_parameters[U_LIGHT0_TYPE] = glGetUniformLocation(m_programID, "lights[0].type");
	m_parameters[U_LIGHT0_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[0].spotDirection");
	m_parameters[U_LIGHT0_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[0].cosCutoff");
	m_parameters[U_LIGHT0_COSINNER] = glGetUniformLocation(m_programID, "lights[0].cosInner");
	m_parameters[U_LIGHT0_EXPONENT] = glGetUniformLocation(m_programID, "lights[0].exponent");
	m_parameters[U_LIGHT1_TYPE] = glGetUniformLocation(m_programID, "lights[1].type");
	m_parameters[U_LIGHT1_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[1].spotDirection");
	m_parameters[U_LIGHT1_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[1].cosCutoff");
	m_parameters[U_LIGHT1_COSINNER] = glGetUniformLocation(m_programID, "lights[1].cosInner");
	m_parameters[U_LIGHT1_EXPONENT] = glGetUniformLocation(m_programID, "lights[1].exponent");
	m_parameters[U_MVP] = glGetUniformLocation(m_programID, "MVP");
	m_parameters[U_MODELVIEW] = glGetUniformLocation(m_programID, "MV");
	m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE] = glGetUniformLocation(m_programID, "MV_inverse_transpose");
	m_parameters[U_MATERIAL_AMBIENT] = glGetUniformLocation(m_programID, "material.kAmbient");
	m_parameters[U_MATERIAL_DIFFUSE] = glGetUniformLocation(m_programID, "material.kDiffuse");
	m_parameters[U_MATERIAL_SPECULAR] = glGetUniformLocation(m_programID, "material.kSpecular");
	m_parameters[U_MATERIAL_SHININESS] = glGetUniformLocation(m_programID, "material.kShininess");
	m_parameters[U_LIGHT0_POSITION] = glGetUniformLocation(m_programID, "lights[0].position_cameraspace");
	m_parameters[U_LIGHT0_COLOR] = glGetUniformLocation(m_programID, "lights[0].color");
	m_parameters[U_LIGHT0_POWER] = glGetUniformLocation(m_programID, "lights[0].power");
	m_parameters[U_LIGHT0_KC] = glGetUniformLocation(m_programID, "lights[0].kC");
	m_parameters[U_LIGHT0_KL] = glGetUniformLocation(m_programID, "lights[0].kL");
	m_parameters[U_LIGHT0_KQ] = glGetUniformLocation(m_programID, "lights[0].kQ");

	m_parameters[U_LIGHT1_POSITION] = glGetUniformLocation(m_programID, "lights[1].position_cameraspace");
	m_parameters[U_LIGHT1_COLOR] = glGetUniformLocation(m_programID, "lights[1].color");
	m_parameters[U_LIGHT1_POWER] = glGetUniformLocation(m_programID, "lights[1].power");
	m_parameters[U_LIGHT1_KC] = glGetUniformLocation(m_programID, "lights[1].kC");
	m_parameters[U_LIGHT1_KL] = glGetUniformLocation(m_programID, "lights[1].kL");
	m_parameters[U_LIGHT1_KQ] = glGetUniformLocation(m_programID, "lights[1].kQ");
	m_parameters[U_LIGHTENABLED] = glGetUniformLocation(m_programID, "lightEnabled");
	m_parameters[U_TEXT_ENABLED] = glGetUniformLocation(m_programID, "textEnabled");
	m_parameters[U_TEXT_COLOR] = glGetUniformLocation(m_programID, "textColor");

	glUseProgram(m_programID);

	glUniform1i(m_parameters[U_NUMLIGHTS], 1);

	// Enable depth tests
	InitCollision();

	Mtx44 projection;
	projection.SetToPerspective(45.f, 4.f / 3.f, 0.1f, 1000.f);
	projectionStack.LoadMatrix(projection);

	for (int i = 0; i < NUM_GEOMETRY; ++i)
	{
		meshList[i] = NULL;
	}

	meshList[GEO_AXES] = MeshBuilder::GenerateAxes("reference", 2000, 2000, 2000);

	meshList[GEO_LIGHTBALL] = MeshBuilder::GenerateSphere("Sphere", Color(1.0f, 1.0f, 1.0f), 15, 60, 2);
	meshList[GEO_LIGHTBALL2] = MeshBuilder::GenerateSphere("Sphere", Color(1.0f, 0.0f, 0.0f), 15, 60, 2);

	meshList[GEO_FRONT] = MeshBuilder::GenerateQuad("front", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_FRONT]->textureID = LoadTGA("Image//RoomFront.tga");

	meshList[GEO_BACK] = MeshBuilder::GenerateQuad("back", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_BACK]->textureID = LoadTGA("Image//Room.tga");

	meshList[GEO_LEFT] = MeshBuilder::GenerateQuad("left", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_LEFT]->textureID = LoadTGA("Image//Room.tga");

	meshList[GEO_RIGHT] = MeshBuilder::GenerateQuad("right", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_RIGHT]->textureID = LoadTGA("Image//Room.tga");

	meshList[GEO_TOP] = MeshBuilder::GenerateQuad("top", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_TOP]->textureID = LoadTGA("Image//Room.tga");

	meshList[GEO_BOTTOM] = MeshBuilder::GenerateQuad("bottom", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_BOTTOM]->textureID = LoadTGA("Image//Room.tga");

	meshList[GEO_TEXT] = MeshBuilder::GenerateText("text", 16, 16);
	meshList[GEO_TEXT]->textureID = LoadTGA("Image//calibri.tga");

	meshList[GEO_CHAIR] = MeshBuilder::GenerateOBJ("Chair", "OBJ//Chair.obj");
	meshList[GEO_CHAIR]->textureID = LoadTGA("Image//Chair.tga");

	meshList[GEO_TABLE] = MeshBuilder::GenerateOBJ("Table", "OBJ//Table.obj");
	meshList[GEO_TABLE]->textureID = LoadTGA("Image//Chair.tga");

	meshList[GEO_BOOK] = MeshBuilder::GenerateOBJ("Book", "OBJ//books_low_poly.obj");
	meshList[GEO_BOOK]->textureID = LoadTGA("Image//book.tga");

	meshList[GEO_SHELF] = MeshBuilder::GenerateOBJ("Shelf", "OBJ//shelf.obj");
	meshList[GEO_SHELF]->textureID = LoadTGA("Image//shelf.tga");

	meshList[GEO_HOUSECHAIR] = MeshBuilder::GenerateOBJ("House Chair", "OBJ//House Chair.obj");
	meshList[GEO_HOUSECHAIR]->textureID = LoadTGA("Image//House Chair.tga");

	meshList[GEO_BOOKTEXT] = MeshBuilder::GenerateQuad("book contents", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_BOOKTEXT]->textureID = LoadTGA("Image//bookText.tga");

	meshList[GEO_CAMPFIRE] = MeshBuilder::GenerateOBJ("Campfire", "OBJ//campfire.obj");
	meshList[GEO_CAMPFIRE]->textureID = LoadTGA("Image//campfire.tga");

	meshList[GEO_FIRE] = MeshBuilder::GenerateQuad("campfire fire", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_FIRE]->textureID = LoadTGA("Image//fire.tga");

	light[0].type = Light2::LIGHT_SPOT;
	light[0].position.Set(0, -200, 1);
	light[0].color.Set(1.000, 0.647, 0.000);
	light[0].power = 1;
	light[0].kC = 0.1f;
	light[0].kL = 0.01f;
	light[0].kQ = 0.001f;
	light[0].cosCutoff = cos(Math::DegreeToRadian(45));
	light[0].cosInner = cos(Math::DegreeToRadian(30));
	light[0].exponent = 3.f;
	light[0].spotDirection.Set(0.f, 1.f, 0.f);

	/*light[1].type = Light2::LIGHT_POINT;
	light[1].position.Set(0, 1, 0);
	light[1].color.Set(1, 1, 1);
	light[1].power = 0;
	light[1].kC = 1.f;
	light[1].kL = 0.01f;
	light[1].kQ = 0.001f;
	light[1].cosCutoff = cos(Math::DegreeToRadian(45));
	light[1].cosInner = cos(Math::DegreeToRadian(30));
	light[1].exponent = 3.f;
	light[1].spotDirection.Set(0.f, 1.f, 0.f);*/


	// Make sure you pass uniform parameters after glUseProgram()
	glUniform1i(m_parameters[U_LIGHT0_TYPE], light[0].type);
	glUniform3fv(m_parameters[U_LIGHT0_COLOR], 1, &light[0].color.r);
	glUniform1f(m_parameters[U_LIGHT0_POWER], light[0].power);
	glUniform1f(m_parameters[U_LIGHT0_KC], light[0].kC);
	glUniform1f(m_parameters[U_LIGHT0_KL], light[0].kL);
	glUniform1f(m_parameters[U_LIGHT0_KQ], light[0].kQ);
	glUniform1f(m_parameters[U_LIGHT0_COSCUTOFF], light[0].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT0_COSINNER], light[0].cosInner);
	glUniform1f(m_parameters[U_LIGHT0_EXPONENT], light[0].exponent);


	//glUniform3fv(m_parameters[U_LIGHT0_COLOR], 1, &light[0].color.r);
	//glUniform1f(m_parameters[U_LIGHT0_POWER], light[0].power);
	//glUniform1f(m_parameters[U_LIGHT0_KC], light[0].kC);
	//glUniform1f(m_parameters[U_LIGHT0_KL], light[0].kL);
	//glUniform1f(m_parameters[U_LIGHT0_KQ], light[0].kQ);

	//glUniform1i(m_parameters[U_LIGHT1_TYPE], light[1].type);
	//glUniform3fv(m_parameters[U_LIGHT1_COLOR], 1, &light[1].color.r);
	//glUniform1f(m_parameters[U_LIGHT1_POWER], light[1].power);
	//glUniform1f(m_parameters[U_LIGHT1_KC], light[1].kC);
	//glUniform1f(m_parameters[U_LIGHT1_KL], light[1].kL);
	//glUniform1f(m_parameters[U_LIGHT1_KQ], light[1].kQ);
	//glUniform1f(m_parameters[U_LIGHT1_COSCUTOFF], light[1].cosCutoff);
	//glUniform1f(m_parameters[U_LIGHT1_COSINNER], light[1].cosInner);
	//glUniform1f(m_parameters[U_LIGHT1_EXPONENT], light[1].exponent);


	//glUniform3fv(m_parameters[U_LIGHT1_COLOR], 1, &light[1].color.r);
	//glUniform1f(m_parameters[U_LIGHT1_POWER], light[1].power);
	//glUniform1f(m_parameters[U_LIGHT1_KC], light[1].kC);
	//glUniform1f(m_parameters[U_LIGHT1_KL], light[1].kL);
	//glUniform1f(m_parameters[U_LIGHT1_KQ], light[1].kQ);

	glUniform1i(m_parameters[U_NUMLIGHTS], 1);
}

bool Collision(Vector3 position, Box box)
{
	if ((position.x >= box.minX && position.x <= box.maxX)
		&& (position.y >= box.minY && position.y <= box.maxY)
		&& (position.z >= box.minZ && position.z <= box.maxZ))
	{
		return true;
	}
	else
	{
		return false;
	}
}

void House::BoundsCheck()
{
	Box Chair = Box(Vector3(0, 0, 0), 7);
	bool collided = Collision(camera.position, Chair);

	Box Book = Box(Vector3(-14, 3.2, 2), 4);
	collidedBook = Collision(camera.position, Book);
}

void House::InitCollision()
{
	objects[0].boxset(Vector3(10, 0, 10), 2, 3, 2);
	objects[1].boxset(Vector3(5, 0, 10), 1, 3, 1);
	objects[2].boxset(Vector3(14, 0, 10), 1, 3, 1);
	objects[3].boxset(Vector3(-14, 0, 2), 2, 30, 1);
	objects[4].boxset(Vector3(-11.2, 0, 11.2), 2, 4, 1);
}

void House::Update(double dt)
{
	static const float LSPEED = 50.0f;

	camera.Update(dt, objects);

	std::cout << camera.position << std::endl;

	if (collidedBook == true)
	{
		if (Application::IsKeyPressed(VK_LBUTTON))
		{
			bookContent = true;
		}
	}
	else
	{
		bookContent = false;
	}

	Vector3 NorthVector(0, 0, 1);
	Vector3 Direction(fireposition - camera.position);

	if (Direction.IsZero() == false)
	{
		Direction.Normalize();
		float dotproduct = NorthVector.Dot(Direction);
		firerotateAmt = acos(dotproduct);
		firerotateAmt = Math::RadianToDegree(firerotateAmt);

		if (Direction.x < 0)
		{
			firerotateAmt = -firerotateAmt;
		}
	}
	AnimateFire(dt);
	BoundsCheck();
}

void House::Render()
{
	//Clear color & depth buffer every frame
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	viewStack.LoadIdentity();
	viewStack.LookAt(camera.position.x, camera.position.y, camera.position.z, camera.target.x, camera.target.y, camera.target.z, camera.up.x, camera.up.y, camera.up.z);
	modelStack.LoadIdentity();

	if (light[0].type == Light2::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(light[0].position.x, light[0].position.y, light[0].position.z);
		Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightDirection_cameraspace.x);
	}
	else if (light[0].type == Light2::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[0].position;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = viewStack.Top() * light[0].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT0_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	}
	else
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[0].position;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightPosition_cameraspace.x);
	}

	//if (light[1].type == Light2::LIGHT_DIRECTIONAL)
	//{
	//	Vector3 lightDir(light[1].position.x, light[1].position.y, light[1].position.z);
	//	Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
	//	glUniform3fv(m_parameters[U_LIGHT1_POSITION], 1, &lightDirection_cameraspace.x);
	//}
	//else if (light[1].type == Light2::LIGHT_SPOT)
	//{
	//	Position lightPosition_cameraspace = viewStack.Top() * light[1].position;
	//	glUniform3fv(m_parameters[U_LIGHT1_POSITION], 1, &lightPosition_cameraspace.x);
	//	Vector3 spotDirection_cameraspace = viewStack.Top() * light[1].spotDirection;
	//	glUniform3fv(m_parameters[U_LIGHT1_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	//}
	//else
	//{
	//	Position lightPosition_cameraspace = viewStack.Top() * light[1].position;
	//	glUniform3fv(m_parameters[U_LIGHT1_POSITION], 1, &lightPosition_cameraspace.x);
	//}

	//RenderMesh(meshList[GEO_AXES], false);

	//modelStack.PushMatrix();
	//modelStack.Translate(light[0].position.x, light[0].position.y, light[0].position.z);
	//modelStack.Scale(0.1, 0.1, 0.1);
	//RenderMesh(meshList[GEO_LIGHTBALL], false);
	//modelStack.PopMatrix();

	RenderSkyBox();

	modelStack.PushMatrix();
		modelStack.Translate(14, 0, 10);
		modelStack.Scale(2, 2.5, 2);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderMesh(meshList[GEO_CHAIR], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
		modelStack.Translate(5, 0, 10);
		modelStack.Scale(2, 2.5, 2);
		modelStack.Rotate(90, 0, 1, 0);
		RenderMesh(meshList[GEO_CHAIR], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
		modelStack.Translate(10, 0, 10);
		modelStack.Scale(1.5, 2, 1.5);
		RenderMesh(meshList[GEO_TABLE], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-14, 3.2, 2);
	modelStack.Rotate(90, 0, 1, 0);
	modelStack.Scale(3, 2, 3);
	RenderMesh(meshList[GEO_BOOK], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-12, 0, 13);
	modelStack.Rotate(-90, 0, 1, 0);
	modelStack.Scale(1, 1, 1);
	RenderMesh(meshList[GEO_HOUSECHAIR], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-12, 0, 10);
	modelStack.Rotate(-90, 0, 1, 0);
	modelStack.Scale(1, 1, 1);
	RenderMesh(meshList[GEO_HOUSECHAIR], false);
	modelStack.PopMatrix();
	
	modelStack.PushMatrix();
	modelStack.Translate(-14, 3, 15);
	modelStack.Rotate(90, 0, 1, 0);
	modelStack.Scale(1, 2, 4);
	RenderMesh(meshList[GEO_SHELF], false);
	for (int i = 0; i < 28; i++)
	{
		modelStack.PushMatrix();
		modelStack.Translate(1.1, 0, 0);
		RenderMesh(meshList[GEO_SHELF], false);
	}

	for (int i = 0; i < 28; i++)
	{
		modelStack.PopMatrix();
	}
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	RenderMesh(meshList[GEO_CAMPFIRE], false);
	modelStack.PushMatrix();
	modelStack.Translate(-0.5, 2, 1);
	modelStack.Rotate(180, 0, 1, 0);
	modelStack.Rotate(firerotateAmt, 0, 1, 0);
	modelStack.Scale(0.5, 0.5, 1);
	RenderMesh(meshList[GEO_FIRE], false);
	modelStack.PopMatrix();
	modelStack.PopMatrix();

	if (collidedBook == true)
	{
		modelStack.PushMatrix();
		modelStack.Translate(-14, 3.2, 2);
		modelStack.Rotate(90, 0, 1, 0);
		modelStack.Scale(3, 2, 3);
		RenderTextOnScreen(meshList[GEO_TEXT], "Open Book", Color(0, 1, 0), 1.2, 30, 30);
		modelStack.PopMatrix();
	}

	if (bookContent == true)
	{
		modelStack.PushMatrix();
		modelStack.Translate(14, 0, 10);
		RenderMeshOnScreen(meshList[GEO_BOOKTEXT], 800, 500, 80, 50, 1, 0, 1, 1, 1);
		modelStack.PopMatrix();
	}


}


void House::AnimateFire(double dt)
{
	FireElapsedTime += dt;
	bool bSomethingHappened = false;
	if (g_dBounceTime > FireElapsedTime)
	{
		return;
	}
	else {
		if (flame == 1)
		{
			meshList[GEO_FIRE]->textureID = LoadTGA("Image//fire.tga");
			flame = 2;
			bSomethingHappened = true;
		}
		else
		{
			meshList[GEO_FIRE]->textureID = LoadTGA("Image//fire2.tga");
			flame = 1;
			bSomethingHappened = true;
		}
	}

	if (bSomethingHappened)
	{
		// set the bounce time to some time in the future to prevent accidental triggers
		g_dBounceTime = FireElapsedTime + 0.25; // 125ms should be enough
	}
}


void House::RenderMesh(Mesh *mesh, bool enableLight)
{
	Mtx44 MVP, modelView, modelView_inverse_transpose;

	MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);
	modelView = viewStack.Top() * modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MODELVIEW], 1, GL_FALSE, &modelView.a[0]);
	if (enableLight)
	{
		glUniform1i(m_parameters[U_LIGHTENABLED], 1);
		modelView_inverse_transpose = modelView.GetInverse().GetTranspose();
		glUniformMatrix4fv(m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE], 1, GL_FALSE, &modelView_inverse_transpose.a[0]);

		//load material
		glUniform3fv(m_parameters[U_MATERIAL_AMBIENT], 1, &mesh->material.kAmbient.r);
		glUniform3fv(m_parameters[U_MATERIAL_DIFFUSE], 1, &mesh->material.kDiffuse.r);
		glUniform3fv(m_parameters[U_MATERIAL_SPECULAR], 1, &mesh->material.kSpecular.r);
		glUniform1f(m_parameters[U_MATERIAL_SHININESS], mesh->material.kShininess);
	}
	else
	{
		glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	}
	if (mesh->textureID > 0)
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, mesh->textureID);
		glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	}
	else
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 0);
	}

	mesh->Render();
	if (mesh->textureID > 0)
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}

}

void House::RenderMeshOnScreen(Mesh* mesh, int x, int y, int sizex, int sizey, int sizez, int rotateAngle, int rX, int rY, int rZ)
{
	glDisable(GL_DEPTH_TEST);
	Mtx44 ortho;
	ortho.SetToOrtho(0, 1600, 0, 900, -10, 10); //size of screen UI
	projectionStack.PushMatrix();
	projectionStack.LoadMatrix(ortho);
	viewStack.PushMatrix();
	viewStack.LoadIdentity(); //No need camera for ortho mode
	modelStack.PushMatrix();
	modelStack.LoadIdentity();
	//to do: scale and translate accordingly
	modelStack.Translate(x, y, 0);
	modelStack.Rotate(rotateAngle, rX, rY, rZ);
	modelStack.Scale(sizex, sizey, sizez);

	modelStack.Scale(1, 1, 1);

	RenderMesh(mesh, false); //UI should not have light
	projectionStack.PopMatrix();
	viewStack.PopMatrix();
	modelStack.PopMatrix();
	glEnable(GL_DEPTH_TEST);
}

void House::RenderSkyBox()
{
	modelStack.PushMatrix();
		modelStack.Translate(0, 15, -15);
		modelStack.Scale(3.03, 3.03, 3.03);
		RenderMesh(meshList[GEO_FRONT], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
		modelStack.Translate(0, 15, 15);
		modelStack.Scale(3.03, 3.03, 3.03);
		modelStack.Rotate(180, 0.0f, 1.0f, 0.0f);
		RenderMesh(meshList[GEO_BACK], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
		modelStack.Translate(0, 20, 0);
		modelStack.Scale(3.03, 3.03, 3.03);
		modelStack.Rotate(90, 1.0f, 0.0f, 0.0f);
		modelStack.Rotate(90, 0.0f, 0.0f, 1.0f);
		RenderMesh(meshList[GEO_TOP], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
		modelStack.Translate(0, 0, 0);
		modelStack.Scale(3.03, 3.03, 3.03);
		modelStack.Rotate(-90, 1.0f, 0.0f, 0.0f);
		modelStack.Rotate(-90, 0.0f, 0.0f, 1.0f);
		RenderMesh(meshList[GEO_BOTTOM], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
		modelStack.Translate(-15, 15, 0);
		modelStack.Scale(3.03, 3.03, 3.03);
		modelStack.Rotate(90, 0.0f, 1.0f, 0.0f);
		RenderMesh(meshList[GEO_LEFT], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
		modelStack.Translate(15, 15, 0);
		modelStack.Scale(3.03, 3.03, 3.03);
		modelStack.Rotate(-90, 0.0f, 1.0f, 0.0f);
		RenderMesh(meshList[GEO_RIGHT], false);
	modelStack.PopMatrix();



}

void House::RenderText(Mesh* mesh, std::string text, Color color)
{
	if (!mesh || mesh->textureID <= 0) //Proper error check
		return;

	glDisable(GL_DEPTH_TEST);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 1);
	glUniform3fv(m_parameters[U_TEXT_COLOR], 1, &color.r);
	glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh->textureID);
	glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);

	for (unsigned i = 0; i < text.length(); ++i)
	{
		Mtx44 characterSpacing;
		characterSpacing.SetToTranslation(i * 1.0f, 0, 0); //1.0f is the spacing of each character, you may change this value
		Mtx44 MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top() * characterSpacing;
		glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);

		mesh->Render((unsigned)text[i] * 6, 6);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 0);
	glEnable(GL_DEPTH_TEST);
}

void House::RenderTextOnScreen(Mesh* mesh, std::string text, Color color, float size, float x, float y)
{
	if (!mesh || mesh->textureID <= 0) //Proper error check
		return;

	glDisable(GL_DEPTH_TEST);

	Mtx44 ortho;
	ortho.SetToOrtho(0, 80, 0, 60, -10, 10); //size of screen UI
	projectionStack.PushMatrix();
	projectionStack.LoadMatrix(ortho);
	viewStack.PushMatrix();
	viewStack.LoadIdentity(); //No need camera for ortho mode
	modelStack.PushMatrix();
	modelStack.LoadIdentity(); //Reset modelStack
	modelStack.Scale(size, size, size);
	modelStack.Translate(x, y, 0);

	glUniform1i(m_parameters[U_TEXT_ENABLED], 1);
	glUniform3fv(m_parameters[U_TEXT_COLOR], 1, &color.r);
	glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh->textureID);
	glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);

	for (unsigned i = 0; i < text.length(); ++i)
	{
		Mtx44 characterSpacing;
		characterSpacing.SetToTranslation(i * 1.0f, 0, 0); //1.0f is the spacing of each character, you may change this value
		Mtx44 MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top() * characterSpacing;
		glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);

		mesh->Render((unsigned)text[i] * 6, 6);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 0);
	projectionStack.PopMatrix();
	viewStack.PopMatrix();
	modelStack.PopMatrix();
	glEnable(GL_DEPTH_TEST);
}

void House::Exit()
{
	// Cleanup VBO here
	for (int i = 0; i < NUM_GEOMETRY; ++i)
	{
		if (meshList[i] != NULL)
			delete meshList[i];

		meshList[i] = NULL;
	}

	glDeleteVertexArrays(1, &m_vertexArrayID);

	glDeleteProgram(m_programID);

}
