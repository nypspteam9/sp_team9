#include "Wolf.h"
#include <time.h>
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>


Wolf::Wolf()
{
	//Rand their starting position
	srand(time(NULL));
	position.x = (rand() % 70 + 120); // Change to fit the boundary
	position.y = 1;
	position.z = -(rand() % 70 + 120);
	MOVEMENT_SPEED = 0.2f;
	dir = FORWARD;
	//Sleep(500);
	moving = true;
	Alive = true;
	cowllided = false;
	WolvesState = IDLE;
	Health = 3;
	legmoving = RIGHTLEG;
	leftlegangle = 0;
	rightlegangle = 0;
	damage = 0.2;
}
Wolf::~Wolf()
{

}

void Wolf::setDirection(Object o[], Camera cam)
{
	int x = rand() % 400 + 1;
	switch (dir) {
	case FORWARD:
		switch (x)
		{
		case 1:
			dir = BACKWARD;
			break;
		case 2:
			dir = RIGHT;
			break;
		case 3:
			dir = LEFT;
			break;
		case 4:
			dir = FORWARDLEFT;
			break;
		case 5:
			dir = FORWARDRIGHT;
			break;
		case 6:
			dir = BACKWARDLEFT;
			break;
		case 7:
			dir = BACKWARDRIGHT;
			break;
		default:
			dir = FORWARD;
			break;
		}
		break;
	case BACKWARD:
		switch (x)
		{
		case 1:
			dir = FORWARD;
			break;
		case 2:
			dir = RIGHT;
			break;
		case 3:
			dir = LEFT;
			break;
		case 4:
			dir = FORWARDLEFT;
			break;
		case 5:
			dir = FORWARDRIGHT;
			break;
		case 6:
			dir = BACKWARDLEFT;
			break;
		case 7:
			dir = BACKWARDRIGHT;
			break;
		default:
			dir = BACKWARD;
			break;
		}
		break;
	case RIGHT:
		switch (x)
		{
		case 1:
			dir = FORWARD;
			break;
		case 2:
			dir = BACKWARD;
			engine->play2D("../Music/dog_bark.mp3", false);
			break;
		case 3:
			dir = LEFT;
			break;
		case 4:
			dir = FORWARDLEFT;
			engine->play2D("../Music/dog_bark.mp3", false);
			break;
		case 5:
			dir = FORWARDRIGHT;
			break;
		case 6:
			dir = BACKWARDLEFT;
			break;
		case 7:
			dir = BACKWARDRIGHT;
			engine->play2D("../Music/dog_bark.mp3", false);
			break;
		default:
			dir = RIGHT;
			break;
		}
		break;
	case LEFT:
		switch (x)
		{
		case 1:
			dir = FORWARD;
			break;
		case 2:
			dir = BACKWARD;
			break;
		case 3:
			dir = RIGHT;
			break;
		case 4:
			dir = FORWARDLEFT;
			break;
		case 5:
			dir = FORWARDRIGHT;
			break;
		case 6:
			dir = BACKWARDLEFT;
			break;
		case 7:
			dir = BACKWARDRIGHT;
			break;
		default:
			dir = LEFT;
			break;
		}
		break;
	case FORWARDLEFT:
		switch (x)
		{
		case 1:
			dir = FORWARD;
			break;
		case 2:
			dir = BACKWARD;
			break;
		case 3:
			dir = RIGHT;
			break;
		case 4:
			dir = LEFT;
			break;
		case 5:
			dir = FORWARDRIGHT;
			break;
		case 6:
			dir = BACKWARDLEFT;
			break;
		case 7:
			dir = BACKWARDRIGHT;
			break;
		default:
			dir = FORWARDLEFT;
			break;
		}
		break;
	case FORWARDRIGHT:
		switch (x)
		{
		case 1:
			dir = FORWARD;
			break;
		case 2:
			dir = BACKWARD;
			break;
		case 3:
			dir = RIGHT;
			break;
		case 4:
			dir = LEFT;
			break;
		case 5:
			dir = FORWARDLEFT;
			break;
		case 6:
			dir = BACKWARDLEFT;
			break;
		case 7:
			dir = BACKWARDRIGHT;
			break;
		default:
			dir = FORWARDRIGHT;
			break;
		}
		break;
	case BACKWARDLEFT:
		switch (x)
		{
		case 1:
			dir = FORWARD;
			break;
		case 2:
			dir = BACKWARD;
			break;
		case 3:
			dir = RIGHT;
			break;
		case 4:
			dir = LEFT;
			break;
		case 5:
			dir = FORWARDLEFT;
			break;
		case 6:
			dir = FORWARDRIGHT;
			break;
		case 7:
			dir = BACKWARDRIGHT;
			break;
		default:
			dir = BACKWARDLEFT;
			break;
		}
		break;
	case BACKWARDRIGHT:
		switch (x)
		{
		case 1:
			dir = FORWARD;
			break;
		case 2:
			dir = BACKWARD;
			break;
		case 3:
			dir = RIGHT;
			break;
		case 4:
			dir = LEFT;
			break;
		case 5:
			dir = FORWARDLEFT;
			break;
		case 6:
			dir = FORWARDRIGHT;
			break;
		case 7:
			dir = BACKWARDLEFT;
			break;
		default:
			dir = BACKWARDRIGHT;
			break;
		}
		break;
	}

	inrange(cam);
	if (Health > 0)
	move(dir, o, cam);
}

void Wolf::inrange(Camera cam)
{
	if (((cam.position.x + view.x) >= 70) && ((cam.position.x + view.x) <= 190))
	{
		if (((cam.position.z + view.z) >= -188.5) && ((cam.position.z + view.z) <= 190))
		{
			WolvesState = CHASING;
			
		}
	}
	else
	{
		WolvesState = IDLE;
	}

}

void Wolf::move(DIRECTION dir, Object o[], Camera cam)
{
	if (WolvesState == IDLE)
	{
		switch (dir)
		{
		case FORWARD:
			view.Set(0, 0, -1);
			rotateAmt = 0;
			break;
		case BACKWARD:
			view.Set(0, 0, 1);
			rotateAmt = 180;
			break;
		case LEFT:
			view.Set(-1, 0, 0);
			rotateAmt = 90;
			break;
		case RIGHT:
			view.Set(1, 0, 0);
			rotateAmt = -90;
			break;
		case FORWARDLEFT:
			view.Set(-1, 0, -1);
			rotateAmt = 45;
			break;
		case FORWARDRIGHT:
			view.Set(1, 0, -1);
			rotateAmt = -45;
			break;
		case BACKWARDLEFT:
			view.Set(-1, 0, 1);
			rotateAmt = 135;
			break;
		case BACKWARDRIGHT:
			view.Set(1, 0, 1);
			rotateAmt = -135;
			break;
		}
		for (int i = 0; i < 54; i++)
		{
			if (((position.x + view.x) >= o[i].minX) && ((position.x + view.x) <= o[i].maxX))
			{
				if (((position.z + view.z) >= o[i].minZ) && ((position.z + view.z) <= o[i].maxZ))
				{
					moving = false;
					break;
				}
			}
		}
		if (moving)
		{
			if (((position.x + view.x) >= 70) && ((position.x + view.x) <= 190))
			{
				if (((position.z + view.z) >= -188.5) && ((position.z + view.z) <= 190))
				{
					position = position + view * MOVEMENT_SPEED;
				}
			}
		}
		else
		{
			moving = true;
		}
	}

	if (WolvesState == CHASING)
	{
		//Calculate view vecctor of wolf to player
		view = (cam.position - position).Normalized(); //Target is Camera's position, - position of wolf and normalize to get view

		//Moving
	
		for (int i = 0; i < 54; i++)
		{
			if (((position.x + view.x) >= o[i].minX) && ((position.x + view.x) <= o[i].maxX))
			{
				if (((position.z + view.z) >= o[i].minZ) && ((position.z + view.z) <= o[i].maxZ))
				{
					moving = false;
				}
			}
		}
		if (moving)
		{
			/*if (position.x > cam.position.x)
			{
				position.x -= 0.2;
			}
			if (position.x < cam.position.x)
			{
				position.x += 0.2;
			}
			if (position.z > cam.position.z)
			{
				position.z -= 0.2;
			}
			if (position.z < cam.position.z)
			{
				position.z += 0.2;
			}*/
			position.x = position.x + view.x * MOVEMENT_SPEED;
			position.z = position.z + view.z * MOVEMENT_SPEED;

		}
		else
		{
			moving = true;
		}
		
	}

}

void Wolf::Animate(double dt)
{
	if (this->moving)
	{
		if (legmoving == LEFTLEG)
		{
			leftlegangle += dt * 40;
			rightlegangle -= dt * 40;
			if (leftlegangle >= 15)
			{
				legmoving = RIGHTLEG;
			}
		}
		else if (legmoving == RIGHTLEG)
		{
			leftlegangle -= dt * 40;
			rightlegangle += dt * 40;
			if (rightlegangle >= 15)
			{
				legmoving = LEFTLEG;
			}
		}
	}
	else
	{
		leftlegangle = 0;
		rightlegangle = 0;
	}
}