#include "SceneText.h"
#include "GL\glew.h"

#include "shader.hpp"
#include "MatrixStack.h"
#include "Mtx44.h"
#include "Application.h"
#include "Utility.h"
#include "LoadTGA.h"

#include "Box.h"

SceneText::SceneText()
{

}

SceneText::~SceneText()
{

}

void SceneText::Init()
{

	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Generate a default VAO for now
	glGenVertexArrays(1, &m_vertexArrayID);
	glBindVertexArray(m_vertexArrayID);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	camera.Init(Vector3(1, 4, 1), Vector3(0, 4, 0), Vector3(0, 1, 0));

	game = false;
	instructions = false;
	quit = false;

	//Load vertex and fragment shaders
	//m_programID = LoadShaders("Shader//SimpleVertexShader.vertexshader", "Shader//SimpleFragmentShader.fragmentshader");
	//m_programID = LoadShaders("Shader//Shading.vertexshader", "Shader//Shading.fragmentshader");
	m_programID = LoadShaders("Shader//Shading.vertexshader", "Shader//LightSource.fragmentshader");
	m_programID = LoadShaders("Shader//Texture.vertexshader", "Shader//Texture.fragmentshader");																				// Get a handle for our "colorTexture" uniform
	m_programID = LoadShaders("Shader//Texture.vertexshader", "Shader//Blending.fragmentshader");
	m_programID = LoadShaders("Shader//Texture.vertexshader", "Shader//Text.fragmentshader");
	m_parameters[U_COLOR_TEXTURE_ENABLED] = glGetUniformLocation(m_programID, "colorTextureEnabled");
	m_parameters[U_COLOR_TEXTURE] = glGetUniformLocation(m_programID, "colorTexture");
	m_parameters[U_NUMLIGHTS] = glGetUniformLocation(m_programID, "numLights");
	m_parameters[U_MVP] = glGetUniformLocation(m_programID, "MVP");
	m_parameters[U_MODELVIEW] = glGetUniformLocation(m_programID, "MV");
	m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE] = glGetUniformLocation(m_programID, "MV_inverse_transpose");
	m_parameters[U_MATERIAL_AMBIENT] = glGetUniformLocation(m_programID, "material.kAmbient");
	m_parameters[U_MATERIAL_DIFFUSE] = glGetUniformLocation(m_programID, "material.kDiffuse");
	m_parameters[U_MATERIAL_SPECULAR] = glGetUniformLocation(m_programID, "material.kSpecular");
	m_parameters[U_MATERIAL_SHININESS] = glGetUniformLocation(m_programID, "material.kShininess");
	m_parameters[U_LIGHTENABLED] = glGetUniformLocation(m_programID, "lightEnabled");
	m_parameters[U_TEXT_ENABLED] = glGetUniformLocation(m_programID, "textEnabled");
	m_parameters[U_TEXT_COLOR] = glGetUniformLocation(m_programID, "textColor");

	glUseProgram(m_programID);

	glUniform1i(m_parameters[U_NUMLIGHTS], 1);

	// Enable depth tests


	Mtx44 projection;
	projection.SetToPerspective(45.f, 4.f / 3.f, 0.1f, 1000.f);
	projectionStack.LoadMatrix(projection);

	for (int i = 0; i < NUM_GEOMETRY; ++i)
	{
		meshList[i] = NULL;
	}

	meshList[GEO_AXES] = MeshBuilder::GenerateAxes("reference", 2000, 2000, 2000);

	meshList[GEO_MENU] = MeshBuilder::GenerateQuad("Menu", Color(0, 0, 0), 5.f, 5.f);

	meshList[GEO_INSTRUCTION] = MeshBuilder::GenerateQuad("Instructions", Color(0, 0, 0), 8.0f, 6.0f);
	meshList[GEO_INSTRUCTION]->textureID = LoadTGA("Image//instruction screen.tga");


	meshList[GEO_TEXT] = MeshBuilder::GenerateText("text", 16, 16);
	meshList[GEO_TEXT]->textureID = LoadTGA("Image//calibri.tga");
}


void SceneText::Update(double dt)
{
	system("CLS");
	static const float LSPEED = 50.0f;
	std::cout << camera.xMouse << std::endl;
	std::cout << camera.yMouse << std::endl;
	if ((camera.xMouse >= 580) && (camera.xMouse <= 1030))
	{
		if ((camera.yMouse >= 370) && (camera.yMouse <= 400))
		{
			game = true;
			instructions = false;
			quit = false;
		}
	}
	if (Application::IsKeyPressed('1'))
	{
		game = true;
		instructions = false;
		quit = false;
	}
	
	if ((camera.xMouse >= 580) && (camera.xMouse <= 1150))
	{
		if ((camera.yMouse >= 435) && (camera.yMouse <= 465))
		{
			game = false;
			instructions = true;
			quit = false;
		}
	}
	if (Application::IsKeyPressed('2'))
	{
		game = false;
		instructions = true;
		quit = false;
	}
	if ((camera.xMouse >= 580) && (camera.xMouse <= 1040))
	{
		if ((camera.yMouse >= 495) && (camera.yMouse <= 520))
		{
			game = false;
			instructions = false;
			quit = true;
		}
	}
	if (Application::IsKeyPressed('3'))
	{
		game = false;
		instructions = true;
		quit = false;
	}

	if (instructionMenu == false && instructions == true && Application::IsKeyPressed(VK_RETURN))
	{
		instructionMenu = true;
	}

	if (instructionMenu == true && instructions == true && Application::IsKeyPressed(VK_BACK))
	{
		instructionMenu = false;
	}

	camera.Update(dt);

	//BoundsCheck();
}

void SceneText::Render()
{
	//Clear color & depth buffer every frame
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	viewStack.LoadIdentity();
	viewStack.LookAt(camera.position.x, camera.position.y, camera.position.z, camera.target.x, camera.target.y, camera.target.z, camera.up.x, camera.up.y, camera.up.z);
	modelStack.LoadIdentity();

	modelStack.PushMatrix();
	RenderMeshOnScreen(meshList[GEO_MENU], 5, 5, 5, 5);
	modelStack.PopMatrix();

	if (instructionMenu == false)
	{
		if (game == true)
		{
			RenderTextOnScreen(meshList[GEO_TEXT], ">Play Game<", Color(0, 1, 0), 2, 14, 17);
		}
		else
		{
			RenderTextOnScreen(meshList[GEO_TEXT], "Play Game", Color(1, 0, 0), 2, 15, 17);
		}
		if (instructions == true)
		{
			RenderTextOnScreen(meshList[GEO_TEXT], ">Instructions<", Color(0, 1, 0), 2, 14, 15);
		}
		else
		{
			RenderTextOnScreen(meshList[GEO_TEXT], "Instructions", Color(1, 0, 0), 2, 15, 15);
		}
		if (quit == true)
		{
			RenderTextOnScreen(meshList[GEO_TEXT], ">Quit Game<", Color(0, 1, 0), 2, 14, 13);
		}
		else
		{
			RenderTextOnScreen(meshList[GEO_TEXT], "Quit Game", Color(1, 0, 0), 2, 15, 13);
		}
	}

	if (instructionMenu == true)
	{

		RenderMeshOnScreen(meshList[GEO_INSTRUCTION], 40, 30, 1, 1);
	}

}

void SceneText::RenderMesh(Mesh *mesh, bool enableLight)
{
	Mtx44 MVP, modelView, modelView_inverse_transpose;

	MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);
	modelView = viewStack.Top() * modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MODELVIEW], 1, GL_FALSE, &modelView.a[0]);
	if (enableLight)
	{
		glUniform1i(m_parameters[U_LIGHTENABLED], 1);
		modelView_inverse_transpose = modelView.GetInverse().GetTranspose();
		glUniformMatrix4fv(m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE], 1, GL_FALSE, &modelView_inverse_transpose.a[0]);

		//load material
		glUniform3fv(m_parameters[U_MATERIAL_AMBIENT], 1, &mesh->material.kAmbient.r);
		glUniform3fv(m_parameters[U_MATERIAL_DIFFUSE], 1, &mesh->material.kDiffuse.r);
		glUniform3fv(m_parameters[U_MATERIAL_SPECULAR], 1, &mesh->material.kSpecular.r);
		glUniform1f(m_parameters[U_MATERIAL_SHININESS], mesh->material.kShininess);
	}
	else
	{
		glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	}
	if (mesh->textureID > 0)
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, mesh->textureID);
		glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	}
	else
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 0);
	}

	mesh->Render();
	if (mesh->textureID > 0)
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}

}

void SceneText::RenderMeshOnScreen(Mesh* mesh, int x, int y, int sizex, int sizey)
{
	glDisable(GL_DEPTH_TEST);
	Mtx44 ortho;
	ortho.SetToOrtho(0, 80, 0, 60, -10, 10); //size of screen UI
	projectionStack.PushMatrix();
	projectionStack.LoadMatrix(ortho);
	viewStack.PushMatrix();
	viewStack.LoadIdentity(); //No need camera for ortho mode
	modelStack.PushMatrix();
	modelStack.LoadIdentity();
	//to do: scale and translate accordingly
	modelStack.Translate(x, y, 0);
	modelStack.Scale(sizex, sizey, 0);
	//modelStack.Rotate(90, 0, 0, 0);

	modelStack.Scale(1, 1, 1);

	RenderMesh(mesh, false); //UI should not have light
	projectionStack.PopMatrix();
	viewStack.PopMatrix();
	modelStack.PopMatrix();
	glEnable(GL_DEPTH_TEST);
}


void SceneText::RenderTextOnScreen(Mesh* mesh, std::string text, Color color, float size, float x, float y)
{
	if (!mesh || mesh->textureID <= 0) //Proper error check
		return;

	glDisable(GL_DEPTH_TEST);

	Mtx44 ortho;
	ortho.SetToOrtho(0, 80, 0, 60, -10, 10); //size of screen UI
	projectionStack.PushMatrix();
	projectionStack.LoadMatrix(ortho);
	viewStack.PushMatrix();
	viewStack.LoadIdentity(); //No need camera for ortho mode
	modelStack.PushMatrix();
	modelStack.LoadIdentity(); //Reset modelStack
	modelStack.Scale(size, size, size);
	modelStack.Translate(x, y, 0);

	glUniform1i(m_parameters[U_TEXT_ENABLED], 1);
	glUniform3fv(m_parameters[U_TEXT_COLOR], 1, &color.r);
	glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh->textureID);
	glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);

	for (unsigned i = 0; i < text.length(); ++i)
	{
		Mtx44 characterSpacing;
		characterSpacing.SetToTranslation(i * 1.0f, 0, 0); //1.0f is the spacing of each character, you may change this value
		Mtx44 MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top() * characterSpacing;
		glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);

		mesh->Render((unsigned)text[i] * 6, 6);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 0);
	projectionStack.PopMatrix();
	viewStack.PopMatrix();
	modelStack.PopMatrix();
	glEnable(GL_DEPTH_TEST);
}

void SceneText::Exit()
{
	// Cleanup VBO here
	for (int i = 0; i < NUM_GEOMETRY; ++i)
	{
		if (meshList[i] != NULL)
			delete meshList[i];

		meshList[i] = NULL;
	}

	glDeleteVertexArrays(1, &m_vertexArrayID);

	glDeleteProgram(m_programID);

}
