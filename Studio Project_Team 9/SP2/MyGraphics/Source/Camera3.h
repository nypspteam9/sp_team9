#ifndef CAMERA_3_H
#define CAMERA_3_H

#include "Camera.h"
#include "Collision.h"

class Camera3 : public Camera
{
public:
	//Vector3 position;
	//Vector3 target;
	//Vector3 up;

	Vector3 defaultPosition;
	Vector3 defaultTarget;
	Vector3 defaultUp;
	Vector3 view;

	double xMouse;
	double yMouse;
	float MOVEMENT_SPEED;
	bool move;

	enum state {
		JUMPING_UP,
		JUMPING_DOWN,
		JUMP_MAX,
		ON_GROUND
	};
	state currentstate;

	Camera3();
	~Camera3();
	virtual void Init(const Vector3& pos, const Vector3& target, const Vector3& up);
	virtual void Update(double dt, Object o[]);
	virtual void Reset();
	void setMovement(float x);

	void Jump(double dt);

	
	int jumpLimit;
	double airtime;
	float jumpHeight;
};

#endif