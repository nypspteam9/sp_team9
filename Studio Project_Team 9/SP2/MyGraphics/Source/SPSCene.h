
#ifndef SPSCENE_H
#define SPSCENE_H

#include "Scene.h"
#include "Camera3.h"
#include "Camera2.h"
#include "Mesh.h"
#include "MeshBuilder.h"
#include "MatrixStack.h"
#include "Light.h"
#include "Box.h"
#include <string>
#include "Collision.h"
#include "Puzzles.h"
#include "Animal.h"
#include "Player.h"
#include "Cow.h"
#include "Wolf.h"
#include "irrKlang.h"

#pragma comment(lib, "irrKlang.lib")

using namespace irrklang;



class SPScene : public Scene
{
	enum UNIFORM_TYPE
	{
		U_MVP = 0,
		U_MODELVIEW,
		U_MODELVIEW_INVERSE_TRANSPOSE,
		U_MATERIAL_AMBIENT,
		U_MATERIAL_DIFFUSE,
		U_MATERIAL_SPECULAR,
		U_MATERIAL_SHININESS,
		U_LIGHT0_POSITION,
		U_LIGHT0_COLOR,
		U_LIGHT0_POWER,
		U_LIGHT0_KC,
		U_LIGHT0_KL,
		U_LIGHT0_KQ,
		U_LIGHTENABLED,
		U_LIGHT0_TYPE,
		U_LIGHT0_SPOTDIRECTION,
		U_LIGHT0_COSCUTOFF,
		U_LIGHT0_COSINNER,
		U_LIGHT0_EXPONENT,
		U_LIGHT1_POSITION,
		U_LIGHT1_COLOR,
		U_LIGHT1_POWER,
		U_LIGHT1_KC,
		U_LIGHT1_KL,
		U_LIGHT1_KQ,
		U_LIGHT1_TYPE,
		U_LIGHT1_SPOTDIRECTION,
		U_LIGHT1_COSCUTOFF,
		U_LIGHT1_COSINNER,
		U_LIGHT1_EXPONENT,
		/*U_LIGHT2_POSITION,
		U_LIGHT2_COLOR,
		U_LIGHT2_POWER,
		U_LIGHT2_KC,
		U_LIGHT2_KL,
		U_LIGHT2_KQ,
		U_LIGHT2_TYPE,
		U_LIGHT2_SPOTDIRECTION,
		U_LIGHT2_COSCUTOFF,
		U_LIGHT2_COSINNER,
		U_LIGHT2_EXPONENT,*/
		U_NUMLIGHTS,
		U_COLOR_TEXTURE_ENABLED,
		U_COLOR_TEXTURE,
		U_TEXT_ENABLED,
		U_TEXT_COLOR,
		U_TOTAL,
	};


	enum GEOMETRY_TYPE
	{
		GEO_AXES,
		GEO_QUAD,
		GEO_CUBE,
		GEO_CIRCLE,
		GEO_RING,
		GEO_SPHERE,
		GEO_SPHERE1,
		GEO_SPHERE2,
		GEO_LIGHTBALL,
		GEO_LIGHTBALL2,
		GEO_LEFT,
		GEO_RIGHT,
		GEO_TOP,
		GEO_BOTTOM,
		GEO_FRONT,
		GEO_BACK,
		GEO_MENU,
		GEO_TEXT,
		GEO_ARM,
		GEO_NIGHTLEFT,
		GEO_NIGHTRIGHT,
		GEO_NIGHTTOP,
		GEO_NIGHTBOTTOM,
		GEO_NIGHTFRONT,
		GEO_NIGHTBACK,
		////////////////////////////////////////////////////////////////////////
		////////////////////////////Our Models//////////////////////////////////
		////////////////////////////////////////////////////////////////////////
		GEO_TREE,
		GEO_PALMTREE,
		GEO_FISHINGROD,
		GEO_PICKAXE,
		GEO_AXE,
		GEO_HAMMER,
		GEO_TORCH,
		GEO_TORCHFLAME,
		GEO_ROCK,
		GEO_ROCK2,
		GEO_ROCK3,
		GEO_ROCK4,
		GEO_MAP,
		GEO_FENCE,
		GEO_PUZZLESCREEN,
		GEO_PUZZLEBOARD,
		GEO_HOUSE,
		GEO_PET,
		GEO_COW,
		GEO_COWLEFT,
		GEO_COWRIGHT,
		GEO_COWLEG,
		GEO_WOLF,
		GEO_WOLFLEFT,
		GEO_WOLFRIGHT,
		GEO_WOLFLEG,
		GEO_HOTBAR,
		GEO_HOTBARSELECTION,
		GEO_MACHETEHB,
		GEO_AXEHB,
		GEO_HAMMERHB,
		GEO_PICKHB,
		GEO_MEATHB,
		GEO_FISHHB,
		GEO_BENCH,
		GEO_MACHETE,
		GEO_BACKGROUNDDESIGN,
		GEO_BACKGROUND,
		GEO_FISHINGBAR,
		NUM_GEOMETRY,
	};

public:
	SPScene();
	~SPScene();

	virtual void Init();
	virtual void Update(double dt);
	void UpdateTorchPosition();
	void AnimateTorch(double dt);
	virtual void Render();
	virtual void Exit();

	void RenderHotbar();
	float g_dBounceTime;
	int flame = 1;
	float FireElapsedTime = 0.0;

	int selection = 0;

	//Tree Interactions
	bool collidedTree1;
	bool collidedTree2;
	bool collidedTree3;
	bool collidedTree4;
	bool collidedTree5;
	bool collidedStone1;
	bool collidedStone2;
	bool collidedStone3;
	bool collidedStone4;
	bool collidedStone5;
	bool collidedStone6;
	bool collidedStone7;
	bool collidedFishing;
	bool tree1;
	bool tree2;
	bool tree3;
	bool tree4;
	bool tree5;
	bool stone1;
	bool stone2;
	bool stone3;
	bool stone4;
	bool stone5;
	bool stone6;
	bool stone7;

	bool tree1Dead;
	bool tree2Dead;
	bool tree3Dead;
	bool tree4Dead;
	bool tree5Dead;

	float rotateTree1;
	float rotateTree2;
	float rotateTree3;
	float rotateTree4;
	float rotateTree5;

	int tree1hp;
	int tree2hp;
	int tree3hp;
	int tree4hp;
	int tree5hp;
	int stone1hp;
	int stone2hp;
	int stone3hp;
	int stone4hp;
	int stone5hp;
	int stone6hp;
	int stone7hp;
	bool resourceClicked;
	bool FishingGame;
	bool FishingClicked;
	bool ybarmove;
	bool gotfish;
	int ybar;
	int barspeed;
	float delay;

	//Crafting Interactions
	bool inventoryMenu;
	bool collidedCrafting;
	bool craftingOptions;
	bool displayCraftItems;
	bool toolOptions;
	bool craftItems;
	bool foundWood;
	bool foundStone;
	bool foundFish;
	bool foundMeat;
	bool haveAxe;
	bool havePickaxe;
	bool haveHammer;
	bool haveMachete;
	bool haveFishingRod;
	bool haveTorch;

	int wood;
	int stone;
	int fish;
	int meat;

	std::string logcount;
	std::string stoneCount;
	std::string fishCount;
	std::string meatCount;

	////////////////////////////////////////////////////////////////////////
	////////////////////////////MainMenuVar/////////////////////////////////
	////////////////////////////////////////////////////////////////////////
	bool Menu;
	bool Pause;
	bool Play;
	bool Quit;
	bool Resume;
	////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////
	//Weapon files
	float axerotate;
	bool axetrigger;

	/********************************************************************/
	float Health;
	float Stamina;
	float Hunger;
	float timerTree;
	float timerStone;
	float timerFish;
	float fishingIdle;
	float timerEat;
	float timerEatMeat;
	float eatingIdle;
	float eatingIdleMeat;
	bool timerStartTree;
	bool timerStartStone;
	int Direction;
	//float timer2;

	//Tool Booleans
	bool HoldingPick;
	bool HoldingAxe;
	bool HoldingFishingRod;
	bool HoldingHammer;
	bool HoldingFish;
	bool HoldingMeat;
	bool HoldingTorch = true;
	bool HoldingMachete;

	std::string Healthcount;
	std::string Staminacount;
	std::string Hungercount;


	//DayNight Cycle
	bool night;
	bool changeSkyBox;

	//Puzzles
	bool collidedPuzzle1;
	bool collidedPuzzle2;
	bool RenderPuzzle;
	float puzzleRespawn;
	float puzzleRespawn2;
	float power;

	CPuzzles* Puzzle1;
	CPuzzles* Puzzle2;
	std::string puzzleAns;

	//Animal stuff
	Animal* Animal[10];
	bool clicked;
	float timer1;
//	Animal* Wolf[2];
	float CowRotate;
	
	Player player;


	Camera3 camera;

private:

	double g_dElapsedTime = 0.0;



	unsigned m_vertexArrayID;
	Mesh* meshList[NUM_GEOMETRY];
	unsigned m_programID;
	unsigned m_parameters[U_TOTAL];

	MS modelStack, viewStack, projectionStack;

	Light light[2];

	Object object[67];
	Object AI[10];

	Vector3 Axemovement;
	Vector3 ArmMovement;
	Vector3 objTarget;
	Vector3 petObject;

	void RenderMesh(Mesh *mesh, bool enableLight);

	void RenderFishingGame();
	void RenderSkyBox();
	void RenderTrees();
	void RenderStones();
	void RenderStatus();
	void RenderMeshOnScreen(Mesh * mesh, int x, int y, int sizex, int sizey, int sizez, int rotateAngle, int rX, int rY, int rZ);
	void RenderText(Mesh* mesh, std::string text, Color color);
	void RenderTextOnScreen(Mesh* mesh, std::string text, Color color, float size, float x, float y);
	void RenderAxeArm();
	void RenderAI();
	void RotateAI(float dt);
	void MenuUI();
	void BoundsCheck();
	void ResourceCollision();
	void PuzzleCollision();
	void PuzzleCheck(double dt);
	void RenderStoneProp();
	void InitCollision();
	void UpdateCollision();
	void InitCollisionAI();
	void UpdateCollisionAI();
	void getAngle(float &angle, Vector3 posP, Vector3 objPos);
	float dotProductDistance(Vector3 positionPlayer, Vector3 positionObstacle);
	void UpdateLight(const Vector3& pos, const Vector3& up, const Vector3& target, float dt);
	ISoundEngine* engine = createIrrKlangDevice();
};

#endif
