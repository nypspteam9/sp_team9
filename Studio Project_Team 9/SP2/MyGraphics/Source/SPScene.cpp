#include "SPScene.h"
#include "GL\glew.h"

#include "shader.hpp"
#include "MatrixStack.h"
#include "Mtx44.h"
#include "Application.h"
#include "Utility.h"
#include "LoadTGA.h"


SPScene::SPScene()
{

}

SPScene::~SPScene()
{

}

void SPScene::Init()
{
	//Seed srand
	srand(time(NULL));
	//Init puzzles
	Puzzle1 = new CPuzzles;
	Puzzle1->setPuzzlenum(1);
	Puzzle2 = new CPuzzles;
	Puzzle2->setPuzzlenum(2);

	//for (int i = 0; i < 8; i++)
	//{
	//	Animal[i] = NULL;
	//}

	//Animal[0] = new Cow;

	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Generate a default VAO for now
	glGenVertexArrays(1, &m_vertexArrayID);

	glBindVertexArray(m_vertexArrayID);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	camera.Init(Vector3(1, 4, 1), Vector3(0, 4, 0), Vector3(0, 1, 0));

	InitCollision();

	
	clicked = false;
	FishingGame = false;
	night = true;
	changeSkyBox = false;
	player = { 100, 100, 100 };
	timerTree = 20.0f;
	timerStone = 20.0f;
	timerFish = 5.0f;
	fishingIdle = 0.0f;
	timerEat = 5.0f;
	timerEatMeat = 5.0f;
	eatingIdle = 0.0f;
	eatingIdleMeat = 0.0f;
	timerStartTree = true;
	timerStartStone = true;
	rotateTree1 = 0.0f;
	rotateTree2 = 0.0f;
	rotateTree3 = 0.0f;
	rotateTree4 = 0.0f;
	rotateTree5 = 0.0f;
	CowRotate = 0.0f;
	ybar = 500;
	delay = 0;
	barspeed = 30;
	//ybarmove = true;

	//Pause = false;
	//------------------------
	//
	HoldingPick = true;
	HoldingAxe = false;
	HoldingFishingRod = true;
	HoldingHammer = false;
	HoldingTorch = false;
	HoldingMachete = false;

	haveTorch = false;
	haveAxe = false;
	havePickaxe = false;
	haveFishingRod = true;
	haveHammer = false;
	haveMachete = false;

	FishingClicked = false;

	tree1hp = 4;
	tree2hp = 4;
	tree3hp = 4;
	tree4hp = 4;
	tree5hp = 4;

	stone1hp = 3;
	stone2hp = 3;
	stone3hp = 3;
	stone4hp = 3;
	stone5hp = 3;
	stone6hp = 3;
	stone7hp = 3;

	//Axemovement.Set(0.05, -0.08, -0.1);
	Axemovement.Set(0.03, -0.07, -0.1);

	//Load vertex and fragment shaders
	m_programID = LoadShaders("Shader//Shading.vertexshader", "Shader//LightSource.fragmentshader");
	m_programID = LoadShaders("Shader//Texture.vertexshader", "Shader//Texture.fragmentshader");
	m_programID = LoadShaders("Shader//Texture.vertexshader", "Shader//Blending.fragmentshader");
	m_programID = LoadShaders("Shader//Texture.vertexshader", "Shader//Text.fragmentshader");
	m_parameters[U_LIGHTENABLED] = glGetUniformLocation(m_programID, "lightEnabled");
	m_parameters[U_TEXT_ENABLED] = glGetUniformLocation(m_programID, "textEnabled");
	m_parameters[U_TEXT_COLOR] = glGetUniformLocation(m_programID, "textColor");
	m_parameters[U_COLOR_TEXTURE_ENABLED] = glGetUniformLocation(m_programID, "colorTextureEnabled");
	m_parameters[U_COLOR_TEXTURE] = glGetUniformLocation(m_programID, "colorTexture");
	m_parameters[U_NUMLIGHTS] = glGetUniformLocation(m_programID, "numLights");
	m_parameters[U_MVP] = glGetUniformLocation(m_programID, "MVP");
	m_parameters[U_MODELVIEW] = glGetUniformLocation(m_programID, "MV");
	m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE] = glGetUniformLocation(m_programID, "MV_inverse_transpose");
	m_parameters[U_MATERIAL_AMBIENT] = glGetUniformLocation(m_programID, "material.kAmbient");
	m_parameters[U_MATERIAL_DIFFUSE] = glGetUniformLocation(m_programID, "material.kDiffuse");
	m_parameters[U_MATERIAL_SPECULAR] = glGetUniformLocation(m_programID, "material.kSpecular");
	m_parameters[U_MATERIAL_SHININESS] = glGetUniformLocation(m_programID, "material.kShininess");

	m_parameters[U_LIGHT0_TYPE] = glGetUniformLocation(m_programID, "lights[0].type");
	m_parameters[U_LIGHT0_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[0].spotDirection");
	m_parameters[U_LIGHT0_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[0].cosCutoff");
	m_parameters[U_LIGHT0_COSINNER] = glGetUniformLocation(m_programID, "lights[0].cosInner");
	m_parameters[U_LIGHT0_EXPONENT] = glGetUniformLocation(m_programID, "lights[0].exponent");
	m_parameters[U_LIGHT0_POSITION] = glGetUniformLocation(m_programID, "lights[0].position_cameraspace");
	m_parameters[U_LIGHT0_COLOR] = glGetUniformLocation(m_programID, "lights[0].color");
	m_parameters[U_LIGHT0_POWER] = glGetUniformLocation(m_programID, "lights[0].power");
	m_parameters[U_LIGHT0_KC] = glGetUniformLocation(m_programID, "lights[0].kC");
	m_parameters[U_LIGHT0_KL] = glGetUniformLocation(m_programID, "lights[0].kL");
	m_parameters[U_LIGHT0_KQ] = glGetUniformLocation(m_programID, "lights[0].kQ");

	m_parameters[U_LIGHT1_TYPE] = glGetUniformLocation(m_programID, "lights[1].type");
	m_parameters[U_LIGHT1_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[1].spotDirection");
	m_parameters[U_LIGHT1_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[1].cosCutoff");
	m_parameters[U_LIGHT1_COSINNER] = glGetUniformLocation(m_programID, "lights[1].cosInner");
	m_parameters[U_LIGHT1_EXPONENT] = glGetUniformLocation(m_programID, "lights[1].exponent");
	m_parameters[U_LIGHT1_POSITION] = glGetUniformLocation(m_programID, "lights[1].position_cameraspace");
	m_parameters[U_LIGHT1_COLOR] = glGetUniformLocation(m_programID, "lights[1].color");
	m_parameters[U_LIGHT1_POWER] = glGetUniformLocation(m_programID, "lights[1].power");
	m_parameters[U_LIGHT1_KC] = glGetUniformLocation(m_programID, "lights[1].kC");
	m_parameters[U_LIGHT1_KL] = glGetUniformLocation(m_programID, "lights[1].kL");
	m_parameters[U_LIGHT1_KQ] = glGetUniformLocation(m_programID, "lights[1].kQ");
	
	glUseProgram(m_programID);
	glUniform1i(m_parameters[U_NUMLIGHTS], 2);

	//Enable depth tests
	Mtx44 projection;
	projection.SetToPerspective(45.f, 4.f / 3.f, 0.1f, 1000.f);
	projectionStack.LoadMatrix(projection);

	for (int i = 0; i < NUM_GEOMETRY; ++i)
	{
		meshList[i] = NULL;
	}

	meshList[GEO_AXES] = MeshBuilder::GenerateAxes("reference", 2000, 2000, 2000);
	meshList[GEO_QUAD] = MeshBuilder::GenerateQuad("quad", Color(0.0f,0.0f,0.0f), 10, 10);

	meshList[GEO_LIGHTBALL] = MeshBuilder::GenerateSphere("Sphere", Color(1.0f, 1.0f, 1.0f), 15, 60, 2);
	meshList[GEO_LIGHTBALL2] = MeshBuilder::GenerateSphere("Sphere", Color(1.0f, 1.0f, 1.0f), 15, 60, 2);

	meshList[GEO_FRONT] = MeshBuilder::GenerateQuad("front", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_FRONT]->textureID = LoadTGA("Image//miramar_bk.tga");

	meshList[GEO_BACK] = MeshBuilder::GenerateQuad("back", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_BACK]->textureID = LoadTGA("Image//miramar_ft.tga");

	meshList[GEO_LEFT] = MeshBuilder::GenerateQuad("left", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_LEFT]->textureID = LoadTGA("Image//miramar_lf.tga");

	meshList[GEO_RIGHT] = MeshBuilder::GenerateQuad("right", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_RIGHT]->textureID = LoadTGA("Image//miramar_rt.tga");

	meshList[GEO_TOP] = MeshBuilder::GenerateQuad("top", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_TOP]->textureID = LoadTGA("Image//miramar_up.tga");

	meshList[GEO_BOTTOM] = MeshBuilder::GenerateQuad("bottom", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_BOTTOM]->textureID = LoadTGA("Image//miramar_dn.tga");



	meshList[GEO_NIGHTFRONT] = MeshBuilder::GenerateQuad("front", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_NIGHTFRONT]->textureID = LoadTGA("Image//nighttime_bk.tga");

	meshList[GEO_NIGHTBACK] = MeshBuilder::GenerateQuad("back", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_NIGHTBACK]->textureID = LoadTGA("Image//nighttime_ft.tga");

	meshList[GEO_NIGHTLEFT] = MeshBuilder::GenerateQuad("left", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_NIGHTLEFT]->textureID = LoadTGA("Image//nighttime_lf.tga");

	meshList[GEO_NIGHTRIGHT] = MeshBuilder::GenerateQuad("right", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_NIGHTRIGHT]->textureID = LoadTGA("Image//nighttime_rt.tga");

	meshList[GEO_NIGHTTOP] = MeshBuilder::GenerateQuad("top", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_NIGHTTOP]->textureID = LoadTGA("Image//nighttime_up.tga");

	meshList[GEO_NIGHTBOTTOM] = MeshBuilder::GenerateQuad("bottom", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_NIGHTBOTTOM]->textureID = LoadTGA("Image//nighttime_dn.tga");



	meshList[GEO_TEXT] = MeshBuilder::GenerateText("text", 16, 16);
	meshList[GEO_TEXT]->textureID = LoadTGA("Image//calibri.tga");

	meshList[GEO_MENU] = MeshBuilder::GenerateQuad("Menu", Color(0, 0, 0), 1.f, 1.f);
	meshList[GEO_MENU]->textureID = LoadTGA("Image//Black.tga");

	meshList[GEO_AXE] = MeshBuilder::GenerateOBJ("Axe", "OBJ//WoodAxe.obj");
	meshList[GEO_AXE]->textureID = LoadTGA("Image//WoodAxe.tga");

	meshList[GEO_PICKAXE] = MeshBuilder::GenerateOBJ("Pickaxe", "OBJ//PickAxe.obj");
	meshList[GEO_PICKAXE]->textureID = LoadTGA("Image//PickAxe.tga");

	meshList[GEO_MACHETE] = MeshBuilder::GenerateOBJ("Pickaxe", "OBJ//machete.obj");
	meshList[GEO_MACHETE]->textureID = LoadTGA("Image//machete.tga");

	meshList[GEO_TORCH] = MeshBuilder::GenerateOBJ("Torch", "OBJ//Torch_OBJ.obj");
	meshList[GEO_TORCH]->textureID = LoadTGA("Image//Torch_UV_Textured.tga");

	meshList[GEO_TORCHFLAME] = MeshBuilder::GenerateOBJ("TorchFlame", "OBJ//torchflame.obj");
	meshList[GEO_TORCHFLAME]->textureID = LoadTGA("Image//torchflame.tga");

	meshList[GEO_ARM] = MeshBuilder::GenerateOBJ("Arm", "OBJ//Arm.obj");
	meshList[GEO_ARM]->textureID = LoadTGA("Image//yellow.tga");

	meshList[GEO_FISHINGROD] = MeshBuilder::GenerateOBJ("FishingRod", "OBJ//Fishing_rod.obj");
	meshList[GEO_FISHINGROD]->textureID = LoadTGA("Image//SP2_MAYA_(24bit)Fishingrod_UVsnapshot.tga");

	meshList[GEO_MAP] = MeshBuilder::GenerateOBJ("Ground", "OBJ//map obj.obj");
	meshList[GEO_MAP]->textureID = LoadTGA("Image//SP2_MAYA_Updated_Map_UVsnapshot(test).tga");
	meshList[GEO_MAP]->material.kSpecular.Set(0.0f, 0.0f, 0.0f);
	meshList[GEO_MAP]->material.kAmbient.Set(0.05f, 0.05f, 0.05f);
	meshList[GEO_MAP]->material.kDiffuse.Set(0.99f, 0.99f, 0.99f);

	meshList[GEO_TREE] = MeshBuilder::GenerateOBJ("Tree", "OBJ//tree.obj");
	meshList[GEO_TREE]->textureID = LoadTGA("Image//tree.tga");
	meshList[GEO_TREE]->material.kSpecular.Set(0.0f, 0.0f, 0.0f);
	meshList[GEO_TREE]->material.kAmbient.Set(0.05f, 0.05f, 0.05f);
	meshList[GEO_TREE]->material.kDiffuse.Set(0.4f, 0.4f, 0.4f);

	meshList[GEO_FENCE] = MeshBuilder::GenerateOBJ("Fence", "OBJ//SP2_WoodFence.obj");
	meshList[GEO_FENCE]->textureID = LoadTGA("Image//SP2_MAYA_WoodFence_UVsnapshot.tga");
	meshList[GEO_FENCE]->material.kSpecular.Set(0.0f, 0.0f, 0.0f);
	meshList[GEO_FENCE]->material.kAmbient.Set(0.05f, 0.05f, 0.05f);
	meshList[GEO_FENCE]->material.kDiffuse.Set(0.99f, 0.99f, 0.99f);

	meshList[GEO_PALMTREE] = MeshBuilder::GenerateOBJ("PalmTree", "OBJ//palmtree.obj");
	meshList[GEO_PALMTREE]->textureID = LoadTGA("Image//PalmTree.tga");
	meshList[GEO_PALMTREE]->material.kSpecular.Set(0.0f, 0.0f, 0.0f);
	meshList[GEO_PALMTREE]->material.kAmbient.Set(0.05f, 0.05f, 0.05f);
	meshList[GEO_PALMTREE]->material.kDiffuse.Set(0.99f, 0.99f, 0.99f);

	meshList[GEO_ROCK] = MeshBuilder::GenerateOBJ("Rock1", "OBJ//rock1.obj");
	meshList[GEO_ROCK]->textureID = LoadTGA("Image//rock1.tga");
	meshList[GEO_ROCK]->material.kSpecular.Set(0.0f, 0.0f, 0.0f);
	meshList[GEO_ROCK]->material.kAmbient.Set(0.05f, 0.05f, 0.05f);
	meshList[GEO_ROCK]->material.kDiffuse.Set(0.99f, 0.99f, 0.99f);

	meshList[GEO_ROCK2] = MeshBuilder::GenerateOBJ("Rock2", "OBJ//rock2.obj");
	meshList[GEO_ROCK2]->textureID = LoadTGA("Image//rock2.tga");
	meshList[GEO_ROCK2]->material.kSpecular.Set(0.0f, 0.0f, 0.0f);
	meshList[GEO_ROCK2]->material.kAmbient.Set(0.05f, 0.05f, 0.05f);
	meshList[GEO_ROCK2]->material.kDiffuse.Set(0.99f, 0.99f, 0.99f);

	meshList[GEO_ROCK3] = MeshBuilder::GenerateOBJ("Rock3", "OBJ//rock3.obj");
	meshList[GEO_ROCK3]->textureID = LoadTGA("Image//rock3.tga");
	meshList[GEO_ROCK3]->material.kSpecular.Set(0.0f, 0.0f, 0.0f);
	meshList[GEO_ROCK3]->material.kAmbient.Set(0.05f, 0.05f, 0.05f);
	meshList[GEO_ROCK3]->material.kDiffuse.Set(0.99f, 0.99f, 0.99f);

	meshList[GEO_ROCK4] = MeshBuilder::GenerateOBJ("Rock4", "OBJ//rock4.obj");
	meshList[GEO_ROCK4]->textureID = LoadTGA("Image//rock4.tga");
	meshList[GEO_ROCK4]->material.kSpecular.Set(0.0f, 0.0f, 0.0f);
	meshList[GEO_ROCK4]->material.kAmbient.Set(0.05f, 0.05f, 0.05f);
	meshList[GEO_ROCK4]->material.kDiffuse.Set(0.99f, 0.99f, 0.99f);

	meshList[GEO_HOUSE] = MeshBuilder::GenerateOBJ("House", "OBJ//House.obj");
	meshList[GEO_HOUSE]->textureID = LoadTGA("Image//House.tga");
	meshList[GEO_HOUSE]->material.kSpecular.Set(0.0f, 0.0f, 0.0f);
	meshList[GEO_HOUSE]->material.kAmbient.Set(0.05f, 0.05f, 0.05f);
	meshList[GEO_HOUSE]->material.kDiffuse.Set(0.99f, 0.99f, 0.99f);

	meshList[GEO_PUZZLESCREEN] = MeshBuilder::GenerateQuad("Puzzle", Color(), 1.f, 1.f);
	meshList[GEO_PUZZLESCREEN]->textureID = LoadTGA("Image//puzzlescreen.tga");

	meshList[GEO_PUZZLEBOARD] = MeshBuilder::GenerateOBJ("Puzzleboard", "OBJ//puzzleboard.obj");
	meshList[GEO_PUZZLEBOARD]->textureID = LoadTGA("Image//puzzleboard.tga");
	meshList[GEO_PUZZLEBOARD]->material.kSpecular.Set(0.0f, 0.0f, 0.0f);
	meshList[GEO_PUZZLEBOARD]->material.kAmbient.Set(0.05f, 0.05f, 0.05f);
	meshList[GEO_PUZZLEBOARD]->material.kDiffuse.Set(0.99f, 0.99f, 0.99f);

	meshList[GEO_HOTBAR] = MeshBuilder::GenerateQuad("Hotbar", Color(), 1.f, 1.f);
	meshList[GEO_HOTBAR]->textureID = LoadTGA("Image//hotbar.tga");

	meshList[GEO_HOTBARSELECTION] = MeshBuilder::GenerateQuad("HotbarSelection", Color(), 1.f, 1.f);
	meshList[GEO_HOTBARSELECTION]->textureID = LoadTGA("Image//hotbarselection.tga");

	meshList[GEO_AXEHB] = MeshBuilder::GenerateQuad("AxeHB", Color(), 1.f, 1.f);
	meshList[GEO_AXEHB]->textureID = LoadTGA("Image//AxeHB.tga");

	meshList[GEO_PICKHB] = MeshBuilder::GenerateQuad("PickHB", Color(), 1.f, 1.f);
	meshList[GEO_PICKHB]->textureID = LoadTGA("Image//PickHB.tga");

	meshList[GEO_HAMMERHB] = MeshBuilder::GenerateQuad("HammerHB", Color(), 1.f, 1.f);
	meshList[GEO_HAMMERHB]->textureID = LoadTGA("Image//HammerHB.tga");

	meshList[GEO_MACHETEHB] = MeshBuilder::GenerateQuad("MacheteHB", Color(), 1.f, 1.f);
	meshList[GEO_MACHETEHB]->textureID = LoadTGA("Image//MacheteHB.tga");

	meshList[GEO_MEATHB] = MeshBuilder::GenerateQuad("MeatHB", Color(), 1.f, 1.f);
	meshList[GEO_MEATHB]->textureID = LoadTGA("Image//MeatHB.tga");

	meshList[GEO_FISHHB] = MeshBuilder::GenerateQuad("FishHB", Color(), 1.f, 1.f);
	meshList[GEO_FISHHB]->textureID = LoadTGA("Image//FishHB.tga");


	meshList[GEO_COW] = MeshBuilder::GenerateOBJ("Cow", "OBJ//CowBody.obj");
	meshList[GEO_COW]->textureID = LoadTGA("Image//BodyTexture.tga");
	meshList[GEO_COW]->material.kSpecular.Set(0.0f, 0.0f, 0.0f);
	meshList[GEO_COW]->material.kAmbient.Set(0.05f, 0.05f, 0.05f);
	meshList[GEO_COW]->material.kDiffuse.Set(0.99f, 0.99f, 0.99f);


	meshList[GEO_COWLEG] = MeshBuilder::GenerateOBJ("CowLegs", "OBJ//CowLeg.obj");
	meshList[GEO_COWLEG]->textureID = LoadTGA("Image//LegTexture.tga");
	meshList[GEO_COWLEG]->material.kSpecular.Set(0.0f, 0.0f, 0.0f);
	meshList[GEO_COWLEG]->material.kAmbient.Set(0.05f, 0.05f, 0.05f);
	meshList[GEO_COWLEG]->material.kDiffuse.Set(0.99f, 0.99f, 0.99f);


	meshList[GEO_WOLF] = MeshBuilder::GenerateOBJ("Wolf", "OBJ//WolfBody.obj");
	meshList[GEO_WOLF]->textureID = LoadTGA("Image//WolfFur.tga");
	meshList[GEO_WOLF]->material.kSpecular.Set(0.0f, 0.0f, 0.0f);
	meshList[GEO_WOLF]->material.kAmbient.Set(0.05f, 0.05f, 0.05f);
	meshList[GEO_WOLF]->material.kDiffuse.Set(0.99f, 0.99f, 0.99f);


	meshList[GEO_WOLFLEG] = MeshBuilder::GenerateOBJ("WolfLegs", "OBJ//WolfLeg.obj");
	meshList[GEO_WOLFLEG]->textureID = LoadTGA("Image//WolfFur.tga");
	meshList[GEO_WOLFLEG]->material.kSpecular.Set(0.0f, 0.0f, 0.0f);
	meshList[GEO_WOLFLEG]->material.kAmbient.Set(0.05f, 0.05f, 0.05f);
	meshList[GEO_WOLFLEG]->material.kDiffuse.Set(0.99f, 0.99f, 0.99f);

	meshList[GEO_PET] = MeshBuilder::GenerateOBJ("Pet", "OBJ//Animal_Pet.obj");
	meshList[GEO_PET]->textureID = LoadTGA("Image//Animal_pet_UV.tga");

	meshList[GEO_BENCH] = MeshBuilder::GenerateOBJ("WorkBench", "OBJ//WorkBench.obj");
	meshList[GEO_BENCH]->textureID = LoadTGA("Image//WorkBench Texture.tga");

	meshList[GEO_BACKGROUND] = MeshBuilder::GenerateQuad("BarBackground", Color(0.82, 0.7, 0.55), 1, 6 );

	meshList[GEO_FISHINGBAR] = MeshBuilder::GenerateQuad("Fishing Bar", Color(0, 1, 0), 1, 1);

	meshList[GEO_BACKGROUNDDESIGN] = MeshBuilder::GenerateQuad("BarDesign", Color(0.95, 0.64, 0.375), 1, 6);

	light[0].type = Light::LIGHT_POINT;
	light[0].position.Set(200, 1, 1);
	light[0].target.Set(0, 1, 0);
	light[0].up.Set(0, 1, 0);
	light[0].DefaultPosition.Set(300, 1, 1);
	light[0].color.Set(1, 1, 1);
	light[0].power = 10 + power;
	light[0].kC = 1.f;
	light[0].kL = 0.01f;
	light[0].kQ = 0.001f;
	light[0].cosCutoff = cos(Math::DegreeToRadian(180));
	light[0].cosInner = cos(Math::DegreeToRadian(160));
	light[0].exponent = 3.f;
	light[0].spotDirection.Set(0.f, 1.f, 0.f);

	light[1].type = Light::LIGHT_POINT;
	light[1].position.Set(camera.position.x, camera.position.y, camera.position.z);
	light[1].target.Set(0, 1, 0);
	light[1].up.Set(0, 1, 0);
	light[1].DefaultPosition.Set(1, 1, 1);
	light[1].color.Set(1, 1, 1);
	light[1].power = 3;
	light[1].kC = 1.f;
	light[1].kL = 0.01f;
	light[1].kQ = 0.001f;
	light[1].cosCutoff = cos(Math::DegreeToRadian(180));
	light[1].cosInner = cos(Math::DegreeToRadian(160));
	light[1].exponent = 3.f;
	light[1].spotDirection.Set(0.f, 1.f, 0.f);
	
	for (int i = 0; i < 8; i++)
	{
		if (Animal[i] == NULL)
			Animal[i] = new Cow;
			Sleep(1000);

	}
	for (int i = 8; i < 10; i++)
	{
		if (Animal[i] == NULL)
			Animal[i] = new Wolf;
			Sleep(125);
	}
	InitCollisionAI();
		

	// Make sure you pass uniform parameters after glUseProgram()

	glUniform1i(m_parameters[U_LIGHT0_TYPE], light[0].type);
	glUniform3fv(m_parameters[U_LIGHT0_COLOR], 1, &light[0].color.r);
	glUniform1f(m_parameters[U_LIGHT0_POWER], light[0].power);
	glUniform1f(m_parameters[U_LIGHT0_KC], light[0].kC);
	glUniform1f(m_parameters[U_LIGHT0_KL], light[0].kL);
	glUniform1f(m_parameters[U_LIGHT0_KQ], light[0].kQ);
	glUniform1f(m_parameters[U_LIGHT0_COSCUTOFF], light[0].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT0_COSINNER], light[0].cosInner);
	glUniform1f(m_parameters[U_LIGHT0_EXPONENT], light[0].exponent);
	glUniform3fv(m_parameters[U_LIGHT0_COLOR], 1, &light[0].color.r);
	glUniform1f(m_parameters[U_LIGHT0_KC], light[0].kC);
	glUniform1f(m_parameters[U_LIGHT0_KL], light[0].kL);
	glUniform1f(m_parameters[U_LIGHT0_KQ], light[0].kQ);

	glUniform1i(m_parameters[U_LIGHT1_TYPE], light[1].type);
	glUniform3fv(m_parameters[U_LIGHT1_COLOR], 1, &light[1].color.r);
	glUniform1f(m_parameters[U_LIGHT1_POWER], light[1].power);
	glUniform1f(m_parameters[U_LIGHT1_KC], light[1].kC);
	glUniform1f(m_parameters[U_LIGHT1_KL], light[1].kL);
	glUniform1f(m_parameters[U_LIGHT1_KQ], light[1].kQ);
	glUniform1f(m_parameters[U_LIGHT1_COSCUTOFF], light[1].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT1_COSINNER], light[1].cosInner);
	glUniform1f(m_parameters[U_LIGHT1_EXPONENT], light[1].exponent);
	glUniform3fv(m_parameters[U_LIGHT1_COLOR], 1, &light[1].color.r);
	glUniform1f(m_parameters[U_LIGHT1_KC], light[1].kC);
	glUniform1f(m_parameters[U_LIGHT1_KL], light[1].kL);
	glUniform1f(m_parameters[U_LIGHT1_KQ], light[1].kQ);

	glUniform1i(m_parameters[U_NUMLIGHTS], 2);

}

void SPScene::InitCollision()
{
	object[0].boxset(Vector3(50, 1, 60), 1, 1, 1);
	object[1].boxset(Vector3(35, 1, 90), 1, 1, 1);
	object[2].boxset(Vector3(40, 1, 40), 1, 1, 1);
	object[3].boxset(Vector3(25, 1, 760), 1, 1, 1);
	object[4].boxset(Vector3(5, 1, 75), 1, 1, 1);
	object[5].boxset(Vector3(-12, 1, 55), 1, 1, 1);
	object[6].boxset(Vector3(-40, 1, 85), 1, 1, 1);
	object[7].boxset(Vector3(-38, 1, 35), 1, 1, 1);
	object[8].boxset(Vector3(-32, 1, -10), 1, 1, 1);
	object[9].boxset(Vector3(-17, 1, -35), 1, 1, 1);
	object[10].boxset(Vector3(-7, 1, -75), 1, 1, 1);
	object[11].boxset(Vector3(17, 1, 120), 1, 1, 1);
	object[12].boxset(Vector3(35, 1, -85), 1, 1, 1);
	object[13].boxset(Vector3(50, 1, -70), 1, 1, 1);
	object[14].boxset(Vector3(45, 1, -40), 1, 1, 1);
	object[15].boxset(Vector3(-25, 1, -25), 1, 1, 1);
	object[16].boxset(Vector3(13, 1, 15), 1, 1, 1);
	object[17].boxset(Vector3(30, 1, -2), 1, 1, 1);
	//Check for these objects

	object[18].boxset(Vector3(10, 1, -65), 1, 1, 1); //Working
	object[19].boxset(Vector3(1, 1, -38), 1, 1, 1);
	object[20].boxset(Vector3(18, 1, -32), 1, 1, 1);
	object[21].boxset(Vector3(16, 1, -120), 1, 1, 1);//Working
	object[22].boxset(Vector3(2, 1, -39), 1, 1, 1);
	object[23].boxset(Vector3(17, 1, -32), 1, 1, 1);
	object[24].boxset(Vector3(-10, 1, -15), 1, 1, 1); //Working
	object[25].boxset(Vector3(27, 1, -97), 1, 1, 1);
	object[26].boxset(Vector3(31, 1, -115), 1, 1, 1);
	object[27].boxset(Vector3(-5, 1, 30), 1, 1, 1); // Working
	object[28].boxset(Vector3(13, 1, 9), 1, 1, 1); //working
	object[29].boxset(Vector3(21, 1, 49), 1, 1, 1);//working
	object[30].boxset(Vector3(-9, 1, 99), 1, 1, 1); // working
	object[31].boxset(Vector3(5, 1, 55), 1, 1, 1);//working
	object[32].boxset(Vector3(1, 1, 124), 1, 1, 1);//working

												   //STONES
	object[33].boxset(Vector3(26.4, 1, 96), 5, 5, 5);
	object[34].boxset(Vector3(-12, 1, 81), 5, 5, 5);
	object[35].boxset(Vector3(45, 1, 152), 5, 5, 5);
	object[36].boxset(Vector3(102, 1, 98), 5, 5, 5);
	object[37].boxset(Vector3(139, 1, 60), 5, 5, 5);
	object[38].boxset(Vector3(100, 1, 145), 5, 5, 5);
	object[39].boxset(Vector3(5.6, 1, -116), 5, 5, 5);
	object[40].boxset(Vector3(-49.4, 1, -65.5), 5, 5, 5);
	object[41].boxset(Vector3(-72, 1, 55.6), 5, 5, 5);
	object[42].boxset(Vector3(-43.7, 1, 134.6), 5, 5, 5);
	object[43].boxset(Vector3(65, 1, 95), 5, 5, 5);
	object[44].boxset(Vector3(78, 1, 120), 5, 5, 5);
	object[45].boxset(Vector3(65, 1, 69), 5, 5, 5);
	object[46].boxset(Vector3(90, 1, 120), 5, 5, 5);
	object[64].boxset(Vector3(143, 1, -74), 5, 5, 5);
	object[65].boxset(Vector3(135, 1, -1.5), 5, 5, 5);
	object[66].boxset(Vector3(139, 1, 69), 5, 5, 5);

	//House
	object[47].boxset(Vector3(-44, 1, 14), 9, 9, 9);

	//Fences
	object[48].boxset(Vector3(-33.1, 1, 190), 4, 50, 4);
	object[49].boxset(Vector3(-2.9, 1, 189.7), 4, 50, 4);
	object[50].boxset(Vector3(-26.1, 1, 142), 6, 4, 4);
	object[51].boxset(Vector3(-9.5, 1, 141.8), 6, 4, 4);
	object[52].boxset(Vector3(114.8, 1, 165), 2, 36, 2);
	object[53].boxset(Vector3(133, 1, 130.2), 12, 4, 4);
	object[54].boxset(Vector3(118.1, 1, 129.8), 4, 2, 2);
	object[55].boxset(Vector3(193.2, 1, 130), 43, 2, 2);
	object[56].boxset(Vector3(-84.8, 1, 55), 2, 270, 2);
	object[57].boxset(Vector3(-43.4, 1, -145.7), 30, 30, 30);
	object[58].boxset(Vector3(194.8, 1, 24.5), 2, 270, 2);
	object[59].boxset(Vector3(1.9, 1, 195.1), 270, 2, 2);
	object[60].boxset(Vector3(38.8, 1, -193.1), 270, 2, 2);
	//tree gate
	object[61].boxset(Vector3(-18.6, 1, 142), 4, 4, 2);
	//stone gate
	object[62].boxset(Vector3(147.8, 1, 130), 4, 4, 2);
	//crafting table
	object[63].boxset(Vector3(-35, 1, 11.4), 4, 4, 2);
	

}

void SPScene::UpdateCollision()
{
	object[61].boxset(Vector3(-18.6 + Puzzle1->puzzleXpos, 1, 142), 4, 4, 2);
	//stone gate
	object[62].boxset(Vector3(147.8 + Puzzle2->puzzleXpos, 1, 130), 4, 4, 2);

}

void SPScene::InitCollisionAI()
{
	for (int i = 0; i < 10; i++)
	{
		if (Animal[i] != NULL)
		AI[i].boxset((Animal[i]->position), 5, 5, 2);
	}
}

void SPScene::UpdateCollisionAI()
{
	for (int i = 0; i < 10; i++)
	{
		if(Animal[i] != NULL)
		AI[i].boxset((Animal[i]->position), 7, 7, 3);
	}
}

void SPScene::UpdateLight(const Vector3& pos, const Vector3& up, const Vector3& target, float dt)
{
	float pitch = (float)(-2 * dt);
	Vector3 view = (target - pos).Normalized();
	Vector3 right = view.Cross(up);
	right.y = 0;
	right.Normalize();
	light[0].up = right.Cross(view);
	Mtx44 rotation;
	rotation.SetToRotation(pitch, right.x, right.y, right.z);
	light[0].DefaultPosition = rotation * light[0].DefaultPosition;
	light[0].position.Set(light[0].DefaultPosition.x, light[0].DefaultPosition.y, light[0].DefaultPosition.z);
}

bool IsPointInBox(Vector3 position, Box box)
{
	if ((position.x >= box.minX && position.x <= box.maxX)
		&& (position.y >= box.minY && position.y <= box.maxY)
		&& (position.z >= box.minZ && position.z <= box.maxZ))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool AIInteraction(Vector3 position, Object box)
{
	if ((position.x >= box.minX && position.x <= box.maxX) && (position.z >= box.minZ && position.z <= box.maxZ))
	{
		return true;
	}
	else
	{
		return false;
	}
}

void SPScene::BoundsCheck()
{
	// Tree
	Box Tree = Box(Vector3(-15, 4, 175), 4);
	collidedTree1 = IsPointInBox(camera.position, Tree);

	Box Tree2 = Box(Vector3(-20, 4, 165), 4);
	collidedTree2 = IsPointInBox(camera.position, Tree2);

	Box Tree3 = Box(Vector3(-15, 4, 158), 4);
	collidedTree3 = IsPointInBox(camera.position, Tree3);

	Box Tree4 = Box(Vector3(-9, 4, 159), 4);
	collidedTree4 = IsPointInBox(camera.position, Tree4);

	Box Tree5 = Box(Vector3(-25, 4, 180), 4);
	collidedTree5 = IsPointInBox(camera.position, Tree5);


	// Stone
	Box Stone1 = Box(Vector3(150, 4, 186), 7);
	collidedStone1 = IsPointInBox(camera.position, Stone1);

	Box Stone2 = Box(Vector3(166, 4, 177), 7);
	collidedStone2 = IsPointInBox(camera.position, Stone2);

	Box Stone3 = Box(Vector3(130, 4, 149), 7);
	collidedStone3 = IsPointInBox(camera.position, Stone3);

	Box Stone4 = Box(Vector3(164, 4, 150), 7);
	collidedStone4 = IsPointInBox(camera.position, Stone4);

	Box Stone5 = Box(Vector3(145.8, 4, 165.4), 7);
	collidedStone5 = IsPointInBox(camera.position, Stone5);

	Box Stone6 = Box(Vector3(148, 4, 141), 7);
	collidedStone6 = IsPointInBox(camera.position, Stone6);

	Box Stone7 = Box(Vector3(127.7, 4, 175.7), 7);
	collidedStone7 = IsPointInBox(camera.position, Stone7);

	// Craft
	Box Craft = Box(Vector3(-35, 1, 11.4), 6);
	collidedCrafting = IsPointInBox(camera.position, Craft);

	//Puzzle
	Box Puzzle1 = Box(Vector3(-17.8, 0, 141.8), 7);
	collidedPuzzle1 = IsPointInBox(camera.position, Puzzle1);

	Box Puzzle2 = Box(Vector3(147.6, 0, 129.6), 7);
	collidedPuzzle2 = IsPointInBox(camera.position, Puzzle2);

	//Fishing Boundary
	Box timeToFish = Box(Vector3(-51.7, 4, -143.7), 40);
	collidedFishing = IsPointInBox(camera.position, timeToFish);

}

void SPScene::Update(double dt)
{
	system("CLS");
	UpdateCollision();

	static const float LSPEED = 50.0f;

	////////////////////////////////////////////////////////////////////////////////////////////
	//THIS FOR AI MOVEMENT AND CHECKING
	for (int i = 0; i < 10; i++)
	{
		if (Animal[i] != NULL)
		Animal[i]->setDirection(object, camera);
	}
	for (int i = 0; i < 10; i++)
	{
		if (Animal[i] != NULL)
		{
			if (i <= 7)
			{
				(static_cast<Cow*>(Animal[i]))->Animate(dt);
			}
			else
			{
				(static_cast<Wolf*>(Animal[i]))->Animate(dt);
			}
		}

	}
	RotateAI(dt);
	UpdateCollisionAI();

	for (int i = 0; i < 10; i++)
	{
		if (Animal[i] != NULL)
		{
			Animal[i]->cowllided = AIInteraction(camera.position, AI[i]);
		}
	}


	if (Application::IsKeyPressed(VK_LBUTTON) )
	{
		if (!clicked)
		{
			for (int i = 0; i < 10; i++)
			{
				if (Animal[i] != NULL)
				{
					if (Animal[i]->cowllided)
					{
						Animal[i]->Health -= 1;
						clicked = true;
						engine->play2D("../Music/hitmarker_sound.mp3", false);
						break;
					}
					//break;
				}
			}

		}
	}
	else
	{
		clicked = false;
	}

	/*for (int i = 0; i < 10; i++)
	{
		std::cout << Animal[i]->Health << std::endl;
	}*/
	for (int i = 0; i < 10; i++)
	{
		if (Animal[i] != NULL)
		{
			if (Animal[i]->cowllided) //For damage from wolves
			{
				player.Health -= Animal[i]->damage;
			}
			if (Animal[i]->Health <= 0) //Death animation for animals when they die
			{
				Animal[i]->deadrotate += 50 * dt;

			}
			if (Animal[i]->deadrotate >= 120) //Deleting animals after they rotate a certain amount
			{
				Animal[i]->Alive = false;
			}
			if (Animal[i]->Alive == false)// Deleting animals after they rotate a certain amount
			{
				if (HoldingMachete == true && haveMachete == true)
				{
					meat += 2;
					foundMeat = true;
				}
				else
				{
					meat++;
					foundMeat = true;
				}

				delete Animal[i];
				Animal[i] = NULL;
			}
			
		}
	}

	timer1 += dt;
	if (timer1 >= 20)
	{
		for (int i = 0; i < 8; i++)
		{
			if (Animal[i] == NULL)
			{
				Animal[i] = new Cow;
			}
		}
		for (int i = 8; i < 10; i++)
		{
			if (Animal[i] == NULL)
			{
				Animal[i] = new Wolf;
			}
		}
		timer1 = 0;
	}

	////////////////////////////////////////////////////////////////////////////////////////////
	float distancebetweenPlayer = dotProductDistance(camera.position, petObject);
	if (distancebetweenPlayer > 1)
	{
		petObject.x = petObject.x + objTarget.x * dt * 0.5;
		petObject.z = petObject.z + objTarget.z * dt * 0.5;
	}

	//DAY NIGHT CYCLE CODES	
	UpdateLight(light[0].DefaultPosition, light[0].up, light[0].target, dt);

	if (light[0].position.y >= 0)
	{
		if (light[0].power <= 200)
		{
			power += dt * 5;
			light[0].power = 10 + power;
			glUniform1f(m_parameters[U_LIGHT0_POWER], light[0].power);
		
		}
		light[0].target.Set(0, 1, 0);
		night = true;
		
	}
	if (light[0].position.x <= -250)
	{
		light[0].color.Set(0.976, 0.8356, 0.644);
		if (light[0].power >= 0)
		{
			power -= dt * 25;
			light[0].power = 10 + power;
			glUniform1f(m_parameters[U_LIGHT0_POWER], light[0].power);
			

		}
		light[0].target.Set(0, -40, 0);

		night = true;
	}
	//END OF DAY NIGHT CYCLE CODES


	//PLAYER AND ITEM FLOAT TO STRING

	logcount = std::to_string(wood);
	stoneCount = std::to_string(stone);
	fishCount = std::to_string(fish);
	meatCount = std::to_string(meat);

	Hungercount = std::to_string(player.Hunger);
	Healthcount = std::to_string(player.Health);
	Staminacount = std::to_string(player.Stamina);


	if (Application::IsKeyPressed(VK_SHIFT))
	{
		if (player.Stamina > 0)
		{
			camera.setMovement(0.8f);
			player.Stamina -= 0.4;
		}
		else
		{
			player.Stamina = 0;
			camera.setMovement(0.3f);
		}
	}
	else
	{
		camera.setMovement(0.3f);

		if (player.Stamina <= 100)
		{
			player.Stamina += 0.1;
			if (player.Stamina <= 0)
			{
				player.Stamina = 0;
				player.Stamina += 0.01;
			}
		}
		else
		{
			player.Stamina = 100;
		}
	}

	if (player.Hunger <= 0)
	{
		player.Health -= 0.01;
		player.Hunger = 0;
	}
	else
	{
		player.Hunger -= 0.01;
	}

	if (player.Health <= 0)
	{
		Application::endgame();
	}
	// RESOURCE UPDATE SECTION

	if (tree1 == true || tree2 == false || tree3 == false || tree4 == false || tree5 == false)
	{
		if (tree1Dead == true)
		{
			rotateTree1 += (float)(80 * dt);
		}
		if (tree2Dead == true)
		{
			rotateTree2 += (float)(80 * dt);
		}
		if (tree3Dead == true)
		{
			rotateTree3 += (float)(80 * dt);
		}
		if (tree4Dead == true)
		{
			rotateTree4 += (float)(80 * dt);
		}
		if (tree5Dead == true)
		{
			rotateTree5 += (float)(80 * dt);
		}
	}

	if (tree1 == true || tree2 == true || tree3 == true || tree4 == true || tree5 == true)
	{
		if (timerStartTree == true)
		{
			timerTree -= (float)(1 * dt);
		}
	}

	if (stone1 == true || stone2 == true || stone3 == true || stone4 == true || stone5 == true || stone6 == true || stone7 == true)
	{
		if (timerStartStone == true)
		{
			timerStone -= (float)(1 * dt);
		}
	}	

	if (RenderPuzzle)
	{
		PuzzleCheck(dt);
	}
	else
	{
		camera.Update(dt, object);
		std::cout << camera.position << std::endl;
		BoundsCheck();

		ResourceCollision();
		PuzzleCollision();
	}

	if (Application::IsKeyPressed('1') && collidedCrafting == false)
	{
		HoldingPick = true;
		HoldingAxe = false;
		HoldingFishingRod = false;
		HoldingFish = false;
		HoldingHammer = false;
		HoldingTorch = false;
		HoldingMachete = false;
		HoldingMeat = false;
		selection = 0;
	}
	if (Application::IsKeyPressed('2') && collidedCrafting == false)
	{
		HoldingPick = false;
		HoldingAxe = true;
		HoldingFishingRod = false;
		HoldingFish = false;
		HoldingHammer = false;
		HoldingTorch = false;
		HoldingMachete = false;
		HoldingMeat = false;
		HoldingMachete = false;
		selection = 100;
	}
	if (Application::IsKeyPressed('L') && collidedCrafting == false)
	{
		HoldingPick = false;
		HoldingAxe = false;
		HoldingFishingRod = false;
		HoldingFish = false;
		HoldingHammer = false;
		HoldingMeat = false;
		HoldingTorch = true;
		HoldingMachete = false;
		selection = 1500;
	}
	if (Application::IsKeyPressed('3') && collidedCrafting == false)
	{
		HoldingPick = false;
		HoldingAxe = false;
		HoldingFishingRod = false;
		HoldingFish = true;
		HoldingTorch = false;
		HoldingMeat = false;
		HoldingHammer = false;
		selection = 200;
	}
	if (Application::IsKeyPressed('4') && collidedCrafting == false)
	{
		HoldingPick = false;
		HoldingAxe = false;
		HoldingFishingRod = false;
		HoldingFish = false;

		HoldingMachete = false;
		HoldingTorch = false;
		HoldingHammer = false;
		HoldingMeat = true;
		selection = 300;
	}
	if (Application::IsKeyPressed('5') && collidedCrafting == false)
	{
		HoldingPick = false;
		HoldingAxe = false;
		HoldingFishingRod = false;
		HoldingHammer = false;
		HoldingFish = false;
		HoldingMachete = true;
		HoldingMeat = false;
		HoldingTorch = false;
		selection = 400;
	}



	if (collidedCrafting == true && craftItems == false)
	{
		if (Application::IsKeyPressed('E'))
		{
			craftItems = true;
			craftingOptions = true;
		}
	}

	if (collidedCrafting == false)
	{
		craftItems = false;
		craftingOptions = false;
		displayCraftItems = false;
		craftingOptions = false;
		toolOptions = false;
	}

	if (craftingOptions == true && collidedCrafting == true)
	{
		if (Application::IsKeyPressed('1'))
		{
			toolOptions = true;
		}
	}

	if (Application::IsKeyPressed(VK_LBUTTON))
	{
		Axemovement.lerp(Vector3(0.05, -0.09, -0.07), 0.4);
		axerotate = 20;
	}
	else
	{
		Axemovement.lerp(Vector3(0.05, -0.08, -0.05), 0.4);
		axerotate = 0;
	}

	if (Application::IsKeyPressed('W') || Application::IsKeyPressed('A') || Application::IsKeyPressed('S') || Application::IsKeyPressed('D'))
	{
		Axemovement.lerp(Vector3(0.06, -0.09, -0.05), 0.4);
		//Axemovement.lerp(Vector3(0.05, -0.08, -0.1), 0.4);
	}


	if (collidedFishing == true && haveFishingRod == true)
	{
		if (Application::IsKeyPressed(VK_LBUTTON))
		{
			fishingIdle += (float)(1 * dt);
			timerFish -= (float)(1 * dt);
			FishingGame = true;
		}
		
	}
	else
	{
		ybar = 500;
		FishingGame = false;
		ybarmove = false;
	}

	if (FishingGame)
	{
		if (ybarmove)
		{
			ybar += (float)(dt * barspeed);
		}
		else
		{
			ybar -= (float)(dt * barspeed);
		}
		if (Application::IsKeyPressed(VK_LBUTTON))
		{
			if (!FishingClicked)
			{
				if (ybar >= 490 && ybar <= 510 && delay == 0)
				{
					fish++;
					foundFish = true;
					barspeed += 20;
					delay = 60;
					FishingClicked = true;
					
				}
				else
				{
					FishingGame = false;
					FishingClicked = false;
				}
			}
		}
		else
		{
			FishingClicked = false;
		}
		if (delay > 0)
		{
			delay--;
		}
	}

	if (ybar >= 640)
	{
		ybarmove = false;
	}
	if (ybar <= 360)
	{
		ybarmove = true;
	}

	std::cout << ybar << std::endl;
	std::cout << delay << std::endl;
	/*if (fishingIdle >= 5)
	{
		fishingIdle = 0.0f;
	}

	if (timerFish <= 0)
	{
		timerFish = 5.0f;
		foundFish = true;
		fish++;
	}*/

	if (fish >= 1)
	{
		if (Application::IsKeyPressed(VK_LBUTTON) && HoldingFish == true)
		{
			eatingIdle += (float)(1 * dt);
			timerEat -= (float)(1 * dt);

			if (eatingIdle >= 5)
			{
				eatingIdle = 0.0f;
			}

			if (timerEat <= 0)
			{
				timerEat = 5.0f;
				fish--;
				player.Hunger += 10.0f;
				player.Health += 20.0f;

				if (player.Health >= 100.0f)
				{
					player.Health = 100.0f;
				}
			}
		}
	}

	if (meat >= 1)
	{
		if (Application::IsKeyPressed(VK_LBUTTON) && HoldingMeat == true)
		{
			eatingIdleMeat += (float)(1 * dt);
			timerEatMeat -= (float)(1 * dt);

			if (eatingIdleMeat >= 5)
			{
				eatingIdleMeat = 0.0f;
			}

			if (timerEatMeat <= 0)
			{
				timerEatMeat = 5.0f;
				meat--;
				player.Hunger += 10.0f;
				player.Health += 30.0f;

				if (player.Health >= 100.0f)
				{
					player.Health = 100.0f;
				}
			}
		}
	}


	if (Puzzle1->PuzzleSolved == true)
	{
		Puzzle1->puzzleRespawnTimer(dt);
	}

	if (Puzzle2->PuzzleSolved == true)
	{
		Puzzle2->puzzleRespawnTimer(dt);
	}

	if (HoldingTorch)
	{
		light[1].power = 3;
		glUniform1f(m_parameters[U_LIGHT1_POWER], light[1].power);
		UpdateTorchPosition();
		AnimateTorch(dt);
	}
	else if (!HoldingTorch)
	{
		light[1].power = 0;
		glUniform1f(m_parameters[U_LIGHT1_POWER], light[1].power);
	}
}

void SPScene::RotateAI(float dt)
{
	for (int i = 0; i < 8; i++)
	{
		if (Animal[i] != NULL)
		{
			if (Animal[i]->rotation > Animal[i]->rotateAmt)
			{
				Animal[i]->rotation -= dt * 50;
			}
			if (Animal[i]->rotation < Animal[i]->rotateAmt)
			{
				Animal[i]->rotation += dt * 50;
			}
		}
	}

	for (int i = 8; i < 10; i++)
	{
		if (Animal[i] != NULL)
		{
			if (Animal[i]->WolvesState == 0)
			{
				Vector3 NorthVector(0, 0, 1);
				Vector3 Direction(Animal[i]->position - camera.position);

				if (Direction.IsZero() == false)
				{
					Direction.Normalize();
					float dotproduct = NorthVector.Dot(Direction);
					Animal[i]->rotateAmt = acos(dotproduct);
					Animal[i]->rotateAmt = Math::RadianToDegree(Animal[i]->rotateAmt);

					if (Direction.x < 0)
					{
						Animal[i]->rotateAmt = -Animal[i]->rotateAmt;
					}
				}

			}
		}

	}
}

void SPScene::UpdateTorchPosition()
{
	Mtx44 rotation;
	Vector3 tempcamera = camera.position;
	Vector3 right = camera.view.Cross(camera.up);
	Vector3 up = right.Cross(camera.view).Normalized();
	rotation.SetToRotation(5, right.x, right.y, right.z);
	Vector3 tempview = rotation * camera.view.Normalized();
	rotation.SetToRotation(-10, up.x, up.y, up.z);
	tempview = rotation * tempview;
	Vector3 temp = tempcamera + (tempview * 0.2);

	light[1].position.x = temp.x;
	light[1].position.y = temp.y;
	light[1].position.z = temp.z;

}

void SPScene::AnimateTorch(double dt)
{
	FireElapsedTime += dt;
	bool bSomethingHappened = false;
	if (g_dBounceTime > FireElapsedTime)
	{
		return;
	}
	else {
		if (flame == 1)
		{
			meshList[GEO_TORCHFLAME]->textureID = LoadTGA("Image//torchflame2.tga");
			flame = 2;
			bSomethingHappened = true;
		}
		else
		{
			meshList[GEO_TORCHFLAME]->textureID = LoadTGA("Image//torchflame.tga");
			flame = 1;
			bSomethingHappened = true;
		}
	}

	if (bSomethingHappened)
	{
		// set the bounce time to some time in the future to prevent accidental triggers
		g_dBounceTime = FireElapsedTime + 0.15; // 125ms should be enough
	}
}

void SPScene::ResourceCollision()
{

	if (tree1Dead == true || tree2Dead == true || tree3Dead == true || tree4Dead == true || tree5Dead == true)
	{
		timerStartTree = true;
	}

	//TREE1
	if (collidedTree1 == true && tree1 == false)
	{
		if (Application::IsKeyPressed(VK_LBUTTON) && tree1Dead == false)
		{
			if (!clicked && haveAxe == false)
			{
				tree1hp -= 1;
				clicked = true;
			}
			else if (!clicked && haveAxe == true && HoldingAxe == true)
			{
				tree1hp -= 2;
				clicked = true;
			}
		}
		else
		{
			clicked = false;
		}

		if (tree1hp == 0 && haveAxe == true && HoldingAxe == true)
		{
			foundWood = true;
			tree1Dead = true;
		}
		else if (tree1hp == 0 && haveAxe == false)
		{
			foundWood = true;
			tree1Dead = true;
		}
	}
	if (tree1 == false && HoldingAxe == true && haveAxe == true)
	{
		if (rotateTree1 > 89 && rotateTree1 < 91)
		{
			tree1 = true;
			wood += 3;
		}
	}
	else if (tree1 == false && haveAxe == false)
	{
		if (rotateTree1 >= 90)
		{
			tree1 = true;
			wood++;
		}
	}
	
	//TREE2
	if (collidedTree2 == true && tree2 == false)
	{
		if (Application::IsKeyPressed(VK_LBUTTON) && tree2Dead == false)
		{
			if (!clicked && haveAxe == false)
			{
				tree2hp -= 1;
				clicked = true;
			}
			else if (!clicked && haveAxe == true && HoldingAxe == true)
			{
				tree2hp -= 2;
				clicked = true;
			}
		}
		else
		{
			clicked = false;
		}

		if (tree2hp == 0 && haveAxe == true && HoldingAxe == true)
		{
			foundWood = true;
			tree2Dead = true;
		}
		else if (tree2hp == 0 && haveAxe == false)
		{
			foundWood = true;
			tree2Dead = true;
		}
	}
	if (tree2 == false && HoldingAxe == true && haveAxe == true)
	{
		if (rotateTree2 > 89 && rotateTree2 < 91)
		{
			tree2 = true;
			wood += 3;
		}
	}
	else if (tree2 == false && haveAxe == false)
	{
		if (rotateTree2 >= 90)
		{
			tree2 = true;
			wood++;
		}
	}
	
	//Tree3
	if (collidedTree3 == true && tree3 == false)
	{
		if (Application::IsKeyPressed(VK_LBUTTON) && tree3Dead == false)
		{
			if (!clicked && haveAxe == false)
			{
				tree3hp -= 1;
				clicked = true;
			}
			else if (!clicked && haveAxe == true && HoldingAxe == true)
			{
				tree3hp -= 2;
				clicked = true;
			}
		}
		else
		{
			clicked = false;
		}

		if (tree3hp == 0 && haveAxe == true && HoldingAxe == true)
		{
			foundWood = true;
			tree3Dead = true;
		}
		else if (tree3hp == 0 && haveAxe == false)
		{
			foundWood = true;
			tree3Dead = true;
		}
	}
	if (tree3 == false && HoldingAxe == true && haveAxe == true)
	{
		if (rotateTree3 > 89 && rotateTree3 < 91)
		{
			tree3 = true;
			wood+=3;
		}
	}
	else if (tree3 == false && haveAxe == false)
	{
		if (rotateTree3 >= 90)
		{
			tree3 = true;
			wood++;
		}
	}


	//TREE4
	if (collidedTree4 == true && tree4 == false)
	{
		if (Application::IsKeyPressed(VK_LBUTTON) && tree4Dead == false)
		{
			if (!clicked && haveAxe == false)
			{
				tree4hp -= 1;
				clicked = true;
			}
			else if (!clicked && haveAxe == true && HoldingAxe == true)
			{
				tree4hp -= 2;
				clicked = true;
			}
		}
		else
		{
			clicked = false;
		}

		if (tree4hp == 0 && haveAxe == true && HoldingAxe == true)
		{
			foundWood = true;
			tree4Dead = true;
		}
		else if (tree4hp == 0 && haveAxe == false)
		{
			foundWood = true;
			tree4Dead = true;
		}
	}
	if (tree4 == false && HoldingAxe == true && haveAxe == true)
	{
		if (rotateTree4 > 89 && rotateTree4 < 91)
		{
			tree4 = true;
			wood += 3;
		}
	}
	else if (tree4 == false && haveAxe == false)
	{
		if (rotateTree4 >= 90)
		{
			tree4 = true;
			wood++;
		}
	}

	if (collidedTree5 == true && tree5 == false)
	{
		if (Application::IsKeyPressed(VK_LBUTTON) && tree5Dead == false)
		{
			if (!clicked && haveAxe == false)
			{
				tree5hp -= 1;
				clicked = true;
			}
			else if (!clicked && haveAxe == true && HoldingAxe == true)
			{
				tree5hp -= 2;
				clicked = true;
			}
		}
		else
		{
			clicked = false;
		}

		if (tree5hp == 0 && haveAxe == true && HoldingAxe == true)
		{
			foundWood = true;
			tree5Dead = true;
		}
		else if (tree5hp == 0 && haveAxe == false)
		{
			foundWood = true;
			tree5Dead = true;
		}
	}
	if (tree5 == false && HoldingAxe == true && haveAxe == true)
	{
		if (rotateTree5 > 89 && rotateTree5 < 91)
		{
			tree3 = true;
			wood += 3;
		}
	}
	else if (tree5 == false && haveAxe == false)
	{
		if (rotateTree5 >= 90)
		{
			tree5 = true;
			wood++;
		}
	}

	//////////////////////
	//      STONES   /////
	//////////////////////

	//STONE1
	if (collidedStone1 == true && stone1 == false)
	{
		if (Application::IsKeyPressed(VK_LBUTTON))
		{
			if (!clicked && havePickaxe == false)
			{
				stone1hp -= 1;
				clicked = true;
			}
			else if (!clicked && havePickaxe == true && HoldingPick == true)
			{
				stone1hp -= 2;
				clicked = true;
			}
		}
		else
		{
			clicked = false;
		}

		if (stone1hp <= 0 && havePickaxe == true && HoldingPick == true)
		{
			stone1 = true;
			foundStone = true;
			timerStartStone = true;
			stone += 3;
		}
		else if (stone1hp <= 0 && haveAxe == false)
		{
			stone1 = true;
			foundStone = true;
			timerStartStone = true;
			stone++;
		}
	}

	//STONE2
	if (collidedStone2 == true && stone2 == false)
	{
		if (Application::IsKeyPressed(VK_LBUTTON))
		{
			if (!clicked && havePickaxe == false)
			{
				stone2hp -= 1;
				clicked = true;
			}
			else if (!clicked && havePickaxe == true && HoldingPick == true)
			{
				stone2hp -= 2;
				clicked = true;
			}
		}
		else
		{
			clicked = false;
		}

		if (stone2hp <= 0 && havePickaxe == true && HoldingPick == true)
		{
			stone2 = true;
			foundStone = true;
			timerStartStone = true;
			stone += 3;
		}
		else if (stone2hp <= 0 && haveAxe == false)
		{
			stone2 = true;
			foundStone = true;
			timerStartStone = true;
			stone++;
		}
	}

	//STONE3
	if (collidedStone3 == true && stone3 == false)
	{
		if (Application::IsKeyPressed(VK_LBUTTON))
		{
			if (!clicked && havePickaxe == false)
			{
				stone3hp -= 1;
				clicked = true;
			}
			else if (!clicked && havePickaxe == true && HoldingPick == true)
			{
				stone3hp -= 2;
				clicked = true;
			}
		}
		else
		{
			clicked = false;
		}

		if (stone3hp <= 0 && havePickaxe == true && HoldingPick == true)
		{
			stone3 = true;
			foundStone = true;
			timerStartStone = true;
			stone += 3;
		}
		else if (stone3hp <= 0 && haveAxe == false)
		{
			stone3 = true;
			foundStone = true;
			timerStartStone = true;
			stone++;
		}
	}

	//STONE4
	if (collidedStone4 == true && stone4 == false)
	{
		if (Application::IsKeyPressed(VK_LBUTTON))
		{
			if (!clicked && havePickaxe == false)
			{
				stone4hp -= 1;
				clicked = true;
			}
			else if (!clicked && havePickaxe == true && HoldingPick == true)
			{
				stone4hp -= 2;
				clicked = true;
			}
		}
		else
		{
			clicked = false;
		}

		if (stone4hp <= 0 && havePickaxe == true && HoldingPick == true)
		{
			stone4 = true;
			foundStone = true;
			timerStartStone = true;
			stone += 3;
		}
		else if (stone4hp <= 0 && haveAxe == false)
		{
			stone4 = true;
			foundStone = true;
			timerStartStone = true;
			stone++;
		}
	}

	//STONE5
	if (collidedStone5 == true && stone5 == false)
	{
		if (Application::IsKeyPressed(VK_LBUTTON))
		{
			if (!clicked && havePickaxe == false)
			{
				stone5hp -= 1;
				clicked = true;
			}
			else if (!clicked && havePickaxe == true && HoldingPick == true)
			{
				stone5hp -= 2;
				clicked = true;
			}
		}
		else
		{
			clicked = false;
		}

		if (stone5hp <= 0 && havePickaxe == true && HoldingPick == true)
		{
			stone5 = true;
			foundStone = true;
			timerStartStone = true;
			stone += 3;
		}
		else if (stone5hp <= 0 && haveAxe == false)
		{
			stone5 = true;
			foundStone = true;
			timerStartStone = true;
			stone++;
		}
	}

	//STONE6
	if (collidedStone6 == true && stone6 == false)
	{
		if (Application::IsKeyPressed(VK_LBUTTON))
		{
			if (!clicked && havePickaxe == false)
			{
				stone6hp -= 1;
				clicked = true;
			}
			else if (!clicked && havePickaxe == true && HoldingPick == true)
			{
				stone6hp -= 2;
				clicked = true;
			}
		}
		else
		{
			clicked = false;
		}

		if (stone6hp <= 0 && havePickaxe == true && HoldingPick == true)
		{
			stone6 = true;
			foundStone = true;
			timerStartStone = true;
			stone += 3;
		}
		else if (stone6hp <= 0 && haveAxe == false)
		{
			stone6 = true;
			foundStone = true;
			timerStartStone = true;
			stone++;
		}
	}

	//STONE7
	if (collidedStone7 == true && stone7 == false)
	{
		if (Application::IsKeyPressed(VK_LBUTTON))
		{
			if (!clicked && havePickaxe == false)
			{
				stone7hp -= 1;
				clicked = true;
			}
			else if (!clicked && havePickaxe == true && HoldingPick == true)
			{
				stone7hp -= 2;
				clicked = true;
			}
		}
		else
		{
			clicked = false;
		}

		if (stone7hp <= 0 && havePickaxe == true && HoldingPick == true)
		{
			stone7 = true;
			foundStone = true;
			timerStartStone = true;
			stone += 3;
		}
		else if (stone7hp <= 0 && haveAxe == false)
		{
			stone7 = true;
			foundStone = true;
			timerStartStone = true;
			stone++;
		}
	}
}

void SPScene::PuzzleCollision()
{
	if (collidedPuzzle1 && !Puzzle1->PuzzleSolved)
	{
		if (Application::IsKeyPressed('E'))
		{
			RenderPuzzle = true;
		}
		else if (Application::IsKeyPressed(VK_ESCAPE))
		{
			RenderPuzzle = false;
		}
	}

	if (collidedPuzzle2 && !Puzzle2->PuzzleSolved)
	{
		if (Application::IsKeyPressed('E'))
		{
			RenderPuzzle = true;
		}
		else if (Application::IsKeyPressed(VK_ESCAPE))
		{
			RenderPuzzle = false;
		}
	}
}

void SPScene::PuzzleCheck(double dt)
{

	g_dElapsedTime += dt;
	if (RenderPuzzle && !Puzzle1->PuzzleSolved && collidedPuzzle1) //Answer the puzzle
	{
		Puzzle1->PuzzleSolved = Puzzle1->ansPuzzle(g_dElapsedTime);
	}
	else if (RenderPuzzle && !Puzzle2->PuzzleSolved && collidedPuzzle2) //Answer the puzzle
	{
		Puzzle2->PuzzleSolved = Puzzle2->ansPuzzle(g_dElapsedTime);
	}

	if (RenderPuzzle && Puzzle1->PuzzleSolved && collidedPuzzle1)
	{
		RenderPuzzle = false;
	}
	if (RenderPuzzle && Puzzle2->PuzzleSolved && collidedPuzzle2)
	{
		RenderPuzzle = false;
	}
}

void SPScene::Render()
{
	//Clear color & depth buffer every frame
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	viewStack.LoadIdentity();
	viewStack.LookAt(camera.position.x, camera.position.y, camera.position.z, camera.target.x, camera.target.y, camera.target.z, camera.up.x, camera.up.y, camera.up.z);
	modelStack.LoadIdentity();

	if (light[0].type == Light::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(light[0].position.x, light[0].position.y, light[0].position.z);
		Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightDirection_cameraspace.x);
	}
	else if (light[0].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[0].position;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = viewStack.Top() * light[0].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT0_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	}
	else
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[0].position;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightPosition_cameraspace.x);
	}

	if (light[1].type == Light::LIGHT_POINT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[1].position;
		glUniform3fv(m_parameters[U_LIGHT1_POSITION], 1, &lightPosition_cameraspace.x);
	}
	else if (light[1].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[1].position;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = viewStack.Top() * light[1].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT0_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	}
	else
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[1].position;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightPosition_cameraspace.x);
	}

	if (RenderPuzzle)
	{
		if (collidedPuzzle1)
		{

			RenderMeshOnScreen(meshList[GEO_PUZZLESCREEN], 800, 450, 160, 90, 1, 0, 1, 1, 1);
			RenderTextOnScreen(meshList[GEO_TEXT], Puzzle1->printPuzzles(), Color(0, 0, 0), 2.5, 6, 17);
			if (Puzzle1->wrongAnimate)
			{
				RenderTextOnScreen(meshList[GEO_TEXT], Puzzle1->puzzleAns, Color(1, 0, 0), 2.5, 7, 12);
				RenderTextOnScreen(meshList[GEO_TEXT], ">", Color(1, 0, 0), 2.5, 6, 12);
				RenderTextOnScreen(meshList[GEO_TEXT], "<", Color(1, 0, 0), 2.5, 7 + Puzzle1->puzzleAns.size(), 12);
			}
			else
			{
				RenderTextOnScreen(meshList[GEO_TEXT], Puzzle1->puzzleAns, Color(0, 0, 0), 2.5, 7, 12);
				RenderTextOnScreen(meshList[GEO_TEXT], ">", Color(0, 0, 0), 2.5, 6, 12);
				RenderTextOnScreen(meshList[GEO_TEXT], "<", Color(0, 0, 0), 2.5, 7 + Puzzle1->puzzleAns.size(), 12);
			}
		}
		else if (collidedPuzzle2)
		{
			RenderMeshOnScreen(meshList[GEO_PUZZLESCREEN], 800, 450, 160, 90, 1, 0, 1, 1, 1);
			RenderTextOnScreen(meshList[GEO_TEXT], Puzzle2->printPuzzles(), Color(0, 0, 0), 2.5, 6, 17);
			if (Puzzle2->wrongAnimate)
			{
				RenderTextOnScreen(meshList[GEO_TEXT], Puzzle2->puzzleAns, Color(1, 0, 0), 2.5, 7, 12);
				RenderTextOnScreen(meshList[GEO_TEXT], ">", Color(1, 0, 0), 2.5, 6, 12);
				RenderTextOnScreen(meshList[GEO_TEXT], "<", Color(1, 0, 0), 2.5, 7 + Puzzle2->puzzleAns.size(), 12);
			}
			else
			{
				RenderTextOnScreen(meshList[GEO_TEXT], Puzzle2->puzzleAns, Color(0, 0, 0), 2.5, 7, 12);
				RenderTextOnScreen(meshList[GEO_TEXT], ">", Color(0, 0, 0), 2.5, 6, 12);
				RenderTextOnScreen(meshList[GEO_TEXT], "<", Color(0, 0, 0), 2.5, 7 + Puzzle2->puzzleAns.size(), 12);
			}

		}
	}
	else {

		modelStack.PushMatrix();
		modelStack.Translate(light[0].position.x, light[0].position.y, light[0].position.z);
		modelStack.Scale(5, 5, 5);
		RenderMesh(meshList[GEO_LIGHTBALL2], false);
		modelStack.PopMatrix();

		RenderSkyBox();

		modelStack.PushMatrix();
		RenderText(meshList[GEO_TEXT], "TESTING", Color(0, 1, 0));
		modelStack.PopMatrix();

		RenderTextOnScreen(meshList[GEO_TEXT], "Hello World", Color(0, 1, 0), 1, 0, 0);

		float angle = 0;
		getAngle(angle, camera.position, petObject);
		modelStack.PushMatrix();
		objTarget = camera.position - petObject;
		modelStack.Translate(petObject.x, petObject.y + 1.5, petObject.z);
		modelStack.Rotate(angle, 0, 1, 0);
		modelStack.Scale(1, 1, 1);
		RenderMesh(meshList[GEO_PET], true);
		modelStack.PopMatrix();

		if (haveFishingRod)
		{
			modelStack.PushMatrix();
			modelStack.Translate(-13.7, 1.5, -128.9);
			modelStack.Rotate(180, 0, 1, 0);
			modelStack.Scale(1, 1, 1);
			RenderMesh(meshList[GEO_FISHINGROD], true);
			modelStack.PopMatrix();
		}

		modelStack.PushMatrix();
		modelStack.Translate(0, 0, 0);
		modelStack.Scale(8, 1, 8);
		RenderMesh(meshList[GEO_MAP], night);
		modelStack.PopMatrix();

		modelStack.PushMatrix();
		modelStack.Translate(-85, 0.9, 200);
		modelStack.Scale(1.5, 1.5, 1.5);
		RenderMesh(meshList[GEO_FENCE], true);
		for (int i = 0; i < 50; i++)
		{
			modelStack.PushMatrix();
			modelStack.Translate(0, 0, -4);
			RenderMesh(meshList[GEO_FENCE], true);
		}
		for (int i = 0; i < 50; i++)
		{
			modelStack.PopMatrix();
		}
		modelStack.PopMatrix();

		//Tree fence right
		modelStack.PushMatrix();
		modelStack.Translate(-33, 1, 190);
		modelStack.Scale(1.5, 1.5, 1.5);
		RenderMesh(meshList[GEO_FENCE], true);
		for (int i = 0; i < 7; i++)
		{
			modelStack.PushMatrix();
			modelStack.Translate(0, 0, -4);
			RenderMesh(meshList[GEO_FENCE], true);
		}
		for (int i = 0; i < 7; i++)
		{
			modelStack.PopMatrix();
		}
		modelStack.PopMatrix();

		//Tree fence front
		modelStack.PushMatrix();
		modelStack.Translate(-27, 1, 142);
		modelStack.Rotate(90, 0, 1, 0);
		modelStack.Scale(1.5, 1.5, 1.5);
		RenderMesh(meshList[GEO_FENCE], true);
		for (int i = 0; i < 1; i++)
		{
			modelStack.PushMatrix();
			modelStack.Translate(0, 0, 4);
			RenderMesh(meshList[GEO_FENCE], true);
		}
		for (int i = 0; i < 1; i++)
		{
			modelStack.PopMatrix();
		}
		modelStack.PopMatrix();

		//Tree fence front
		modelStack.PushMatrix();
		modelStack.Translate(-10, 1, 142);
		modelStack.Rotate(90, 0, 1, 0);
		modelStack.Scale(1.5, 1.5, 1.5);
		RenderMesh(meshList[GEO_FENCE], true);
		for (int i = 0; i < 1; i++)
		{
			modelStack.PushMatrix();
			modelStack.Translate(0, 0, 4);
			RenderMesh(meshList[GEO_FENCE], true);
		}
		for (int i = 0; i < 1; i++)
		{
			modelStack.PopMatrix();
		}
		modelStack.PopMatrix();

		//Tree fence left
		modelStack.PushMatrix();
		modelStack.Translate(-3, 1, 190);
		modelStack.Scale(1.5, 1.5, 1.5);
		RenderMesh(meshList[GEO_FENCE], true);
		for (int i = 0; i < 7; i++)
		{
			modelStack.PushMatrix();
			modelStack.Translate(0, 0, -4);
			RenderMesh(meshList[GEO_FENCE], true);
		}
		for (int i = 0; i < 7; i++)
		{
			modelStack.PopMatrix();
		}
		modelStack.PopMatrix();

		//tree fence gate
		modelStack.PushMatrix();
		modelStack.Translate(-17.8, 0, 141.8);
		modelStack.Rotate(90, 0, 1, 0);
		modelStack.Scale(1.5, 1.5, 1.5);
		RenderMesh(meshList[GEO_PUZZLEBOARD], true);
		modelStack.PopMatrix();


		//Stone fence right
		modelStack.PushMatrix();
		modelStack.Translate(115, 1, 189);
		modelStack.Scale(1.5, 1.5, 1.5);
		RenderMesh(meshList[GEO_FENCE], true);
		for (int i = 0; i < 9; i++)
		{
			modelStack.PushMatrix();
			modelStack.Translate(0, 0, -4);
			RenderMesh(meshList[GEO_FENCE], true);
		}
		for (int i = 0; i < 9; i++)
		{
			modelStack.PopMatrix();
		}
		modelStack.PopMatrix();

		//Stone fence front
		modelStack.PushMatrix();
		modelStack.Translate(121, 1, 130);
		modelStack.Rotate(90, 0, 1, 0);
		modelStack.Scale(1.5, 1.5, 1.5);
		RenderMesh(meshList[GEO_FENCE], true);
		for (int i = 0; i < 4; i++)
		{
			modelStack.PushMatrix();
			modelStack.Translate(0, 0, 4);
			RenderMesh(meshList[GEO_FENCE], true);
		}
		for (int i = 0; i < 4; i++)
		{
			modelStack.PopMatrix();
		}
		modelStack.PopMatrix();

		//Stone fence front
		modelStack.PushMatrix();
		modelStack.Translate(155, 1, 130);
		modelStack.Rotate(90, 0, 1, 0);
		modelStack.Scale(1.5, 1.5, 1.5);
		RenderMesh(meshList[GEO_FENCE], true);
		for (int i = 0; i < 6; i++)
		{
			modelStack.PushMatrix();
			modelStack.Translate(0, 0, 4);
			RenderMesh(meshList[GEO_FENCE], true);
		}
		for (int i = 0; i < 6; i++)
		{
			modelStack.PopMatrix();
		}
		modelStack.PopMatrix();

		//stone fence gate
		modelStack.PushMatrix();
		modelStack.Translate(147.6, 0, 129.6);
		modelStack.Rotate(90, 0, 1, 0);
		modelStack.Scale(1.5, 1.5, 1.5);
		RenderMesh(meshList[GEO_PUZZLEBOARD], true);
		modelStack.PopMatrix();

		modelStack.PushMatrix();
		modelStack.Translate(-44, 1, 14);
		modelStack.Scale(8, 8, 8);
		modelStack.Rotate(90, 0, 1, 0);
		RenderMesh(meshList[GEO_HOUSE], night);
		modelStack.PopMatrix();

		modelStack.PushMatrix();
		modelStack.Translate(-35, 1, 11.4);
		modelStack.Scale(1.5, 1.5, 1);
		RenderMesh(meshList[GEO_BENCH], true);
		modelStack.PopMatrix();

		RenderFishingGame();
		RenderAI();
		RenderTrees();
		RenderStones();
		RenderStatus();
		MenuUI();
		RenderHotbar();
		RenderAxeArm();
	}

}

void SPScene::RenderHotbar()
{
	if (havePickaxe)
	{
		RenderMeshOnScreen(meshList[GEO_PICKHB], 600, 150, 10, 5, 1, 0, 1, 1, 1);
	}
	if (haveAxe)
	{
		RenderMeshOnScreen(meshList[GEO_AXEHB], 700, 150, 10, 5, 1, 0, 1, 1, 1);
	}
	if (haveMachete)
	{
		RenderMeshOnScreen(meshList[GEO_MACHETEHB], 1000, 150, 10, 5, 1, 0, 1, 1, 1);
	}
	if (fish >= 1)
	{
		RenderMeshOnScreen(meshList[GEO_FISHHB], 800, 150, 10, 5, 1, 0, 1, 1, 1);
	}
	if (meat >= 1)
	{
		RenderMeshOnScreen(meshList[GEO_MEATHB], 900, 150, 10, 5, 1, 0, 1, 1, 1);
	}

	RenderMeshOnScreen(meshList[GEO_HOTBAR], 800, 150, 50, 5, 1, 0, 1, 1, 1);
	RenderMeshOnScreen(meshList[GEO_HOTBARSELECTION], 600 + selection, 150, 10, 5, 1, 0, 1, 1, 1);
}

void SPScene::RenderStones()
{
	if (timerStone <= 0)
	{
		stone1 = false;
		stone2 = false;
		stone3 = false;
		stone4 = false;
		stone5 = false;
		stone6 = false;
		stone7 = false;
		timerStartStone = false;
		timerStone = 20.0f;
		stone1hp = 3;
		stone2hp = 3;
		stone3hp = 3;
		stone4hp = 3;
		stone5hp = 3;
		stone6hp = 3;
		stone7hp = 3;
	}

	////////////////////////
	// Interactable Rocks///
	////////////////////////

	//STONE1
	if (stone1 == false && stone1hp == 2)
	{
		modelStack.PushMatrix();
		modelStack.Translate(150, 1, 186);
		modelStack.Scale(3, 3, 3);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}
	else if (stone1 == false && stone1hp == 1)
	{
		modelStack.PushMatrix();
		modelStack.Translate(150, 1, 186);
		modelStack.Scale(1, 1, 1);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}
	else if (stone1 == false)
	{
		modelStack.PushMatrix();
		modelStack.Translate(150, 1, 186);
		modelStack.Scale(5, 5, 5);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}


	//STONE2
	if (stone2 == false && stone2hp == 2)
	{
		modelStack.PushMatrix();
		modelStack.Translate(166, 1, 177);
		modelStack.Scale(3, 3, 3);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}
	else if (stone2 == false && stone2hp == 1)
	{
		modelStack.PushMatrix();
		modelStack.Translate(166, 1, 177);
		modelStack.Scale(1, 1, 1);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}
	else if (stone2 == false)
	{
		modelStack.PushMatrix();
		modelStack.Translate(166, 1, 177);
		modelStack.Scale(5, 5, 5);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}


	//STONE3
	if (stone3 == false && stone3hp == 2)
	{
		modelStack.PushMatrix();
		modelStack.Translate(130, 1, 149);
		modelStack.Scale(3, 3, 3);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}
	else if (stone3 == false && stone3hp == 1)
	{
		modelStack.PushMatrix();
		modelStack.Translate(130, 1, 149);
		modelStack.Scale(1, 1, 1);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}
	else if (stone3 == false)
	{
		modelStack.PushMatrix();
		modelStack.Translate(130, 1, 149);
		modelStack.Scale(5, 5, 5);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}


	//STONE4
	if (stone4 == false && stone4hp == 2)
	{
		modelStack.PushMatrix();
		modelStack.Translate(164, 1, 150);
		modelStack.Scale(3, 3, 3);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}
	else if (stone4 == false && stone4hp == 1)
	{
		modelStack.PushMatrix();
		modelStack.Translate(164, 1, 150);
		modelStack.Scale(1, 1, 1);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}
	else if (stone4 == false)
	{
		modelStack.PushMatrix();
		modelStack.Translate(164, 1, 150);
		modelStack.Scale(5, 5, 5);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}


	//STONE5
	if (stone5 == false && stone5hp == 2)
	{
		modelStack.PushMatrix();
		modelStack.Translate(145.8, 1, 165.4);
		modelStack.Scale(3, 3, 3);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}
	else if (stone5 == false && stone5hp == 1)
	{
		modelStack.PushMatrix();
		modelStack.Translate(145.8, 1, 165.4);
		modelStack.Scale(1, 1, 1);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}
	else if (stone5 == false)
	{
		modelStack.PushMatrix();
		modelStack.Translate(145.8, 1, 165.4);
		modelStack.Scale(5, 5, 5);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}


	//STONE6
	if (stone6 == false && stone6hp == 2)
	{
		modelStack.PushMatrix();
		modelStack.Translate(148, 1, 141);
		modelStack.Scale(3, 3, 3);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}
	else if (stone6 == false && stone6hp == 1)
	{
		modelStack.PushMatrix();
		modelStack.Translate(148, 1, 141);
		modelStack.Scale(1, 1, 1);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}
	else if (stone6 == false)
	{
		modelStack.PushMatrix();
		modelStack.Translate(148, 1, 141);
		modelStack.Scale(5, 5, 5);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}


	//STONE7
	if (stone7 == false && stone7hp == 2)
	{
		modelStack.PushMatrix();
		modelStack.Translate(127.7, 1, 175.7);
		modelStack.Scale(3, 3, 3);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}
	else if (stone7 == false && stone7hp == 1)
	{
		modelStack.PushMatrix();
		modelStack.Translate(127.7, 1, 175.7);
		modelStack.Scale(1, 1, 1);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}
	else if (stone7 == false)
	{
		modelStack.PushMatrix();
		modelStack.Translate(127.7, 1, 175.7);
		modelStack.Scale(5, 5, 5);
		RenderMesh(meshList[GEO_ROCK], night);
		modelStack.PopMatrix();
	}

	modelStack.PushMatrix();
	modelStack.Translate(26.4, 1, 96);
	modelStack.Scale(5, 5, 5);
	RenderMesh(meshList[GEO_ROCK4], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-12, 1, 81);
	modelStack.Scale(5, 5, 5);
	RenderMesh(meshList[GEO_ROCK4], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(45, 1, 152);
	modelStack.Scale(5, 5, 5);
	RenderMesh(meshList[GEO_ROCK4], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(102, 1, 98);
	modelStack.Scale(5, 5, 5);
	RenderMesh(meshList[GEO_ROCK4], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(139, 1, 60);
	modelStack.Scale(5, 5, 5);
	RenderMesh(meshList[GEO_ROCK4], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(136, 1, -1.5);
	modelStack.Scale(5, 5, 5);
	RenderMesh(meshList[GEO_ROCK4], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(143.8, 1, -73.2);
	modelStack.Scale(5, 5, 5);
	RenderMesh(meshList[GEO_ROCK4], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(5.6, 1, -116);
	modelStack.Scale(5, 5, 5);
	RenderMesh(meshList[GEO_ROCK4], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-49.4, 1, -65.5);
	modelStack.Scale(5, 5, 5);
	RenderMesh(meshList[GEO_ROCK4], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-72, 1, 55.6);
	modelStack.Scale(5, 5, 5);
	RenderMesh(meshList[GEO_ROCK4], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-43.7, 1, 134.6);
	modelStack.Scale(5, 5, 5);
	RenderMesh(meshList[GEO_ROCK4], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(65, 1, 95);
	modelStack.Scale(5, 5, 5);
	RenderMesh(meshList[GEO_ROCK4], night);
	modelStack.PushMatrix();
	modelStack.Translate(5, 0, 5);
	RenderMesh(meshList[GEO_ROCK4], night);
	modelStack.PushMatrix();
	modelStack.Translate(10, 0, -10);
	RenderMesh(meshList[GEO_ROCK4], night);
	modelStack.PushMatrix();
	modelStack.Translate(-15, 0, 0);
	RenderMesh(meshList[GEO_ROCK4], night);
	modelStack.PushMatrix();
	modelStack.Translate(25, 0, 5);
	RenderMesh(meshList[GEO_ROCK4], night);
	modelStack.PopMatrix();
	modelStack.PopMatrix();
	modelStack.PopMatrix();
	modelStack.PopMatrix();
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(78, 1, 120);
	modelStack.Scale(5, 5, 5);
	RenderMesh(meshList[GEO_ROCK4], night);
	modelStack.PushMatrix();
	modelStack.Translate(5, 0, 5);
	RenderMesh(meshList[GEO_ROCK4], night);
	modelStack.PopMatrix();
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-225, 0.7, 200);
	modelStack.Scale(8, 8, 8);
	RenderMesh(meshList[GEO_ROCK2], night);
	for (int i = 0; i < 33; i++)
	{
		modelStack.PushMatrix();
		modelStack.Translate(0, 0, -1.5);
		RenderMesh(meshList[GEO_ROCK2], night);
	}
	for (int i = 0; i < 33; i++)
	{
		modelStack.PopMatrix();
	}
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(168, 0.7, 200);
	modelStack.Scale(8, 8, 8);
	RenderMesh(meshList[GEO_ROCK2], night);
	for (int i = 0; i < 33; i++)
	{
		modelStack.PushMatrix();
		modelStack.Translate(0, 0, -1.5);
		RenderMesh(meshList[GEO_ROCK2], night);
	}
	for (int i = 0; i < 33; i++)
	{
		modelStack.PopMatrix();
	}
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-225.7, -2, 198.7);
	modelStack.Scale(10, 10, 10);
	RenderMesh(meshList[GEO_ROCK2], night);
	for (int i = 0; i < 26; i++)
	{
		modelStack.PushMatrix();
		modelStack.Translate(1.5, 0, 0);
		RenderMesh(meshList[GEO_ROCK2], night);
	}
	for (int i = 0; i < 26; i++)
	{
		modelStack.PopMatrix();
	}
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-225.7, -2, -198.7);
	modelStack.Scale(10, 10, 10);
	RenderMesh(meshList[GEO_ROCK2], night);
	for (int i = 0; i < 26; i++)
	{
		modelStack.PushMatrix();
		modelStack.Translate(1.5, 0, 0);
		RenderMesh(meshList[GEO_ROCK2], night);
	}
	for (int i = 0; i < 26; i++)
	{
		modelStack.PopMatrix();
	}
	modelStack.PopMatrix();
}

void SPScene::RenderTrees()
{
	if (timerTree <= 0)
	{
		tree1 = false;
		tree2 = false;
		tree3 = false;
		tree4 = false;
		tree5 = false;
		timerStartTree = false;
		timerTree = 20.0f;
		rotateTree1 = 0.0f;
		rotateTree2 = 0.0f;
		rotateTree3 = 0.0f;
		rotateTree4 = 0.0f;
		rotateTree5 = 0.0f;
		tree1Dead = false;
		tree1Dead = false;
		tree2Dead = false;
		tree3Dead = false;
		tree4Dead = false;
		tree5Dead = false;
		tree1hp = 4;
		tree2hp = 4;
		tree3hp = 4;
		tree4hp = 4;
		tree5hp = 4;
	}

	modelStack.PushMatrix();
	modelStack.Translate(50, -1, 60);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(35, -1, 90);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(40, -1, 40);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(25, -1, 60);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(5, -1, 75);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-12, -1, 55);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-40, -1, 85);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-38, -1, 35);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-32, -1, -10);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-17, -1, -35);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-7, -1, -75);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(15, -1, -45);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(35, -1, -85);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(50, -1, -70);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(45, -1, -40);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-25, -1, -25);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(13, -1, 15);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(30, -1, -2);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(10, -1, -65);
	modelStack.Rotate(30, 0, 1, 0);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PushMatrix();
	modelStack.Translate(-12, 0, 40);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PushMatrix();
	modelStack.Translate(-13, 0, -17);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();
	modelStack.PopMatrix();
	modelStack.PopMatrix();


	modelStack.PushMatrix();
	modelStack.Translate(16, -1, -120);
	modelStack.Rotate(130, 0, 1, 0);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PushMatrix();
	modelStack.Translate(-18, 0, 10);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PushMatrix();
	modelStack.Translate(-13, 0, -17);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();
	modelStack.PopMatrix();
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-10, -1, -15);
	modelStack.Rotate(60, 0, 1, 0);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PushMatrix();
	modelStack.Translate(-12, 0, 40);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PushMatrix();
	modelStack.Translate(-13, 0, -17);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();
	modelStack.PopMatrix();
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-5, -1, 30);
	modelStack.Rotate(70, 0, 1, 0);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PushMatrix();
	modelStack.Translate(-12, 0, 40);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PushMatrix();
	modelStack.Translate(-13, 0, -17);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();
	modelStack.PopMatrix();
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-9, -1, 99);
	modelStack.Rotate(70, 0, 1, 0);
	modelStack.Scale(0.8, 0.8, 0.8);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PushMatrix();
	modelStack.Translate(-12, 0, 40);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PushMatrix();
	modelStack.Translate(-13, 0, -17);
	RenderMesh(meshList[GEO_TREE], night);
	modelStack.PopMatrix();
	modelStack.PopMatrix();
	modelStack.PopMatrix();

	// Interactable Trees
	if (tree1 == false && rotateTree1 <= 90.0f)
	{
		modelStack.PushMatrix();
		modelStack.Translate(-15, -1, 175);
		modelStack.Rotate(rotateTree1, 1, 0, 0);
		modelStack.Scale(0.5, 0.5, 0.5);
		RenderMesh(meshList[GEO_PALMTREE], night);
		modelStack.PopMatrix();
	}

	if (tree2 == false && rotateTree2 <= 90.0f)
	{
		modelStack.PushMatrix();
		modelStack.Translate(-20, -1, 165);
		modelStack.Rotate(rotateTree2, 1, 0, 0);
		modelStack.Scale(0.5, 0.5, 0.5);
		RenderMesh(meshList[GEO_PALMTREE], night);
		modelStack.PopMatrix();
	}

	if (tree3 == false && rotateTree3 <= 90.0f)
	{
		modelStack.PushMatrix();
		modelStack.Translate(-15, -1, 158);
		modelStack.Rotate(rotateTree3, 1, 0, 0);
		modelStack.Scale(0.5, 0.5, 0.5);
		RenderMesh(meshList[GEO_PALMTREE], night);
		modelStack.PopMatrix();
	}

	if (tree4 == false && rotateTree4 <= 90.0f)
	{
		modelStack.PushMatrix();
		modelStack.Translate(-9, -1, 159);
		modelStack.Rotate(rotateTree4, 1, 0, 0);
		modelStack.Scale(0.5, 0.5, 0.5);
		RenderMesh(meshList[GEO_PALMTREE], night);
		modelStack.PopMatrix();
	}

	if (tree5 == false && rotateTree5 <= 90.0f)
	{
		modelStack.PushMatrix();
		modelStack.Translate(-25, -1, 180);
		modelStack.Rotate(rotateTree5, 1, 0, 0);
		modelStack.Scale(0.5, 0.5, 0.5);
		RenderMesh(meshList[GEO_PALMTREE], night);
		modelStack.PopMatrix();
	}
}

void SPScene::MenuUI()
{
	if ((collidedTree1 == true && tree1 == false) || (collidedTree2 == true && tree2 == false) || (collidedTree3 == true && tree3 == false) || (collidedTree4 == true && tree4 == false) || (collidedTree5 == true && tree5 == false))
	{
		modelStack.PushMatrix();
		RenderTextOnScreen(meshList[GEO_TEXT], "Chop Tree", Color(1.000, 1.000, 1.000), 1, 38, 38);
		modelStack.PopMatrix();
	}

	if (foundWood == true)
	{
		modelStack.PushMatrix();
		RenderTextOnScreen(meshList[GEO_TEXT], "Wood x", Color(0.196, 0.804, 0.196), 1, 73, 50);
		modelStack.PopMatrix();
		if (wood >= 1)
		{
			modelStack.PushMatrix();
			RenderTextOnScreen(meshList[GEO_TEXT], logcount, Color(0.196, 0.804, 0.196), 1, 79, 50);
			modelStack.PopMatrix();
		}
		else if (wood <= 0)
		{
			modelStack.PushMatrix();
			RenderTextOnScreen(meshList[GEO_TEXT], "0", Color(0.196, 0.804, 0.196), 1, 79, 50);
			modelStack.PopMatrix();
		}
	}
	else
	{
		modelStack.PushMatrix();
		RenderTextOnScreen(meshList[GEO_TEXT], "???", Color(0.196, 0.804, 0.196), 1, 77, 50);
		modelStack.PopMatrix();
	}

	if ((collidedStone1 == true && stone1 == false) || (collidedStone2 == true && stone2 == false) || (collidedStone3 == true && stone3 == false) || (collidedStone4 == true && stone4 == false) || (collidedStone5 == true && stone5 == false) || (collidedStone6 == true && stone6 == false) || (collidedStone7 == true && stone7 == false))
	{
		modelStack.PushMatrix();
		RenderTextOnScreen(meshList[GEO_TEXT], "Break Stone", Color(1.000, 1.000, 1.000), 1, 38, 38);
		modelStack.PopMatrix();
	}

	if (foundStone == true)
	{
		modelStack.PushMatrix();
		RenderTextOnScreen(meshList[GEO_TEXT], "Stone x", Color(0.196, 0.804, 0.196), 1, 72, 48);
		modelStack.PopMatrix();
		if (stone >= 1)
		{
			modelStack.PushMatrix();
			RenderTextOnScreen(meshList[GEO_TEXT], stoneCount, Color(0.196, 0.804, 0.196), 1, 79, 48);
			modelStack.PopMatrix();
		}
		else if (stone <= 0)
		{
			modelStack.PushMatrix();
			RenderTextOnScreen(meshList[GEO_TEXT], "0", Color(0.196, 0.804, 0.196), 1, 79, 48);
			modelStack.PopMatrix();
		}
	}
	else
	{
		modelStack.PushMatrix();
		RenderTextOnScreen(meshList[GEO_TEXT], "???", Color(0.196, 0.804, 0.196), 1, 77, 48);
		modelStack.PopMatrix();
	}

	//Render puzzle prompts
	if (collidedPuzzle1 && Puzzle1->PuzzleSolved)
	{
		RenderTextOnScreen(meshList[GEO_TEXT], "Puzzle solved", Color(0, 1, 0), 2, 11, 17);
		RenderTextOnScreen(meshList[GEO_TEXT], "Puzzle resets in:", Color(0, 1, 0), 2, 11, 16);
		RenderTextOnScreen(meshList[GEO_TEXT], Puzzle1->returntimer(), Color(0, 1, 0), 2, 11, 15);
	}
	else if (collidedPuzzle1 && !Puzzle1->PuzzleSolved)
	{
		RenderTextOnScreen(meshList[GEO_TEXT], "Press 'E' to attempt puzzle", Color(1.000, 1.000, 1.000), 2, 11, 15);
	}

	if (collidedPuzzle2 && Puzzle2->PuzzleSolved)
	{
		RenderTextOnScreen(meshList[GEO_TEXT], "Puzzle solved", Color(0, 1, 0), 2, 11, 17);
		RenderTextOnScreen(meshList[GEO_TEXT], "Puzzle resets in:", Color(0, 1, 0), 2, 11, 16);
		RenderTextOnScreen(meshList[GEO_TEXT], Puzzle2->returntimer(), Color(0, 1, 0), 2, 11, 15);

	}
	else if (collidedPuzzle2 && !Puzzle2->PuzzleSolved)
	{
		RenderTextOnScreen(meshList[GEO_TEXT], "Press 'E' to attempt puzzle", Color(1.000, 1.000, 1.000), 2, 11, 15);
	}
	//FishText
	if (foundFish == true)
	{
		modelStack.PushMatrix();
		RenderTextOnScreen(meshList[GEO_TEXT], "Fish x", Color(0.196, 0.804, 0.196), 1, 73, 46);
		modelStack.PopMatrix();
		if (fish >= 1)
		{
			modelStack.PushMatrix();
			RenderTextOnScreen(meshList[GEO_TEXT], fishCount, Color(0.196, 0.804, 0.196), 1, 79, 46);
			modelStack.PopMatrix();
		}
		else if (fish <= 0)
		{
			modelStack.PushMatrix();
			RenderTextOnScreen(meshList[GEO_TEXT], "0", Color(0.196, 0.804, 0.196), 1, 79, 46);
			modelStack.PopMatrix();
		}
	}
	else
	{
		modelStack.PushMatrix();
		RenderTextOnScreen(meshList[GEO_TEXT], "???", Color(0.196, 0.804, 0.196), 1, 77, 46);
		modelStack.PopMatrix();
	}


	if (foundMeat == true)
	{
		modelStack.PushMatrix();
		RenderTextOnScreen(meshList[GEO_TEXT], "Meat x", Color(0.196, 0.804, 0.196), 1, 73, 44);
		modelStack.PopMatrix();
		if (meat >= 1)
		{
			modelStack.PushMatrix();
			RenderTextOnScreen(meshList[GEO_TEXT], meatCount, Color(0.196, 0.804, 0.196), 1, 79, 44);
			modelStack.PopMatrix();
		}
		else if (meat <= 0)
		{
			modelStack.PushMatrix();
			RenderTextOnScreen(meshList[GEO_TEXT], "0", Color(0.196, 0.804, 0.196), 1, 79, 44);
			modelStack.PopMatrix();
		}
	}
	else
	{
		modelStack.PushMatrix();
		RenderTextOnScreen(meshList[GEO_TEXT], "???", Color(0.196, 0.804, 0.196), 1, 77, 44);
		modelStack.PopMatrix();
	}


	//Crafting Menu
	if (collidedCrafting == false)
	{
		craftItems = false;
		craftingOptions = false;
		displayCraftItems = false;
		toolOptions = false;
	}

	if (collidedCrafting == true && displayCraftItems == false)
	{
		modelStack.PushMatrix();
		RenderTextOnScreen(meshList[GEO_TEXT], "[E] Craft Items", Color(1.000, 1.000, 1.000), 1.2, 25, 30);
		modelStack.PopMatrix();
	}
	if (craftingOptions == true && collidedCrafting == true)
	{
		displayCraftItems = true;
		modelStack.PushMatrix();
		RenderTextOnScreen(meshList[GEO_TEXT], "1) Tools", Color(1.000, 1.000, 1.000), 1.2, 27, 32);
		modelStack.PopMatrix();
	}

	if (toolOptions == true && collidedCrafting == true)
	{
		craftingOptions = false;
		modelStack.PushMatrix();
		RenderTextOnScreen(meshList[GEO_TEXT], "[2] Axe", Color(1.000, 1.000, 1.000), 1.2, 25, 35);
		modelStack.PopMatrix();

		modelStack.PushMatrix();
		RenderTextOnScreen(meshList[GEO_TEXT], "[3] Pickaxe", Color(1.000, 1.000, 1.000), 1.2, 25, 34);
		modelStack.PopMatrix();

		modelStack.PushMatrix();
		RenderTextOnScreen(meshList[GEO_TEXT], "[4] Fishing Rod", Color(1.000, 1.000, 1.000), 1.2, 25, 33);
		modelStack.PopMatrix();

		modelStack.PushMatrix();
		RenderTextOnScreen(meshList[GEO_TEXT], "[5] Machete", Color(1.000, 1.000, 1.000), 1.2, 25, 32);
		modelStack.PopMatrix();
	}

	if (toolOptions == true && collidedCrafting == true)
	{
		if (Application::IsKeyPressed('2') && haveAxe == false)
		{
			if (wood >= 1 && stone >= 1)
			{
				wood--;
				stone--;
				craftItems = false;
				craftingOptions = false;
				displayCraftItems = false;
				craftingOptions = false;
				toolOptions = false;
				haveAxe = true;
			}
		}

		if (Application::IsKeyPressed('3') && havePickaxe == false)
		{
			if (wood >= 1 && stone >= 1)
			{
				wood--;
				stone--;
				craftItems = false;
				craftingOptions = false;
				displayCraftItems = false;
				craftingOptions = false;
				toolOptions = false;
				havePickaxe = true;
			}
		}

		if (Application::IsKeyPressed('4') && haveFishingRod == false)
		{
			if (wood >= 1 && stone >= 1)
			{
				wood--;
				stone--;
				craftItems = false;
				craftingOptions = false;
				displayCraftItems = false;
				craftingOptions = false;
				toolOptions = false;
				haveFishingRod = true;
			}
		}

		if (Application::IsKeyPressed('5') && haveMachete == false)
		{
			if (wood >= 1 && stone >= 1)
			{
				wood--;
				stone--;
				craftItems = false;
				craftingOptions = false;
				displayCraftItems = false;
				craftingOptions = false;
				toolOptions = false;
				haveMachete = true;
			}
		}
	}

	if (Application::IsKeyPressed(VK_LBUTTON) && HoldingFish == true)
	{
		if (eatingIdle >= 0.1f)
		{
			modelStack.PushMatrix();
			RenderTextOnScreen(meshList[GEO_TEXT], "Eating fish.", Color(1.000, 1.000, 1.000), 1.2, 30, 29);
			modelStack.PopMatrix();
		}
		if (eatingIdle >= 1.5f)
		{
			modelStack.PushMatrix();
			RenderTextOnScreen(meshList[GEO_TEXT], "Eating fish..", Color(1.000, 1.000, 1.000), 1.2, 30, 29);
			modelStack.PopMatrix();
		}
		if (eatingIdle >= 3.5f)
		{
			modelStack.PushMatrix();
			RenderTextOnScreen(meshList[GEO_TEXT], "Eating fish...", Color(1.000, 1.000, 1.000), 1.2, 30, 29);
			modelStack.PopMatrix();
		}
	}

	if (Application::IsKeyPressed(VK_LBUTTON) && HoldingMeat == true)
	{
		if (eatingIdleMeat >= 0.1f)
		{
			modelStack.PushMatrix();
			RenderTextOnScreen(meshList[GEO_TEXT], "Eating meat.", Color(1.000, 1.000, 1.000), 1.2, 30, 29);
			modelStack.PopMatrix();
		}
		if (eatingIdleMeat >= 1.5f)
		{
			modelStack.PushMatrix();
			RenderTextOnScreen(meshList[GEO_TEXT], "Eating meat..", Color(1.000, 1.000, 1.000), 1.2, 30, 29);
			modelStack.PopMatrix();
		}
		if (eatingIdleMeat >= 3.5f)
		{
			modelStack.PushMatrix();
			RenderTextOnScreen(meshList[GEO_TEXT], "Eating meat...", Color(1.000, 1.000, 1.000), 1.2, 30, 29);
			modelStack.PopMatrix();
		}
	}
} 

void SPScene::RenderStatus()
{
	if (!Menu)
	{
		RenderTextOnScreen(meshList[GEO_TEXT], "HUNGER ", Color(1, 0, 0), 1, 62, 57);
		RenderTextOnScreen(meshList[GEO_TEXT], Hungercount, Color(1, 0, 0), 1, 69, 57);

		RenderTextOnScreen(meshList[GEO_TEXT], "STAMINA ", Color(1.000, 1.000, 0.000), 1, 60, 54);
		RenderTextOnScreen(meshList[GEO_TEXT], Staminacount, Color(1.000, 1.000, 0.000), 1, 69, 54);

		RenderTextOnScreen(meshList[GEO_TEXT], "HEALTH ", Color(0, 1, 0), 1, 2, 57);
		RenderTextOnScreen(meshList[GEO_TEXT], Healthcount, Color(0, 1, 0), 1, 9, 57);
		
		for (int i = 8; i < 10; i++)
		{
			if (Animal[i] != NULL)
			{
				if (Animal[i]->WolvesState == 0)
				{
					RenderTextOnScreen(meshList[GEO_TEXT], "CHASING", Color(1, 0, 0), 1, 2, 54);
				}
			}
		}
	}
}

void SPScene::RenderMeshOnScreen(Mesh* mesh, int x, int y, int sizex, int sizey, int sizez, int rotateAngle, int rX, int rY, int rZ)
{
	glDisable(GL_DEPTH_TEST);
	Mtx44 ortho;
	ortho.SetToOrtho(0, 1600, 0, 900, -10, 10); //size of screen UI
	projectionStack.PushMatrix();
	projectionStack.LoadMatrix(ortho);
	viewStack.PushMatrix();
	viewStack.LoadIdentity();
	modelStack.PushMatrix();
	modelStack.LoadIdentity();
	modelStack.Translate(x, y, 0);
	modelStack.Rotate(rotateAngle, rX, rY, rZ);
	modelStack.Scale(sizex, sizey, sizez);

	modelStack.Scale(1, 1, 1);

	RenderMesh(mesh, false);
	projectionStack.PopMatrix();
	viewStack.PopMatrix();
	modelStack.PopMatrix();
	glEnable(GL_DEPTH_TEST);
}

void SPScene::RenderMesh(Mesh *mesh, bool enableLight)
{
	Mtx44 MVP, modelView, modelView_inverse_transpose;

	MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);
	modelView = viewStack.Top() * modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MODELVIEW], 1, GL_FALSE, &modelView.a[0]);
	if (enableLight)
	{
		glUniform1i(m_parameters[U_LIGHTENABLED], 1);
		modelView_inverse_transpose = modelView.GetInverse().GetTranspose();
		glUniformMatrix4fv(m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE], 1, GL_FALSE, &modelView_inverse_transpose.a[0]);

		//load material
		glUniform3fv(m_parameters[U_MATERIAL_AMBIENT], 1, &mesh->material.kAmbient.r);
		glUniform3fv(m_parameters[U_MATERIAL_DIFFUSE], 1, &mesh->material.kDiffuse.r);
		glUniform3fv(m_parameters[U_MATERIAL_SPECULAR], 1, &mesh->material.kSpecular.r);
		glUniform1f(m_parameters[U_MATERIAL_SHININESS], mesh->material.kShininess);
	}
	else
	{
		glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	}
	if (mesh->textureID > 0)
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, mesh->textureID);
		glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	}
	else
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 0);
	}

	mesh->Render();
	if (mesh->textureID > 0)
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}

void SPScene::RenderSkyBox()
{
	if (light[0].position.x <= -290)
	{
		changeSkyBox = true;
	}
	else if (light[0].position.x >= 290)
	{
		changeSkyBox = false;
	}
	


	modelStack.PushMatrix();
	modelStack.Translate(0, 9, -400);
	modelStack.Scale(80.06, 80.06, 80.06);
	if (changeSkyBox == true)
	{
		RenderMesh(meshList[GEO_NIGHTFRONT], false);
	}
	else if (changeSkyBox == false)
	{
		RenderMesh(meshList[GEO_FRONT], false);
	}
	modelStack.PopMatrix();


	modelStack.PushMatrix();
	modelStack.Translate(0, 9, 400);
	modelStack.Rotate(180, 0.0f, 1.0f, 0.0f);
	modelStack.Scale(80.06, 80.06, 80.06);
	if (changeSkyBox == true)
	{
		RenderMesh(meshList[GEO_NIGHTBACK], false);
	}
	else if (changeSkyBox == false)
	{
		RenderMesh(meshList[GEO_BACK], false);
	}
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(0, 408, 0);
	modelStack.Rotate(90, 1.0f, 0.0f, 0.0f);
	modelStack.Rotate(90, 0.0f, 0.0f, 1.0f);
	modelStack.Scale(80.06, 80.06, 80.06);
	if (changeSkyBox == true)
	{
		RenderMesh(meshList[GEO_NIGHTTOP], false);
	}
	else if (changeSkyBox == false)
	{
		RenderMesh(meshList[GEO_TOP], false);
	}
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(0, -90, 0);
	modelStack.Rotate(-90, 1.0f, 0.0f, 0.0f);
	modelStack.Rotate(-90, 0.0f, 0.0f, 1.0f);
	modelStack.Scale(80.06, 80.06, 80.06);
	if (changeSkyBox == true)
	{
		RenderMesh(meshList[GEO_NIGHTBOTTOM], false);
	}
	else if (changeSkyBox == false)
	{
		RenderMesh(meshList[GEO_BOTTOM], false);
	}
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-400, 9, 0);
	modelStack.Rotate(90, 0.0f, 1.0f, 0.0f);
	modelStack.Scale(80.06, 80.06, 80.06);
	if (changeSkyBox == true)
	{
		RenderMesh(meshList[GEO_NIGHTLEFT], false);
	}
	else if (changeSkyBox == false)
	{
		RenderMesh(meshList[GEO_LEFT], false);
	}
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(400, 9, 0);
	modelStack.Rotate(-90, 0.0f, 1.0f, 0.0f);
	modelStack.Scale(80.06, 80.06, 80.06);
	if (changeSkyBox == true)
	{
		RenderMesh(meshList[GEO_NIGHTRIGHT], false);
	}
	else if (changeSkyBox == false)
	{
		RenderMesh(meshList[GEO_RIGHT], false);
	}
	modelStack.PopMatrix();
}

void SPScene::RenderText(Mesh* mesh, std::string text, Color color)
{
	if (!mesh || mesh->textureID <= 0) //Proper error check
		return;

	glDisable(GL_DEPTH_TEST);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 1);
	glUniform3fv(m_parameters[U_TEXT_COLOR], 1, &color.r);
	glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh->textureID);
	glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);

	for (unsigned i = 0; i < text.length(); ++i)
	{
		Mtx44 characterSpacing;
		characterSpacing.SetToTranslation(i * 1.0f, 0, 0); //1.0f is the spacing of each character, you may change this value
		Mtx44 MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top() * characterSpacing;
		glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);

		mesh->Render((unsigned)text[i] * 6, 6);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 0);
	glEnable(GL_DEPTH_TEST);
}

void SPScene::RenderTextOnScreen(Mesh* mesh, std::string text, Color color, float size, float x, float y)
{
	if (!mesh || mesh->textureID <= 0) //Proper error check
		return;

	glDisable(GL_DEPTH_TEST);

	Mtx44 ortho;
	ortho.SetToOrtho(0, 80, 0, 60, -10, 10); //size of screen UI
	projectionStack.PushMatrix();
	projectionStack.LoadMatrix(ortho);
	viewStack.PushMatrix();
	viewStack.LoadIdentity(); //No need camera for ortho mode
	modelStack.PushMatrix();
	modelStack.LoadIdentity(); //Reset modelStack
	modelStack.Scale(size, size, size);
	modelStack.Translate(x, y, 0);

	glUniform1i(m_parameters[U_TEXT_ENABLED], 1);
	glUniform3fv(m_parameters[U_TEXT_COLOR], 1, &color.r);
	glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh->textureID);
	glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);

	int j = 0;
	int k = 0;
	for (unsigned i = 0; i < text.length(); ++i, k++)
	{
		if (text[i] == '\n')
		{
			j -= 1;
			k = 0;
			i += 2;
		}

		Mtx44 characterSpacing;
		characterSpacing.SetToTranslation(k * 0.8f, j, 0); //1.0f is the spacing of each character, you may change this value
		Mtx44 MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top() * characterSpacing;
		glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);

		mesh->Render((unsigned)text[i] * 6, 6);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 0);
	projectionStack.PopMatrix();
	viewStack.PopMatrix();
	modelStack.PopMatrix();
	glEnable(GL_DEPTH_TEST);
}

void SPScene::RenderAxeArm()
{

	glEnable(GL_DEPTH_TEST);
	Mtx44 Persp;
	Persp.SetToPerspective(60.f, 4.f / 3.f, 0.005, 20.0f);
	projectionStack.PushMatrix();
	projectionStack.LoadMatrix(Persp);
	viewStack.PushMatrix();
	viewStack.LoadIdentity();
	modelStack.PushMatrix();
	modelStack.LoadIdentity();
	modelStack.PushMatrix();
	modelStack.Translate(-0.01, 0, 0);
	modelStack.Translate(Axemovement.x, Axemovement.y, Axemovement.z);
	modelStack.Rotate(90, 0, 1, 0);
	modelStack.Rotate(-5, 1, 0, 0);
	modelStack.Rotate(-axerotate, 0, 0, 1);
	modelStack.Scale(0.05, 0.05, 0.05);

	if (HoldingPick && havePickaxe)
	{
		RenderMesh(meshList[GEO_PICKAXE], true);
	}
	else if (HoldingAxe && haveAxe)
	{
		RenderMesh(meshList[GEO_AXE], true);
	}
	else if (HoldingMachete && haveMachete)
	{
		modelStack.PushMatrix();
		modelStack.Translate(0, 0.7, 0);
		modelStack.Scale(0.5, 0.5, 0.5);
		RenderMesh(meshList[GEO_MACHETE], true);
		modelStack.PopMatrix();
	}
	else if (HoldingTorch)
	{
		modelStack.PushMatrix();
		modelStack.Rotate(90, 0, 1, 0);
		modelStack.Translate(0, -1.8, 0);
			modelStack.PushMatrix();
			modelStack.Scale(0.5, 1, 1);
			modelStack.Rotate(180, 0, 1, 0);
			modelStack.Translate(0, -2.3, 0);
			RenderMesh(meshList[GEO_TORCHFLAME], false);
			modelStack.PopMatrix();
		RenderMesh(meshList[GEO_TORCH], false);
		modelStack.PopMatrix();
	}
		
	modelStack.PushMatrix();
	modelStack.PushMatrix();
	/*modelStack.Scale(0.7, 0.3, 0.8);
	modelStack.Translate(0.5, -1.2, 0);*/
	RenderMesh(meshList[GEO_ARM], true);
	modelStack.PopMatrix();
	modelStack.PopMatrix();

	projectionStack.PopMatrix();
	viewStack.PopMatrix();
	modelStack.PopMatrix();

	glEnable(GL_DEPTH_TEST);

}

void SPScene::RenderAI()
{
	for (int i = 0; i < 8; i++)
	{
		if (Animal[i] != NULL)
		{
			modelStack.PushMatrix();
				modelStack.Translate(Animal[i]->position.x, Animal[i]->position.y, Animal[i]->position.z);
				modelStack.Rotate(90, 0, 1, 0);
				modelStack.Rotate(Animal[i]->rotateAmt, 0, 1, 0);
				modelStack.Rotate(Animal[i]->deadrotate, 1, 0, 0);
				modelStack.Scale(2, 2, 2);
				RenderMesh(meshList[GEO_COW], night); //Cow Body
				modelStack.PushMatrix();
					modelStack.Translate(0, 0.2, -0.05);
					modelStack.PushMatrix();
						modelStack.Translate((static_cast<Cow*>(Animal[i]))->leftlegangle / 80, -(static_cast<Cow*>(Animal[i]))->leftlegangle / 70, 0);
						modelStack.Rotate((static_cast<Cow*>(Animal[i]))->leftlegangle, 0, 0, 1);
						RenderMesh(meshList[GEO_COWLEG], night); //front left
					modelStack.PopMatrix();
					modelStack.PushMatrix();
						modelStack.Translate(0, 0, 0.8);
						modelStack.Translate((static_cast<Cow*>(Animal[i]))->rightlegangle / 80, -(static_cast<Cow*>(Animal[i]))->rightlegangle / 70, 0);
						modelStack.Rotate((static_cast<Cow*>(Animal[i]))->rightlegangle, 0, 0, 1);
						RenderMesh(meshList[GEO_COWLEG], night); //front right
					modelStack.PopMatrix();
						modelStack.Translate(-1, 0, 0);
					modelStack.PushMatrix();
						modelStack.Translate((static_cast<Cow*>(Animal[i]))->rightlegangle / 80, -(static_cast<Cow*>(Animal[i]))->rightlegangle / 70, 0);
						modelStack.Rotate((static_cast<Cow*>(Animal[i]))->rightlegangle + 10, 0, 0, 1);
						RenderMesh(meshList[GEO_COWLEG], night); //back left
					modelStack.PopMatrix();
					modelStack.PushMatrix();
						modelStack.Translate(0, 0, 0.8);
						modelStack.Translate((static_cast<Cow*>(Animal[i]))->leftlegangle / 80, -(static_cast<Cow*>(Animal[i]))->leftlegangle / 70, 0);
						modelStack.Rotate((static_cast<Cow*>(Animal[i]))->leftlegangle + 10, 0, 0, 1);
						RenderMesh(meshList[GEO_COWLEG], night); //back right
					modelStack.PopMatrix();
				modelStack.PopMatrix();
			modelStack.PopMatrix();

			}
		}

		for (int i = 8; i < 10; i++)
		{
			if (Animal[i] != NULL)
			{
					modelStack.PushMatrix();
						modelStack.Translate(Animal[i]->position.x, Animal[i]->position.y, Animal[i]->position.z);
						modelStack.Rotate(180, 0, 1, 0);
						modelStack.Rotate(Animal[i]->rotateAmt, 0, 1, 0);
						modelStack.Rotate(Animal[i]->deadrotate, 1, 0, 0);
						modelStack.Scale(2, 2, 2);
						RenderMesh(meshList[GEO_WOLF], night); //Wolf Body
						modelStack.PushMatrix();
							modelStack.Translate(-0.35, 0.1, -1);
							modelStack.PushMatrix();
								modelStack.Translate(0, (static_cast<Wolf*>(Animal[i]))->rightlegangle / 70, (static_cast<Wolf*>(Animal[i]))->rightlegangle / 120);
								modelStack.Rotate((static_cast<Wolf*>(Animal[i]))->rightlegangle + 10, 1, 0, 0);
								RenderMesh(meshList[GEO_WOLFLEG], night); //back right
								modelStack.PopMatrix();
								modelStack.PushMatrix();
									modelStack.Translate(0.4, 0, 0);
									modelStack.Translate(0, (static_cast<Wolf*>(Animal[i]))->leftlegangle / 70, (static_cast<Wolf*>(Animal[i]))->leftlegangle / 120);
									modelStack.Rotate((static_cast<Wolf*>(Animal[i]))->leftlegangle+10, 1, 0, 0);
									RenderMesh(meshList[GEO_WOLFLEG], night); //back left
								modelStack.PopMatrix();
								modelStack.Translate(0, 0, 1);
								modelStack.PushMatrix();
									modelStack.Translate(0, (static_cast<Wolf*>(Animal[i]))->leftlegangle / 70, (static_cast<Wolf*>(Animal[i]))->leftlegangle / 120);
									modelStack.Rotate((static_cast<Wolf*>(Animal[i]))->leftlegangle, 1, 0, 0);
									RenderMesh(meshList[GEO_WOLFLEG], night); //front right	
								modelStack.PopMatrix();
								modelStack.PushMatrix();
									modelStack.Translate(0.4, 0, 0);
									modelStack.Translate(0, (static_cast<Wolf*>(Animal[i]))->rightlegangle / 70, (static_cast<Wolf*>(Animal[i]))->rightlegangle / 120);
									modelStack.Rotate((static_cast<Wolf*>(Animal[i]))->rightlegangle, 1, 0, 0);
									RenderMesh(meshList[GEO_WOLFLEG], night); //front left
								modelStack.PopMatrix();
						modelStack.PopMatrix();
					modelStack.PopMatrix();
			}
		}

	
}
// Functions
float SPScene::dotProductDistance(Vector3 positionPlayer, Vector3 positionObstacle)
{
	float var_A, var_B, var_C, var_D;
	var_A = positionObstacle.x - positionPlayer.x;
	var_B = positionObstacle.z - positionPlayer.z;
	var_C = pow(var_A, 2) + pow(var_B, 2);
	var_D = sqrt(var_C);
	return var_D;
}

void SPScene::getAngle(float &angle, Vector3 posP, Vector3 objPos)
{
	Vector3 fowardVec(0, 0, 1);
	Vector3 direction(posP - objPos);

	if (direction.IsZero() == false)
	{
		direction.Normalize();
		float dot = fowardVec.Dot(direction);
		angle = acos(dot);
		angle = Math::RadianToDegree(angle);

		if (direction.x < 0)
		{
			angle = -angle;
		}
	}
	else
	{
		return;
	}
}

void SPScene::Exit()
{
	// Cleanup VBO here
	for (int i = 0; i < NUM_GEOMETRY; ++i)
	{
		if (meshList[i] != NULL)
			delete meshList[i];

		meshList[i] = NULL;

	}

	for (int i = 0; i < 8; ++i)
	{
		if (Animal[i] != NULL)
		{
			delete Animal[i];
			Animal[i] = NULL;
		}
	}
	delete Puzzle1;
	delete Puzzle2;

	Puzzle1 = NULL;
	Puzzle2 = NULL;



	glDeleteVertexArrays(1, &m_vertexArrayID);

	glDeleteProgram(m_programID);

}

void SPScene::RenderFishingGame()
{
	if (FishingGame)
	{
	RenderMeshOnScreen(meshList[GEO_BACKGROUNDDESIGN], 1400, 500, 3, 5.8, 1, 0, 1, 1, 1);
	RenderMeshOnScreen(meshList[GEO_BACKGROUND], 1400, 500, 2, 5, 1, 0, 1, 1, 1);
	RenderMeshOnScreen(meshList[GEO_FISHINGBAR], 1400, 500, 2, 4, 1, 0, 1, 1, 1);
	RenderMeshOnScreen(meshList[GEO_FISHINGBAR], 1400, ybar, 2, 2, 1, 0, 1, 1, 1);
	}
}