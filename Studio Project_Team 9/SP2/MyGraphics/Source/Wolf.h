#ifndef WOLF_H
#define WOLF_H

#include "Animal.h"
#include "Vector3.h"
#include "Camera3.h"
#include "irrKlang.h"

#pragma comment(lib, "irrKlang.lib")

using namespace irrklang;

class Wolf :
	public Animal
{
	

private:
	enum leg {
		LEFTLEG,
		RIGHTLEG,
	};
	bool moving;
	leg legmoving;

public:
	float leftlegangle;
	float rightlegangle;
	Wolf();
	~Wolf();


	void inrange(Camera cam);
	void setDirection(Object o[], Camera cam);
	void move(DIRECTION dir, Object o[], Camera cam);


	ISoundEngine* engine = createIrrKlangDevice();

	void Animate(double dt);


};


#endif