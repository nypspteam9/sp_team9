#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string>
#include <iostream>
#include "Application.h"


class CPuzzles
{
private:

	std::string puzzlearray[10];
	std::string ansarray[10][10];
	std::string puzzle;
	int puzzlenum;
	double g_dBounceTime = 0.0;

public:
	CPuzzles();
	~CPuzzles();
	int puzzleXpos;
	float puzzleRespawn;
	float puzzleRespawnTimer(double dt);
	void setPuzzlenum(int a);
	void reset();
	bool PuzzleSolved;
	std::string printPuzzles();
	std::string puzzleAns;
	std::string returntimer();
	bool ansPuzzle(double g_dElapsedTime);
	bool checkAns();

	bool wrongAnimate = false;
	float wrongAnimateTime;


};

