#include "Puzzles.h"



CPuzzles::CPuzzles()
{
	puzzlenum = rand() % 10;
	puzzlearray[0] = "I have a head & no body, \n but I have a tail. \n What am I?";
	ansarray[0][0] = "coin";
	ansarray[0][1] = "a coin";

	puzzlearray[1] = "What do you call a bear \n without an ear?";
	ansarray[1][0] = "b";
	ansarray[1][1] = "the letter b";

	puzzlearray[2] = "What has a bed but doesnt \n sleep and a mouth \n but never eats?";
	ansarray[2][0] = "river";
	ansarray[2][1] = "a river";
	ansarray[2][2] = "the river";

	puzzlearray[3] = "You buy me to eat, \n but never eat me. \n What am I?";
	ansarray[3][0] = "silverware";
	ansarray[3][1] = "spoon";
	ansarray[3][2] = "a spoon";
	ansarray[3][3] = "knife";
	ansarray[3][4] = "a knife";
	ansarray[3][5] = "fork";
	ansarray[3][6] = "a fork";
	ansarray[3][7] = "bowl";
	ansarray[3][8] = "a bowl";
	ansarray[3][9] = "plate";
	ansarray[3][10] = "a plate";

	puzzlearray[4] = "what is 1+1?";
	ansarray[4][0] = "2";

	puzzlearray[5] = "If you are running \n in a race and pass \n the second place person, \n what place are you in??";
	ansarray[5][0] = "2";
	ansarray[5][1] = "2nd";
	ansarray[5][2] = "2nd place";
	ansarray[5][3] = "second place";
	ansarray[5][4] = "second";

	puzzlearray[6] = "I can be made and I can \n be played.I can be\n cracked and I can be \n told. What am I ?";
	ansarray[6][0] = "joke";
	ansarray[6][1] = "a joke";

	puzzlearray[7] = "We see it once in a year, \n twice in a week but never \n in a day. What is it?";
	ansarray[7][0] = "e";
	ansarray[7][1] = "letter e";
	ansarray[7][2] = "the letter e";

	puzzlearray[8] = "I am a container with no \n sides and no lid, yet \n golden treasure lays inside.\n What am I?";
	ansarray[8][0] = "egg";
	ansarray[8][1] = "an egg";
	ansarray[8][2] = "the egg";

	puzzlearray[9] = "You go at red and stop \n at green. What am I?";
	ansarray[9][0] = "watermelon";
	ansarray[9][1] = "a watermelon";
	ansarray[9][2] = "the watermelon";

	
	puzzleRespawn = 0;
	puzzle = puzzlearray[puzzlenum];
	puzzleXpos = 0;
	PuzzleSolved = false;
}

float CPuzzles::puzzleRespawnTimer(double dt)
{
	puzzleRespawn -= dt;
	if (puzzleRespawn <= 0)
	{
		reset();
		return 0;
	}
	else
	return puzzleRespawn;
}

void CPuzzles::setPuzzlenum(int a)
{
	puzzlenum = a;
	puzzle = puzzlearray[a];
}

void CPuzzles::reset()
{
	puzzlenum = rand() % 10;
	puzzle = puzzlearray[puzzlenum];
	PuzzleSolved = false;
	puzzleAns = "";
	puzzleXpos = 0;
}


CPuzzles::~CPuzzles()
{
	
}

std::string CPuzzles::printPuzzles()
{
	return puzzle;
}

std::string CPuzzles::returntimer()
{
	int respawnint = (puzzleRespawn + 0.5);
	std::string temp =  std::to_string(respawnint);
	return temp;
}

bool CPuzzles::ansPuzzle(double g_dElapsedTime)
{
	bool bSomethingHappened = false;
	if (g_dBounceTime > g_dElapsedTime)
	{
		return false;
	}
		
	if (Application::IsKeyPressed('A'))
	{
		  puzzleAns += 'a';
		  bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('B'))
	{
		puzzleAns += 'b';
		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('C'))
	{
		puzzleAns += 'c';
		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('D'))
	{
		puzzleAns += 'd';
		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('E'))
	{
		puzzleAns += 'e';
		 bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('F'))
	{
		puzzleAns += 'f';
		 bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('G'))
	{
		puzzleAns += 'g';
		 bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('H'))
	{
		puzzleAns += 'h';
		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('I'))
	{
		puzzleAns += 'i';
		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('J'))
	{
		puzzleAns += 'j';
		 bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('K'))
	{
		puzzleAns += 'k';
		 bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('L'))
	{
		puzzleAns += 'l';
		 bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('M'))
	{
		puzzleAns += 'm';
		 bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('N'))
	{
		puzzleAns += 'n';
		 bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('O'))
	{
		puzzleAns += 'o';
		 bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('P'))
	{
		puzzleAns += 'p';
		 bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('Q'))
	{
		puzzleAns += 'q';
		 bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('R'))
	{
		puzzleAns += 'r';
		 bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('S'))
	{
		puzzleAns += 's';
		 bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('T'))
	{
		puzzleAns += 't';
		 bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('U'))
	{
		puzzleAns += 'u';
		 bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('V'))
	{
		puzzleAns += 'v';
		 bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('W'))
	{
		puzzleAns += 'w';
		 bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('X'))
	{
		puzzleAns += 'x';
		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('Y'))
	{
		puzzleAns += 'y';
		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('Z'))
	{
		puzzleAns += 'z';
		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('1'))
	{
		puzzleAns += '1';
		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('2'))
	{
		puzzleAns += '2';
		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('3'))
	{
		puzzleAns += '3';
		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('4'))
	{
		puzzleAns += '4';
		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('5'))
	{
		puzzleAns += '5';
		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('6'))
	{
		puzzleAns += '6';
		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('7'))
	{
		puzzleAns += '7';
		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('8'))
	{
		puzzleAns += '8';
		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('9'))
	{
		puzzleAns += '9';
		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed('0'))
	{
		puzzleAns += '0';
		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed(VK_BACK))
	{
		if (puzzleAns.size() > 0)
		{
			puzzleAns.resize(puzzleAns.size() - 1);
		}
		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed(VK_SPACE))
	{
		puzzleAns += ' ';

		bSomethingHappened = true;
	}
	else if (Application::IsKeyPressed(VK_RETURN))
	{
		if (checkAns())
		{
			puzzleXpos = 1000;
			puzzleRespawn = 10;
			return true;
		}
		else
		{
			wrongAnimate = true;
			wrongAnimateTime = g_dElapsedTime+0.5;
		}
		bSomethingHappened = true;
	}
	if (bSomethingHappened)
	{
		// set the bounce time to some time in the future to prevent accidental triggers
		g_dBounceTime = g_dElapsedTime +0.15; // 125ms should be enough
	}
	if (this->wrongAnimate)
	{
		if (wrongAnimateTime <= g_dElapsedTime)
			wrongAnimate = false;
	}
	return false;


}

bool CPuzzles::checkAns()
{
	for (int i = 0; ansarray[puzzlenum][i] != ""; i++)
	{
		if (ansarray[puzzlenum][i].compare(puzzleAns) == 0)
		{
			return true;
		}
	}
		return false;
}

