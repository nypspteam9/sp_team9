
#ifndef APPLICATION_H
#define APPLICATION_H

#include "timer.h"
#include "irrKlang.h"

#pragma comment(lib, "irrKlang.lib")

using namespace irrklang;


enum GAMESTATE
{
	GAME_INTRO,
	GAME_MENU,
};

class Application
{


public:
	Application();
	~Application();
	void Init();
	void Run();
	void Exit();
	static bool IsKeyPressed(unsigned short key);
	static void GetMousePos(double & xpos, double & ypos);
	static void SetMousePos();
	static void endgame();
	bool instructionMenu;

	//static bool getGame();
	//static bool getQuit();

private:

	float MainCameraX;
	//float MainCameraY;
	float MainCameraZ;

	float HouseCameraX;
	float HouseCameraZ;
	bool puzzle;
	bool clicked;
	bool clicked2;
	bool quit;
	bool game;
	bool instructions;
	

	//Declare a window object
	StopWatch m_timer;

	ISoundEngine* engine = createIrrKlangDevice();
};

#endif