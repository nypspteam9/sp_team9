#ifndef COW_H
#define COW_H

#include "Animal.h"
#include "Vector3.h"
#include "Camera3.h"
#include "irrKlang.h"

#pragma comment(lib, "irrKlang.lib")

using namespace irrklang;

class Cow :
	public Animal
{
private :
	enum leg {
		LEFTLEG,
		RIGHTLEG,
	};
	bool moving;
	leg legmoving;

public :
	Cow();
	~Cow();
	float leftlegangle;
	float rightlegangle;
	void setDirection(Object o[], Camera cam );
	void move(DIRECTION dir, Object o[], Camera cam);

	void Animate(double dt);

	ISoundEngine* engine = createIrrKlangDevice();
};


#endif