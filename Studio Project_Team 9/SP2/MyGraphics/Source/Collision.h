#ifndef COLLISION_H
#define COLLISION_H
#include "Vector3.h"

class Object
{
public:

	Vector3 position;
	float length;
	float width;
	float height;

	float minX;
	float maxX;
	float minY;
	float maxY;
	float minZ;
	float maxZ;

	Object();
	~Object();

	void boxset(Vector3 _position, float _length, float _width, float _height);

	/*
	ModelBox(Vector3 _position, float _length, float _height, float _width)
    {
        position = _position;
        minX = position.x - _length;
        maxX = position.x + _length;
        minY = position.y - _height;
        maxY = position.y + _height;
        minZ = position.z - _width;
        maxZ = position.z + _width;
    }
	*/

};

#endif
