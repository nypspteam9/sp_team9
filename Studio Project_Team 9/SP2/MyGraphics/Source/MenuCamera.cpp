#include "MenuCamera.h"
#include "Application.h"
#include "Mtx44.h"

MenuCamera::MenuCamera()
{
}

MenuCamera::~MenuCamera()
{
}

void MenuCamera::Init(const Vector3& pos, const Vector3& target, const Vector3& up)
{
	this->position = defaultPosition = pos;
	this->target = defaultTarget = target;
	Vector3 view = (target - position).Normalized();
	Vector3 right = view.Cross(up);
	right.y = 0;
	right.Normalize();
	this->up = defaultUp = right.Cross(view).Normalized();

	xMouse = 0;
	yMouse = 0;
}

void MenuCamera::Update(double dt)
{
	static const float CAMERA_SPEED = 5.f;
	static const float MOVEMENT_SPEED = 0.1f;

	Vector3 view = (target - position).Normalized();
	Vector3 right = view.Cross(up);


	Application::GetMousePos(xMouse, yMouse);

	if (Application::IsKeyPressed('A'))
	{
		position = position - right * MOVEMENT_SPEED;
		target = position + view;
	}
	if (Application::IsKeyPressed('D'))
	{
		position = position + right * MOVEMENT_SPEED;
		target = position + view;
	}
	if (Application::IsKeyPressed('W'))
	{
		position = position + view * MOVEMENT_SPEED;
		target = position + view;
	}
	if (Application::IsKeyPressed('S'))
	{
		position = position - view * MOVEMENT_SPEED;
		target = position + view;
	}

	if (Application::IsKeyPressed('R'))
	{
		Reset();
	}

	if (yMouse < 300)
	{


		float pitch = (float)(CAMERA_SPEED * dt * (300 - yMouse));
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();

		Mtx44 rotation;
		rotation.SetToRotation(pitch, right.x, right.y, right.z);

		view = rotation * view;
		target = position + view;
	}

	if (yMouse > 300)
	{

		float pitch = (float)(CAMERA_SPEED * dt * (300 - yMouse));
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();

		Mtx44 rotation;
		rotation.SetToRotation(pitch, right.x, right.y, right.z);

		view = rotation * view;
		target = position + view;
	}

	if (xMouse < 400)
	{
		float yaw = (float)(CAMERA_SPEED * dt * (400 - xMouse));
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();

		Mtx44 rotation;
		rotation.SetToRotation(yaw, up.x, up.y, up.z);

		view = rotation * view;
		target = position + view;
	}

	if (xMouse > 400)
	{
		float yaw = (float)(CAMERA_SPEED * dt * (400 - xMouse));
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();

		Mtx44 rotation;
		rotation.SetToRotation(yaw, up.x, up.y, up.z);

		view = rotation * view;
		target = position + view;
	}
	Application::SetMousePos();
}

void MenuCamera::Reset()
{
	position = defaultPosition;
	target = defaultTarget;
	up = defaultUp;
}