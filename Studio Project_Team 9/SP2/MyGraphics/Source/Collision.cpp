#include "Collision.h"

Object::Object()
{

}

Object::~Object()
{

}

void Object::boxset(Vector3 _position, float _length, float _width, float _height) //Set your center of the object, make a box, used for collision in camera
{
	position = _position;
	length = _length;
	width = _width;
	minX = position.x - _length;
	maxX = position.x + _length;
	minY = position.y - _height;
	maxY = position.y + _height;
	minZ = position.z - _width;
	maxZ = position.z + _width;
}
