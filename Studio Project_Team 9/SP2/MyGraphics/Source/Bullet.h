#ifndef BULLET_H
#define BULLET_H
#include "Vector3.h"

class Bullet
{
public:

	Vector3 Position;
	Vector3 Target;
	bool active;

	Bullet();
	~Bullet();

};

#endif