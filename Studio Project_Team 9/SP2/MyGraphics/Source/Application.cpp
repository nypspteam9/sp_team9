
#include "Application.h"

//Include GLEW
#include <GL/glew.h>

//Include GLFW
#include <GLFW/glfw3.h>

//Include the standard C++ headers
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>

//All Scenes used
#include "SPScene.h"
#include "SceneText.h"
#include "House.h"
#include "EndScene.h"
#include "SceneManager.h"


GLFWwindow* m_window;
GAMESTATE gamestate = GAME_INTRO;
const unsigned char FPS = 60; // FPS of this game
const unsigned int frameTime = 1000 / FPS; // time for each frame

bool Scenehouse;

//Define an error callback
static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
	_fgetchar();
}

void Application::GetMousePos(double &xpos, double &ypos)
{
	glfwGetCursorPos(m_window, &xpos, &ypos);
}

void Application::SetMousePos()
{
	glfwSetCursorPos(m_window, 800, 450);
}

//Define the key input callback
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}

bool Application::IsKeyPressed(unsigned short key)
{
	return ((GetAsyncKeyState(key) & 0x8001) != 0);
}

Application::Application()
{

}

Application::~Application()
{

}

void resize_callback(GLFWwindow* window, int w, int h)
{
	glViewport(0, 0, w, h); //update opengl the new window size
}


void Application::Init()
{
	//Set the error callback
	glfwSetErrorCallback(error_callback);
	game = false;
	//Initialize GLFW
	if (!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	//Set the GLFW window creation hints - these are optional
	glfwWindowHint(GLFW_SAMPLES, 4); //Request 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //Request a specific OpenGL version
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //Request a specific OpenGL version
												   //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL 


																   //Create a window and create its OpenGL context
	m_window = glfwCreateWindow(1600, 900, "Computer Graphics", NULL, NULL);
	glfwSetWindowSizeCallback(m_window, resize_callback);
	//If the window couldn't be created
	if (!m_window)
	{
		fprintf(stderr, "Failed to open GLFW window.\n");
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	//This function makes the context of the specified window current on the calling thread. 
	glfwMakeContextCurrent(m_window);

	//Sets the key callback
	//glfwSetKeyCallback(m_window, key_callback);

	glewExperimental = true; // Needed for core profile
							 //Initialize GLEW
	GLenum err = glewInit();

	//If GLEW hasn't initialized
	if (err != GLEW_OK)
	{
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		//return -1;
	}

	//gamestate = 1;

	Scene *menu = new SceneText();
	CSceneManager::Instance()->AddScene(menu);
	Scene *house = new House();
	CSceneManager::Instance()->AddScene(house);
	Scene *endscene = new EndScene();
	CSceneManager::Instance()->AddScene(endscene);
	Scene *scene = new SPScene();
	CSceneManager::Instance()->AddScene(scene);

	clicked = false;
	clicked2 = false;

}

void Application::Run()
{
	//Main Loop
	//Scene *scene = new SPScene();
	//scene->Init();
	//SceneText *scenehouse = new SceneText();
	//}
	engine->stopAllSounds();
	engine->play2D("../Music/MainMenu.mp3", true);



	m_timer.startTimer();    // Start timer to calculate how long it takes to render this frame
	while (!glfwWindowShouldClose(m_window) && !IsKeyPressed(VK_ESCAPE))
	{
		MainCameraX = static_cast<SPScene*>(CSceneManager::Instance()->GetCurrentScene())->camera.position.x;
		MainCameraZ = static_cast<SPScene*>(CSceneManager::Instance()->GetCurrentScene())->camera.position.z;
		puzzle = static_cast<SPScene*>(CSceneManager::Instance()->GetCurrentScene())->RenderPuzzle;

		HouseCameraX = static_cast<House*>(CSceneManager::Instance()->GetCurrentScene())->camera.position.x;
		HouseCameraZ = static_cast<House*>(CSceneManager::Instance()->GetCurrentScene())->camera.position.z;

		game = static_cast<SceneText*>(CSceneManager::Instance()->GetCurrentScene())->game;
		quit = static_cast<SceneText*>(CSceneManager::Instance()->GetCurrentScene())->quit;
		instructions = static_cast<SceneText*>(CSceneManager::Instance()->GetCurrentScene())->instructions;
		

		CSceneManager::Instance()->Update(&m_timer);
		if (CSceneManager::Instance()->returnSceneID() == CSceneManager::START_MENU)
		{
			//game = true;
			if (IsKeyPressed(VK_LBUTTON))
			{
				engine->stopAllSounds();
				engine->play2D("../Music/GamePlayMusic.mp3", true);
				if (game) 
				{
					CSceneManager::Instance()->GoToScene(3);

				}
				else if (instructions)
				{
					CSceneManager::Instance()->GoToScene(0);
				}
				else if (quit)
				{
					break;
				}
			}
		}
		if (CSceneManager::Instance()->returnSceneID() == CSceneManager::MAINGAME)
		{
			glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
			if (IsKeyPressed('P') && !puzzle)
			{
				game = true;
				quit = false;
				CSceneManager::Instance()->GoToScene(0);
			}
			if ((MainCameraX >= -35) && (MainCameraX <= -32))
			{
				if ((MainCameraZ >= 15) && (MainCameraZ <= 18))
				{
					if (IsKeyPressed(VK_RETURN) && clicked == false)
					{
						engine->stopAllSounds();
						engine->play2D("../Music/In_House_music.mp3", true);
						CSceneManager::Instance()->GoToScene(1);
						//TransportedHouse = true;
						clicked = true;
					}
				}
			}
			else
			{
				clicked = false;
			}
			

		}
		if (CSceneManager::Instance()->returnSceneID() == CSceneManager::HOUSE)
		{
			if ((HouseCameraX >= -2) && (HouseCameraX <= 1))
			{
				if ((HouseCameraZ >= -14) && (HouseCameraZ <= -11))
				{
					if (IsKeyPressed(VK_RETURN) && clicked2 == false)
					{
						engine->stopAllSounds();
						engine->play2D("../Music/GamePlayMusic.mp3", true);
						CSceneManager::Instance()->GoToScene(3);
						clicked2 = true;
					}
				}
			}
			else
			{
				clicked2 = false;
			}
			
		
		}


		//scene->Update(m_timer.getElapsedTime());
		//scene->Render();

		//Swap buffers
		glfwSwapBuffers(m_window);
		//Get and organize events, like keyboard and mouse input, window resizing, etc...
		glfwPollEvents();
		m_timer.waitUntil(frameTime);       // Frame rate limiter. Limits each frame to a specified time in ms.   

	} //Check if the ESC key had been pressed or if the window had been closed
	CSceneManager::Instance()->Exit();

	//scene->Exit();
	//delete scene;

}

void Application::endgame()
{
	CSceneManager::Instance()->GoToScene(2);

}

void Application::Exit()
{
	//Close OpenGL window and terminate GLFW
	glfwDestroyWindow(m_window);
	//Finalize and clean up GLFW
	glfwTerminate();
}
