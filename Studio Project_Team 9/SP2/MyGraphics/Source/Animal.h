#ifndef ANIMAL_H
#define ANIMAL_H
#include "Vector3.h"
#include "Collision.h"
#include "Camera3.h"

class Animal {



public:
	enum DIRECTION
	{
		FORWARD,
		BACKWARD,
		LEFT,
		RIGHT,
		FORWARDLEFT,
		FORWARDRIGHT,
		BACKWARDLEFT,
		BACKWARDRIGHT,
	};



	enum State {
		CHASING,
		IDLE
	};

	Vector3 position;
	Vector3 view;
	Vector3 right;
	float Rotating;
	float rotateAmt;
	float CurrentRotate;
	float MOVEMENT_SPEED;
	float rotation;
	int Health;
	bool cowllided;
	bool Alive;
	float deadrotate;
	float damage;

	Animal();
	~Animal();
	DIRECTION dir;
	State WolvesState;

	//void setDirection();
	//void move(DIRECTION dir);

	virtual void setDirection(Object o[], Camera cam) { 0; };


};
#endif