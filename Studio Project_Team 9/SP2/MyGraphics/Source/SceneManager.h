#pragma once
#include <vector>
#include <iostream>
#include "Scene.h"
#include "timer.h"

/**

- This class is supposed to Init ALL scenes first Before doing anything,
make sure that has been done first.


**/

class CSceneManager {
public:
	CSceneManager();

	enum Scenes {
		START_MENU,
		HOUSE,
		ENDMENU,
		MAINGAME,
		TOTAL_SCENES
	};
	// Returns Current Object
	static CSceneManager* Instance();

	// Add new Scene to Vector
	void AddScene(Scene* newScene);

	// Set to A Specific Scene
	void GoToScene(int SceneID);

	// Update Current Scene
	void Update(StopWatch* dt);

	int returnSceneID();

	Scene* GetCurrentScene();

	// Exit
	void Exit();

	std::vector<Scene*> sceneList;

private:
	// The Actual instance
	static CSceneManager* instance;

	// The vector to contain all the scenes


	// To Store the Current Scene enum
	int currentSceneID;

	// Total Amount of Scenes


	// Init the Scene
	void InitScene();

	// Added anot
	bool added;
};