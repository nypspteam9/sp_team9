#include "Camera2.h"
#include "Application.h"
#include "Mtx44.h"

Camera2::Camera2()
{
}

Camera2::~Camera2()
{
}

void Camera2::Init(const Vector3& pos, const Vector3& target, const Vector3& up)
{
	this->position = defaultPosition = pos;
	this->target = defaultTarget = target;
	Vector3 view = (target - position).Normalized();
	Vector3 right = view.Cross(up);
	right.y = 0;
	right.Normalize();
	this->up = defaultUp = right.Cross(view).Normalized();
	xMouse = 0;
	yMouse = 0;
}

void Camera2::Update(double dt)
{
	static const float CAMERA_SPEED = 50.f;
	static const float MOVEMENT_SPEED = 0.1f;

	Vector3 view = (target - position).Normalized();
	Vector3 right = view.Cross(up);

	

	Application::GetMousePos(xMouse, yMouse);

	

	if (Application::IsKeyPressed('R'))
	{
		Reset();
	}

}

void Camera2::Reset()
{
	position = defaultPosition;
	target = defaultTarget;
	up = defaultUp;
}